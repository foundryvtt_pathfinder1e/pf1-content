_id: B5qjfHf1pFgEA7bg
_key: '!journal!B5qjfHf1pFgEA7bg'
_stats:
  coreVersion: '12.331'
folder: br8fwJFrbgshQXJz
name: 3.2.5. Mountain Terrain
pages:
  - _id: w4jTFUFkH6lt5KrQ
    _key: '!journal.pages!B5qjfHf1pFgEA7bg.w4jTFUFkH6lt5KrQ'
    _stats:
      coreVersion: '12.331'
    name: 3.2.5. Mountain Terrain
    sort: 0
    text:
      content: >-
        <p>@Compendium[pf-content.pf-rules.XOJpjqmBzwE4R4n0]{(Index)
        Environment}</p><p>@Compendium[pf-content.pf-rules.CPUpraqn1jz9vhMh]{3.2.
        Wilderness}</p><ul><li>@Compendium[pf-content.pf-rules.QrderWyxAp6RUvtz]{3.2.1.
        Getting
        Lost}</li><li>@Compendium[pf-content.pf-rules.pcu8vDTPaIYw82SU]{3.2.2.
        Forest
        Terrain}</li><li>@Compendium[pf-content.pf-rules.zJ4qqxSB0Iufd5Be]{3.2.3.
        Marsh
        Terrain}</li><li>@Compendium[pf-content.pf-rules.RVKULXygOAXTZ1WC]{3.2.4.
        Hills
        Terrain}</li><li><strong>@Compendium[pf-content.pf-rules.B5qjfHf1pFgEA7bg]{3.2.5.
        Mountain
        Terrain}</strong></li><li>@Compendium[pf-content.pf-rules.tuZEOzPezZUbz4Fj]{3.2.5.2.
        Mountain
        Travel}</li><li>@Compendium[pf-content.pf-rules.3YyU7Yg6wbShgOKM]{3.2.6.
        Desert
        Terrain}</li><li>@Compendium[pf-content.pf-rules.QOCypcVHE6zpYlyD]{3.2.7.
        Plains
        Terrain}</li><li>@Compendium[pf-content.pf-rules.xiRC5p84P3tFW9mo]{3.2.8.
        Aquatic Terrain}</li></ul><hr /><p>@Source[PZO1110;pages=428]</p><p>The
        three mountain terrain categories are alpine meadows, rugged mountains,
        and forbidding mountains. As characters ascend into a mountainous area,
        they’re likely to face each terrain category in turn, beginning with
        alpine meadows, extending through rugged mountains, and reaching
        forbidding mountains near the
        summit.</p><p>@Compendium[pf-content.pf-rules.uMrEisQUiYJPGSEn]{3.2.5.1.
        Avalanches}</p><p>@Compendium[pf-content.pf-rules.tuZEOzPezZUbz4Fj]{3.2.5.2.
        Mountain Travel}</p><p>Mountains have an important terrain element, the
        rock wall, that is marked on the border between squares rather than
        taking up squares itself.</p><hr /><table><thead><tr><td>Mountain
        Category</td></tr><tr><td> </td><td>Alpine
        Meadow</td><td>Rugged</td><td>Forbidding</td></tr></thead><tbody><tr><td>Gradual
        slope</td><td>50%</td><td>25%</td><td>15%</td></tr><tr><td>Steep
        slope</td><td>40%</td><td>55%</td><td>55%</td></tr><tr><td>Cliff</td><td>10%</td><td>15%</td><td>20%</td></tr><tr><td>Chasm</td><td>—</td><td>5%</td><td>10%</td></tr><tr><td>Light
        undergrowth</td><td>20%</td><td>10%</td><td>—</td></tr><tr><td>Scree</td><td>—</td><td>20%</td><td>30%</td></tr><tr><td>Dense
        Rubble</td><td>—</td><td>20%</td><td>30%</td></tr></tbody></table><hr
        /><h2>Gradual and Steep Slopes</h2><p>These function as described in
        Hills Terrain.</p><h2>Cliff</h2><p>These terrain elements also function
        like their hills terrain counterparts, but they’re typically 2d6 × 10
        feet tall. Cliffs taller than 80 feet take up 20 feet of horizontal
        space.</p><h2>Chasm</h2><p>Usually formed by natural geological
        processes, chasms function like pits in a dungeon setting. Chasms aren’t
        hidden, so characters won’t fall into them by accident (although bull
        rushes are another story). A typical chasm is 2d4 × 10 feet deep, at
        least 20 feet long, and anywhere from 5 feet to 20 feet wide. It takes a
        DC 15 Climb check to climb out of a chasm. In forbidding mountain
        terrain, chasms are typically 2d8 × 10 feet deep.</p><h2>Light
        Undergrowth</h2><p>This functions as described in Forest
        Terrain.</p><h2>Scree</h2><p>A field of shifting gravel, scree doesn’t
        affect speed, but it can be treacherous on a slope. The DC of Acrobatics
        checks increases by 2 if there’s scree on a gradual slope and by 5 if
        there’s scree on a steep slope. The DC of Stealth checks increases by 2
        if the scree is on a slope of any kind.</p><h2>Dense Rubble</h2><p>The
        ground is covered with rocks of all sizes. It costs 2 squares of
        movement to enter a square with dense rubble. The DC of Acrobatics
        checks on dense rubble increases by 5, and the DC of Stealth checks
        increases by 2.</p><h2>Rock Wall</h2><p>A vertical plane of stone, rock
        walls require DC 25 Climb checks to ascend. A typical rock wall is 2d4 ×
        10 feet tall in rugged mountains and 2d8 × 10 feet tall in forbidding
        mountains. Rock walls are drawn on the edges of squares, not in the
        squares themselves.</p><h2>Cave Entrance</h2><p>Found in cliff and steep
        slope squares and next to rock walls, cave entrances are typically
        between 5 and 20 feet wide and 5 feet deep. A cave could be anything
        from a simple chamber to the entrance to an elaborate dungeon. Caves
        used as monster lairs typically have 1d3 rooms that are 1d4 × 10 feet
        across.</p><h2>Other Mountain Terrain Features</h2><p>Most alpine
        meadows begin above the treeline, so trees and other forest elements are
        rare in the mountains. Mountain terrain can include active streams (5 to
        10 feet wide and no more than 5 feet deep) and dry streambeds (treat as
        a trench 5 to 10 feet across). Particularly high-altitude areas tend to
        be colder than the lowland areas that surround them, so they might be
        covered in ice sheets (described in
        @Compendium[pf-content.pf-rules.3YyU7Yg6wbShgOKM]{3.2.6. Desert
        Terrain}).</p><h2>Stealth and Detection in Mountains</h2><p>As a
        guideline, the maximum distance in mountain terrain at which a
        Perception check for detecting the nearby presence of others can succeed
        is 4d10 × 10 feet. Certain peaks and ridgelines afford much better
        vantage points, of course, and twisting valleys and canyons have much
        shorter spotting distances. Because there’s little vegetation to
        obstruct line of sight, the specifics on your map are your best guide
        for the range at which an encounter could begin. As in hills terrain, a
        ridge or peak provides enough cover to hide from anyone below the high
        point.</p><p>It’s easier to hear faraway sounds in the mountains. The DC
        of Perception checks that rely on sound increase by 1 per 20 feet
        between listener and source, not per 10 feet.</p>
      format: 1
    title:
      level: 1
      show: true
    type: text
sort: 0
