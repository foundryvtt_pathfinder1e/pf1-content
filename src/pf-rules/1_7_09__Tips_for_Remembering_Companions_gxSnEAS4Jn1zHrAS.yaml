_id: gxSnEAS4Jn1zHrAS
_key: '!journal!gxSnEAS4Jn1zHrAS'
_stats:
  coreVersion: '12.331'
folder: sWur0vXI68cZozsg
name: 1.7.09. Tips for Remembering Companions
pages:
  - _id: sPo3ttni3aH6MmI0
    _key: '!journal.pages!gxSnEAS4Jn1zHrAS.sPo3ttni3aH6MmI0'
    _stats:
      coreVersion: '12.331'
    name: 1.7.09. Tips for Remembering Companions
    sort: 0
    text:
      content: >-
        <p>@Compendium[pf-content.pf-rules.Ic1tlF3EE1QNW3kE]{(Index)}</p><p>@Compendium[pf-content.pf-rules.8scSEgBEtJZj0suf]{(Index)
        Basic
        Rules}</p><ul><li><p>@Compendium[pf-content.pf-rules.sjhxRhOgV3ynmqf9]{1.7.
        Animal
        Companions}</p><ul><li>@Compendium[pf-content.pf-rules.bLW7my2JM1AWHcPt]{1.7.01.
        Controlling Companions, Familiars, and
        Followers}</li><li>@Compendium[pf-content.pf-rules.QrfgKKGq1a9P28cD]{1.7.02.
        Animal
        Options}</li><li>@Compendium[pf-content.pf-rules.751RCqF5c5cRGCVG]{1.7.03.
        Monstrous
        Options}</li><li>@Compendium[pf-content.pf-rules.A2INBQqsGOHfeN36]{1.7.04.
        Plant
        Options}</li><li>@Compendium[pf-content.pf-rules.3nO5z2V4Y5WdTxqp]{1.7.05.
        Vermin
        Options}</li><li>@Compendium[pf-content.pf-rules.fzg18IfdQWR57QpN]{1.7.06.
        Advancing
        Companions}</li><li>@Compendium[pf-content.pf-rules.EBgdBlDozsMHFm1r]{1.7.07.
        Intelligent
        Animals}</li><li>@Compendium[pf-content.pf-rules.MaMACE7dH6mGN9yB]{1.7.08.
        Awakened
        Animals}</li><li><strong>@Compendium[pf-content.pf-rules.gxSnEAS4Jn1zHrAS]{1.7.09.
        Tips for Remembering
        Companions}</strong></li><li>@Compendium[pf-content.pf-rules.S1ITgv2XUahBeROc]{1.7.10.
        Companion Plot
        Hooks}</li><li>@Compendium[pf-content.pf-rules.6zNHWvDTzqgtPPTd]{1.7.11.
        Reviving and Replacing Companions}</li></ul></li></ul><hr /><p>There are
        several ways to make sure a companion doesn’t get lost or
        forgotten.</p><h2>Props</h2><p>Physical props can help you, the other
        players, and the GM remember companions. If the campaign uses miniatures
        on the tabletop, the companion should have its own miniature or token.
        If all the adventurers move forward, it’s easy to see that a lonely
        miniature was left behind. Even without miniatures, having a physical
        representation of the companion on the tabletop keeps it in mind.
        Whether this is a stuffed animal, a toy, an action figure, a cardboard
        stand-up, a GameMastery Face Card, or a simple character sheet with a
        colorful illustration, this kind of reminder gives the companion a
        presence on the tabletop.</p><h2>Another Player</h2><p>If you regularly
        forget the presence of your companion and the GM is busy dealing with
        the rest of the game, another player can take over playing the
        companion. If the second player has an introverted character or one
        whose actions in combat are fast and efficient, allowing that player to
        control the companion gives him another opportunity to have some time in
        the spotlight. The second player should roll initiative separately for
        the companion so the companion’s actions don’t get forgotten on either
        turn—giving the companion its own turn reinforces its role in the
        party.</p><p>Allowing another character to play the companion also gives
        the group additional roleplaying opportunities. You might feel silly
        talking as both your character and your cohort, but more comfortable
        having a dialogue with your cohort when it’s played by someone else
        (this also keeps the cohort from blindly doing whatever you say).
        Wearing a hat or mask, or holding up a small flag or banner to represent
        the companion can help other players keep track of who is acting when
        you speak.</p><h2>Casual Observer</h2><p>Some gaming groups have a
        casual player, friend, spouse, or child who isn’t interested in playing
        a normal character for the campaign, but likes to watch the game or be
        nearby when everyone else is playing. That person might be interested in
        playing a companion for one or more sessions (especially if it’s a
        creature that’s funny and cute). This is an opportunity for that person
        to get involved in the game without the responsibility of being a full
        contributing member to the group—and just might be the hook that
        convinces that observer to become active in the game.</p><p>If playing a
        companion goes well, the GM may create a one-shot spin-off adventure in
        which all the players play companion creatures instead of normal PCs
        (perhaps because the PCs are captured, incapacitated, or merely
        sleeping), returning to the normal campaign when that adventure is
        completed.</p><p>Often, a companion is forgotten about when it’s not
        needed. a familiar hides in a backpack and only comes out when the
        sorcerer needs to spy on something or deliver a spell with a range of
        touch. An animal companion or cohort follows the druid silently and acts
        only when a skill check or attack roll is needed. An eidolon is used as
        a mount or an expendable resource in battle. You and the GM need to
        remember that a companion is a creature, not an unthinking tool, and
        can’t simply be ignored.</p><p>Followers are a little more complex
        because there can be so many of them and they don’t usually adventure
        with you. You and the GM should keep notes about each follower (or group
        of followers, if there are several in a common location such as a
        temple) and link this information to the followers’ base of operations.
        For example, the GM’s notes about the capital city should mention the
        thieves’ guild informant follower of the rogue PC. Artwork representing
        the follower (even a simple piece of free clip art found online) can be
        a stronger reminder than a name that’s easily lost in a page full of
        words.</p><p>Followers also have a unique companion role in that they
        spend most of their time away from you, and might use that time
        positively or negatively. Just because a follower is low level and
        you’re not doesn’t mean the follower stops being a person with needs,
        fears, and a role to play in your heroic story. Even if you dismiss the
        follower aspect of the Leadership feat as baggage, a follower is going
        to pay attention to what you do, and if this hero-worship grows
        tarnished from neglect or abuse, that very same follower provides an
        opportunity for the GM to demonstrate how bad will among the common folk
        can negatively affect an adventurer’s life (see the Reputation section
        of this chapter for more information).</p>
      format: 1
    title:
      level: 1
      show: true
    type: text
sort: 0
