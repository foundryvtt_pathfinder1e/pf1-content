_id: mDgGgG7B4Ox86IEq
_key: '!journal!mDgGgG7B4Ox86IEq'
_stats:
  coreVersion: '12.331'
folder: B3pncms1TJP9sIoG
name: 3.5.3.1.1. Plane of Air
pages:
  - _id: mib6s0m6T9OF57eK
    _key: '!journal.pages!mDgGgG7B4Ox86IEq.mib6s0m6T9OF57eK'
    _stats:
      coreVersion: '12.331'
    name: 3.5.3.1.1. Plane of Air
    sort: 0
    text:
      content: >-
        <p>@Compendium[pf-content.pf-rules.XOJpjqmBzwE4R4n0]{(Index)
        Environment}</p><p>@Compendium[pf-content.pf-rules.ryIRK7CjUk9fI8TT]{3.5.
        The
        Planes}</p><ul><li><p>@Compendium[pf-content.pf-rules.BtNqgktOkZENzN1c]{3.5.3.1.
        Elemental
        Planes}</p><ul><li><p><strong>@UUID[Compendium.pf-content.pf-rules.JournalEntry.mDgGgG7B4Ox86IEq]{3.5.3.1.1.
        Plane of
        Air}</strong></p></li><li><p>@UUID[Compendium.pf-content.pf-rules.JournalEntry.9Lhm9foOPJ0V7BKp]{3.5.3.1.2.
        Plane of
        Earth}</p></li><li><p>@UUID[Compendium.pf-content.pf-rules.JournalEntry.dd3XFS6cZ47Y1Zut]{3.5.3.1.3.
        Plane of
        Fire}</p></li><li><p>@UUID[Compendium.pf-content.pf-rules.JournalEntry.qjK0wOAyu9X5wWUC]{3.5.3.1.4.
        Plane of Water}</p></li></ul></li></ul><hr
        /><p>@Source[PZO1114;pages=189]</p><p>The Plane of Air is an empty
        plane, consisting of sky above and sky below. It is the most comfortable
        and survivable of the Inner Planes and is the home of all manner of
        airborne creatures. Indeed, flying creatures find themselves at a great
        advantage on this plane. While travelers without flight can survive
        easily here, they are at a disadvantage.</p><h2>Planar Traits</h2><p>The
        Plane of Air has the following traits:</p><ul><li><strong>Subjective
        Directional Gravity</strong>: Inhabitants of the plane determine their
        own “down” direction. Objects not under the motive force of others do
        not move.</li><li><strong>Air-Dominant</strong></li><li><strong>Enhanced
        Magic</strong>: Spells and spell-like abilities with the air descriptor
        or that use, manipulate, or create air (including those of the Air
        domain and the elemental [air] bloodline) are
        enhanced.</li><li><strong>Impeded Magic</strong>: Spells and spell-like
        abilities with the earth descriptor or that use or create earth
        (including those of the Earth domain, spell-like abilities of the
        elemental [earth] bloodline, and spells that summon earth elementals or
        outsiders with the earth subtype) are
        impeded.</li></ul><h2>Description</h2><p>The Plane of Air is an empty
        plane, consisting of sky above and sky below. It is the most comfortable
        and survivable of the Inner Planes and is the home of all manner of
        airborne creatures. Indeed, flying creatures find themselves at a great
        advantage on this plane. While travelers without flight can survive
        easily here, they are at a disadvantage.</p><p>The Plane of Air exists
        at the edge of the universe—a “skin” of wind and clouds where down seems
        to be in every direction at once. Here, the sky is everything there is,
        and only those who can fly can ever truly call this reality
        home.</p><p>Just beyond the Material Plane floats the immense, seemingly
        peaceful blue firmament of the Plane of Air. Known to many as the
        Endless Sky, this plane is shot through with enormous clouds, floating
        cities of fantastic design, meandering sheets of ice and crystal,
        strange spheres of brass and iron, and even more astounding features.
        Towering cloud walls mark the borders it shares with the edge of the
        Material Plane’s universe, and gigantic water bubbles—oceanic ecosystems
        in their own right—pepper the areas abutting the Plane of Water.
        Although its population is scant in comparison to those of the other
        Elemental Planes, the Plane of Air is home to a grand djinni society,
        dozens of mephit kingdoms, and a plentitude of diverse
        creatures.</p><p>Breathable air is the plane’s most dominant substance,
        making the Plane of Air the most hospitable of all the Elemental Planes
        for visitors from the Material Plane.</p><p>Flying creatures have great
        advantages here, though nonnatives of all sorts might find themselves
        constrained by the lack of solid ground. Such material does exist, in
        the form of great chunks of drifting ice originating from the adjacent
        Plane of Water and magically suspended hunks of earth and crystal—but
        such places are few and far between.</p><h2>Denizens</h2><p>Although it
        is the least populated of the Elemental Planes due to its lack of solid
        ground, the Plane of Air still teems with life. Unlike on the outer
        planes, the petitioners of the Plane of Air (vaporous entities known as
        air pneumas) are not common, as the time it takes for an air pneuma to
        transition into an air elemental or similar creature is typically quite
        short. Listed below are several prominent or noteworthy residents whom
        planar travelers might encounter.</p><h3>Djinn</h3><p>The Plane of Air’s
        native genies are the true lords of the Endless Sky. Powerful beings in
        their own right, they focus on art, culture, trade, and especially the
        acquisition of knowledge, which brings them great wealth and even
        greater influence. Calmer and more peaceful than the genies native to
        other planes, djinn are nonetheless quick to remind all listeners that
        they have been the plane’s undisputed masters since before the mortal
        races appeared. This sense of superiority, combined with their
        paternalism toward nongenies, has earned the djinn a widespread
        reputation of insufferable self-importance. Despite the djinn’s stuffy
        proclamations, some believe that their superiority has not always been
        uncontested. Mysterious metal spheres scattered throughout the plane
        fuel rumors of a long-vanquished foe. A recent theory involves an
        ancient war between the djinn and a contingent of extraplanar outsiders,
        or perhaps gods. Some claim that these spheres actually imprison the
        genies’ rivals. The djinn’s failure to acknowledge these rumors, plus
        their refusal to build upon or even touch these spheres, has done
        nothing to quell such speculation.</p><h3>Air Elementals</h3><p>Though
        just as disorganized as other elementals, air elementals have their own
        culture and are known for being more gregarious than others of their
        kind. They are intelligent, but their mind-sets and emotions can change
        as quickly as the winds from which they are born. The elementals’
        command obedience from all but the most rebellious air elementals. Air
        elementals are not the realm’s only elementals. In the places where the
        azure sky turns dark with violent storms, lightning elementals dash
        about like children playing in the sea, unleashing great bolts of
        electricity. Ice elementals also venture to this plane, despite being
        native to the Plane of Water.</p><h3>Cloud Dragons</h3><p>Cloud dragons
        are natives of the Plane of Air and are known for flitting unhindered
        through the expanse of the Endless Sky, indulging their fickle whims.
        They have an innate curiosity and may begin a quest for lore or a lost
        bauble only to allow a distraction to sidetrack them for years. Cloud
        dragons’ whimsical journeys sometimes take them away from their home
        plane; most cloud dragons on the Material Plane traveled there for a
        specific but now long-forgotten purpose and, finding the mountain peaks
        of Material Plane worlds quite hospitable, decided to
        stay.</p><h3>Mephits</h3><p>Three types of mephits dwell on the Plane of
        Air: air, dust, and ice mephits. These miniscule scamps sometimes serve
        the powerful entities that dwell on the Plane of Air and are often seen
        flitting about the plane’s great cities, delivering messages or running
        errands. Many mephits, however, choose to remain independent. These
        mephits create small nations on the plane’s scattered islands, and each
        nation’s strictures and social mores depend on the type and personality
        of its self-proclaimed mephit ruler. Mephit nations often have confusing
        and contradictory laws and customs, making it easy for visitors to
        unknowingly break them. Moreover, familiarity with the customs of one
        nation never provides much help in understanding the rules of
        another.</p><h3>Sylphs</h3><p>Although uncommon on their progenitors’
        home plane, highly capable sylphs have left the Material Plane in fair
        numbers to reside on the Plane of Air. They work often as commanders of
        airship fleets or as extraplanar diplomats for the djinn—or, more
        rarely, the elemental population. Still others are high-priced guides
        for the plane’s most esteemed or influential visitors. Some conspirators
        posit that such sylphs act as spies for extraplanar forces that wish to
        eventually challenge the djinn for dominance of the plane, but
        practically no one—the air genies included— gives the theory any
        credence.</p><h2>Divinities</h2><ul><li><p>Elemental lords of
        air</p></li></ul>
      format: 1
    title:
      level: 1
      show: true
    type: text
sort: 0
