_id: CK4ZYweM6IifrrV0
_key: '!journal!CK4ZYweM6IifrrV0'
_stats:
  coreVersion: '12.331'
folder: mJGgfLDxjMpNAoek
name: 2.07.3. Combat Maneuvers
pages:
  - _id: ZBZ3xU50IbPhwGWh
    _key: '!journal.pages!CK4ZYweM6IifrrV0.ZBZ3xU50IbPhwGWh'
    _stats:
      coreVersion: '12.331'
    name: 2.07.3. Combat Maneuvers
    sort: 0
    text:
      content: >-
        <p>@Compendium[pf-content.pf-rules.OiNttXSDW598DIEs]{(Index) Combat
        Rules}</p><p>@Compendium[pf-content.pf-rules.u7ymeeQewjTXVrlW]{2.07.
        Special
        Attacks}</p><ul><li>@Compendium[pf-content.pf-rules.vuTQH6gykORoSCUE]{2.07.1.
        Aid
        Another}</li><li>@Compendium[pf-content.pf-rules.o0Ufwyu65bmusBhM]{2.07.2.
        Charge}</li><li><strong>@Compendium[pf-content.pf-rules.CK4ZYweM6IifrrV0]{2.07.3.
        Combat
        Maneuvers}</strong></li><li>@Compendium[pf-content.pf-rules.QrPAIYOFsZBHKHvt]{2.07.4.
        Feint}</li><li>@Compendium[pf-content.pf-rules.uR2lQHLIAn5HhlfT]{2.07.5.
        Mounted
        Combat}</li><li>@Compendium[pf-content.pf-rules.KrSwE6n4UyxJfYPB]{2.07.6.
        Throw Splash
        Weapon}</li><li>@Compendium[pf-content.pf-rules.jLQbe8GmyB470OIJ]{2.07.7.
        Two-Weapon Fighting}</li></ul><hr
        /><p>@Source[PZO1110;pages=198]</p><p>During combat, you can attempt to
        perform a number of maneuvers that can hinder or even cripple your foe,
        including bull rush, disarm, grapple, overrun, sunder, and trip.
        Although these maneuvers have vastly different results, they all use a
        similar mechanic to determine
        success.</p><ul><li>@Compendium[pf-content.pf-rules.4ZtXz1UxNVHAoK9F]{2.07.3.01.
        Bull
        Rush}</li><li>@Compendium[pf-content.pf-rules.TOgcg1rD2rqozLK8]{2.07.3.02.
        Dirty
        Trick}</li><li>@Compendium[pf-content.pf-rules.2e9cYCdNMg0E9nDP]{2.07.3.03.
        Disarm}</li><li>@Compendium[pf-content.pf-rules.2QXapJZHDOdCaqJC]{2.07.3.04.
        Drag}</li><li>@Compendium[pf-content.pf-rules.wwDkxmSkg7edogZk]{2.07.3.05.
        Grapple}</li><li>@Compendium[pf-content.pf-rules.6txWY4yVuOOzjdRf]{2.07.3.06.
        Overrun}</li><li>@Compendium[pf-content.pf-rules.8CRc1rPAFBfhJfBN]{2.07.3.07.
        Reposition}</li><li>@Compendium[pf-content.pf-rules.LiqgRZYAPqTbhf18]{2.07.3.08.
        Steal}</li><li>@Compendium[pf-content.pf-rules.bEwb7yQOidiPzb5v]{2.07.3.09.
        Sunder}</li><li>@Compendium[pf-content.pf-rules.QBZtZChctrkE8ksB]{2.07.3.10.
        Trip}</li></ul><h2>Combat Maneuver Bonus</h2><p>Each character and
        creature has a Combat Maneuver Bonus (or CMB) that represents its skill
        at performing combat maneuvers. A creature’s CMB is determined using the
        following formula:</p><blockquote><p>CMB = Base attack bonus + Strength
        modifier + special size modifier</p></blockquote><p>Creatures that are
        size Tiny or smaller use their Dexterity modifier in place of their
        Strength modifier to determine their CMB. The special size modifier for
        a creature’s Combat Maneuver Bonus is as follows:</p><p>Fine –8,
        Diminutive –4, Tiny –2, Small –1, Medium +0, Large +1, Huge +2,
        Gargantuan +4, Colossal +8.</p><p>Some feats and abilities grant a bonus
        to your CMB when performing specific maneuvers.</p><h2>Performing a
        Combat Maneuver</h2><p>When performing a combat maneuver, you must use
        an action appropriate to the maneuver you are attempting to perform.
        While many combat maneuvers can be performed as part of an attack
        action, full-attack action, or attack of opportunity (in place of a
        melee attack), others require a specific action. Unless otherwise noted,
        performing a combat maneuver provokes an attack of opportunity from the
        target of the maneuver. If you are hit by the target, you take the
        damage normally and apply that amount as a penalty to the attack roll to
        perform the maneuver. If your target is immobilized, unconscious, or
        otherwise incapacitated, your maneuver automatically succeeds (treat as
        if you rolled a natural 20 on the attack roll). If your target is
        stunned, you receive a +4 bonus on your attack roll to perform a combat
        maneuver against it.</p><p>When you attempt to perform a combat
        maneuver, make an attack roll and add your CMB in place of your normal
        attack bonus. Add any bonuses you currently have on attack rolls due to
        spells, feats, and other effects. These bonuses must be applicable to
        the weapon or attack used to perform the maneuver. The DC of this
        maneuver is your target’s Combat Maneuver Defense. Combat maneuvers are
        attack rolls, so you must roll for concealment and take any other
        penalties that would normally apply to an attack roll.</p><h2>Combat
        Maneuver Defense</h2><p>Each character and creature has a Combat
        Maneuver Defense (or CMD) that represents its ability to resist combat
        maneuvers. A creature’s CMD is determined using the following
        formula:</p><blockquote><p>CMD = 10 + Base attack bonus + Strength
        modifier + Dexterity modifier + special size
        modifier</p></blockquote><p>The special size modifier for a creature’s
        Combat Maneuver Defense is as follows:</p><p>Fine –8, Diminutive –4,
        Tiny –2, Small –1, Medium +0, Large +1, Huge +2, Gargantuan +4, Colossal
        +8.</p><p>Some feats and abilities grant a bonus to your CMD when
        resisting specific maneuvers. A creature can also add any circumstance,
        deflection, dodge, insight, luck, morale, profane, and sacred bonuses to
        AC to its CMD. Any penalties to a creature’s AC also apply to its CMD. A
        flat-footed creature does not add its Dexterity bonus to its
        CMD.</p><h2>Determine Success</h2><p>If your attack roll equals or
        exceeds the CMD of the target, your maneuver is a success and has the
        listed effect. Some maneuvers, such as bull rush, have varying levels of
        success depending on how much your attack roll exceeds the target’s CMD.
        Rolling a natural 20 while attempting a combat maneuver is always a
        success (except when attempting to escape from bonds), while rolling a
        natural 1 is always a failure.</p>
      format: 1
    title:
      level: 1
      show: true
    type: text
sort: 0
