_id: dmUzb6b2BspbBdDp
_key: '!journal!dmUzb6b2BspbBdDp'
_stats:
  coreVersion: '12.331'
folder: CMTuXqZ6Ywxqk9Ar
name: 7.1.5.16. Bonus Type
pages:
  - _id: GS8oEHkijcTUfJjO
    _key: '!journal.pages!dmUzb6b2BspbBdDp.GS8oEHkijcTUfJjO'
    _stats:
      coreVersion: '12.331'
    name: 7.1.5.16. Bonus Type
    sort: 0
    text:
      content: >-
        <p>@Compendium[pf-content.pf-rules.dS0UkKOINqI2iTDX]{(Index)
        Magic}</p><p>@Compendium[pf-content.pf-rules.zCeqoMSKdZ8r4EPd]{7.1.
        Mastering
        Magic}</p><ul><li><p>@Compendium[pf-content.pf-rules.xdyGkidQGN7vYL1E]{7.1.5.
        Designing
        Spells}</p><ul><li>@Compendium[pf-content.pf-rules.7Lc81tg2g1Q6TMPo]{7.1.5.01.
        The Golden
        Rule}</li><li>@Compendium[pf-content.pf-rules.DZBAa9axA5Gv4el0]{7.1.5.02.
        Spell
        Terminology}</li><li>@Compendium[pf-content.pf-rules.dbb5pwAitIa3cqqj]{7.1.5.03.
        Intended
        Level}</li><li>@Compendium[pf-content.pf-rules.1DkXvcE3KMmBww9Z]{7.1.5.04.
        Function}</li><li>@Compendium[pf-content.pf-rules.Xu1jyIRMERPFh8X6]{7.1.5.05.
        Spell
        Research}</li><li>@Compendium[pf-content.pf-rules.fF9HpImlhp8oNX5Q]{7.1.5.06.
        Spell
        Damage}</li><li>@Compendium[pf-content.pf-rules.zrMFTIHfzbQTTwZ1]{7.1.5.07.
        Target}</li><li>@Compendium[pf-content.pf-rules.wsF1rdV2ACkYU3Su]{7.1.5.08.
        Damage
        Caps}</li><li>@Compendium[pf-content.pf-rules.9TyMr3XedNUcWVzB]{7.1.5.09.
        Range}</li><li>@Compendium[pf-content.pf-rules.KlR2PDab6uknvopJ]{7.1.5.10.
        Duration}</li><li>@Compendium[pf-content.pf-rules.x0E7IZ5imhQ5srrf]{7.1.5.11.
        Saving
        Throw}</li><li>@Compendium[pf-content.pf-rules.RRG1A0rBL44Qeskg]{7.1.5.12.
        Spell
        Resistance}</li><li>@Compendium[pf-content.pf-rules.kvl8JOILza5HEj7N]{7.1.5.13.
        Casting
        Time}</li><li>@Compendium[pf-content.pf-rules.wb9tYgBlWX8peqFl]{7.1.5.14.
        Components}</li><li>@Compendium[pf-content.pf-rules.PCvwd6PaQH7mD7o6]{7.1.5.15.
        School}</li><li><strong>@Compendium[pf-content.pf-rules.dmUzb6b2BspbBdDp]{7.1.5.16.
        Bonus
        Type}</strong></li><li>@Compendium[pf-content.pf-rules.gh6sxSeK1RNLLFoN]{7.1.5.17.
        Description}</li><li>@Compendium[pf-content.pf-rules.q6Xva2npOy7fVL42]{7.1.5.18.
        Hierarchy of Attack
        Effects}</li><li>@Compendium[pf-content.pf-rules.s6CWKZDo0zQeRXMJ]{7.1.5.19.
        Depletable
        Statistics}</li><li>@Compendium[pf-content.pf-rules.r8zYCFuq59NEtjkz]{7.1.5.20.
        Core is
        King}</li><li>@Compendium[pf-content.pf-rules.aVUL2wrJszAUzNMK]{7.1.5.21.
        Multipurpose
        Spells}</li><li>@Compendium[pf-content.pf-rules.Ak9Ws3Z16GY6QSoH]{7.1.5.22.
        Choosing
        Descriptors}</li><li>@Compendium[pf-content.pf-rules.baSlaM24kwB68nuf]{7.1.5.23.
        Benchmarks}</li></ul></li></ul><hr
        /><p>@Source[PZO1117;pages=134]</p><p>There are many types of bonuses in
        the game. It’s tempting to look at that list of bonuses, find “holes” in
        the spell list that don’t have spells for certain bonus types, and
        create a new spell that adds one of those unused bonus types to your
        favorite statistic or roll. Resist this temptation. Not all bonus types
        are equal within the game, and many bonus types are only meant for
        certain things. See Table 2–7: Bonus Types and Effects.</p><p>A dash
        entry (—) in the table indicates there are no common examples of items
        or spells that grant that kind of bonus. If you’re designing an item or
        spell and want to include a certain type of bonus to a particular
        ability or statistic, check Table 2–7 first; if the bonus type doesn’t
        say it can affect that ability or statistic, use one that does instead.
        One reason for this table is that some bonuses are better than others
        (deflection bonuses work against incorporeal creatures and when you are
        flat-footed, natural armor bonuses do not). A second reason is that
        allowing any kind of bonus on any roll or statistic makes it really easy
        to stack many small bonuses more cheaply than a larger bonus, which
        makes powerful magic items like a ring of protection +5 much less
        interesting. A third reason is that some of these combinations just
        don’t make sense, like a deflection bonus to Strength or a shield bonus
        on Knowledge checks.</p><h2>Table 2-7: Bonus Types and
        Effects</h2><table border="1"><thead><tr><td>Bonus Type</td><td>Can
        Affect</td><td>Sample Item</td><td>Sample
        Spell</td></tr></thead><tbody><tr><td>Alchemical</td><td>Ability scores,
        saves</td><td>Antitoxin</td><td>-</td></tr><tr><td>Armor</td><td>AC</td><td><a
        href="https://www.aonprd.com/MagicWondrousDisplay.aspx?FinalName=Bracers%20of%20Armor1">Bracers
        of armor</a></td><td><a
        href="https://www.aonprd.com/SpellDisplay.aspx?ItemName=Mage%20armor">Mage
        armor</a></td></tr><tr><td>Circumstance</td><td>Attacks,
        checks</td><td><a
        href="https://www.aonprd.com/MagicWondrousDisplay.aspx?FinalName=Robe%20of%20Blending">Robe
        of blending</a></td><td>-</td></tr><tr><td>Competence</td><td>Attacks,
        checks, saves</td><td><a
        href="https://www.aonprd.com/MagicWondrousDisplay.aspx?FinalName=Boots%20of%20Elvenkind">Boots
        of elvenkind</a></td><td><a
        href="https://www.aonprd.com/SpellDisplay.aspx?ItemName=Guidance">Guidance</a></td></tr><tr><td>Deflection</td><td>AC</td><td><a
        href="https://www.aonprd.com/MagicRingsDisplay.aspx?FinalName=Ring%20of%20Protection1">Ring
        of
        protection</a></td><td>-</td></tr><tr><td>Dodge</td><td>AC</td><td>Never*</td><td>Never*</td></tr><tr><td>Enhancement</td><td>Ability
        scores, AC, attacks, damage, speed</td><td><a
        href="https://www.aonprd.com/MagicWondrousDisplay.aspx?FinalName=Belt%20of%20Giant%20Strength2">Belt
        of giant strength</a></td><td><a
        href="https://www.aonprd.com/SpellDisplay.aspx?ItemName=Magic%20weapon">Magic
        weapon</a></td></tr><tr><td>Inherent</td><td>Ability scores</td><td><a
        href="https://www.aonprd.com/MagicWondrousDisplay.aspx?FinalName=Manual%20of%20Bodily%20Health1">Manual
        of bodily health</a></td><td><a
        href="https://www.aonprd.com/SpellDisplay.aspx?ItemName=Wish">Wish</a></td></tr><tr><td>Insight</td><td>AC,
        attacks, checks, saves</td><td><a
        href="https://www.aonprd.com/MagicWondrousDisplay.aspx?FinalName=Ioun%20Stone%20Dusty%20Rose%20Prism">Dusty
        rose prism ioun stone</a></td><td><a
        href="https://www.aonprd.com/SpellDisplay.aspx?ItemName=True%20strike">True
        strike</a></td></tr><tr><td>Luck</td><td>AC, attacks, checks, damage,
        saves</td><td><a
        href="https://www.aonprd.com/MagicWondrousDisplay.aspx?FinalName=Stone%20of%20Good%20Luck%20(Luckstone)">Stone
        of good luck</a></td><td><a
        href="https://www.aonprd.com/SpellDisplay.aspx?ItemName=Divine%20favor">Divine
        favor</a></td></tr><tr><td>Morale</td><td>Attacks, checks, damage,
        saves, Str, Con, Dex</td><td><a
        href="https://www.aonprd.com/MagicWondrousDisplay.aspx?FinalName=Candle%20of%20Invocation">Candle
        of invocation</a></td><td><a
        href="https://www.aonprd.com/SpellDisplay.aspx?ItemName=Bless">Bless</a></td></tr><tr><td>Natural
        armor</td><td>AC</td><td><a
        href="https://www.aonprd.com/MagicWondrousDisplay.aspx?FinalName=Amulet%20of%20Natural%20Armor1">Amulet
        of natural armor</a></td><td><a
        href="https://www.aonprd.com/SpellDisplay.aspx?ItemName=Barkskin">Barkskin</a></td></tr><tr><td>Profane</td><td>AC,
        checks, damage, DCs, saves</td><td>-</td><td><a
        href="https://www.aonprd.com/SpellDisplay.aspx?ItemName=Desecrate">Desecrate</a></td></tr><tr><td>Resistance</td><td>Saves</td><td><a
        href="https://www.aonprd.com/MagicWondrousDisplay.aspx?FinalName=Cloak%20of%20Resistance1">Cloak
        of resistance</a></td><td><a
        href="https://www.aonprd.com/SpellDisplay.aspx?ItemName=Mind%20blank">Mind
        blank</a></td></tr><tr><td>Sacred</td><td>AC, checks, damage, DCs,
        saves</td><td>-</td><td><a
        href="https://www.aonprd.com/SpellDisplay.aspx?ItemName=Consecrate">Consecrate</a></td></tr><tr><td>Shield</td><td>AC</td><td><a
        href="https://www.aonprd.com/MagicRingsDisplay.aspx?FinalName=Ring%20of%20Force%20Shield">Ring
        of force shield</a></td><td><a
        href="https://www.aonprd.com/SpellDisplay.aspx?ItemName=Shield">Shield</a></td></tr><tr><td>Size</td><td>Ability
        scores, attacks, AC</td><td>-</td><td><a
        href="https://www.aonprd.com/SpellDisplay.aspx?ItemName=Enlarge%20person">Enlarge
        person</a></td></tr></tbody></table><p>* Spells and magic items should
        never grant dodge bonuses because dodge bonuses always stack, and it
        would be a simple matter achieving that AC using the armor, deflection,
        enhancement, and natural armor bonuses in the game.</p>
      format: 1
    title:
      level: 1
      show: true
    type: text
sort: 0
