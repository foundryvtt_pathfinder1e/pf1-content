_id: qm7mqCXwfpA2EM6w
_key: '!journal!qm7mqCXwfpA2EM6w'
_stats:
  coreVersion: '12.331'
folder: 6xAF0VmbLWU6dblG
name: 2.02.1.3. Cast a Spell
pages:
  - _id: krctSvejnEHlxisE
    _key: '!journal.pages!qm7mqCXwfpA2EM6w.krctSvejnEHlxisE'
    _stats:
      coreVersion: '12.331'
    name: 2.02.1.3. Cast a Spell
    sort: 0
    text:
      content: >-
        <p>@Compendium[pf-content.pf-rules.OiNttXSDW598DIEs]{(Index) Combat
        Rules}</p><p>@Compendium[pf-content.pf-rules.VZCpOrg3jCyNaYeg]{2.02.
        Actions in
        Combat}</p><ul><li><p>@Compendium[pf-content.pf-rules.rBQpOuMH0B3O44Da]{2.02.1.
        Standard
        Actions}</p><ul><li>@Compendium[pf-content.pf-rules.PsEv51WUo7p8GpBc]{2.02.1.1.
        Attack}</li><li>@Compendium[pf-content.pf-rules.r0ymFTNYo4FzlMSq]{2.02.1.2.
        Activate Magic
        Item}</li><li><strong>@Compendium[pf-content.pf-rules.qm7mqCXwfpA2EM6w]{2.02.1.3.
        Cast a
        Spell}</strong></li><li>@Compendium[pf-content.pf-rules.QjHgjK8yLzdgS9C2]{2.02.1.4.
        Start/Complete Full-Round
        Action}</li><li>@Compendium[pf-content.pf-rules.YVJsIHmx7LM0vjis]{2.02.1.5.
        Total
        Defense}</li><li>@Compendium[pf-content.pf-rules.1gHRLA4nwxDAlhkT]{2.02.1.6.
        Use Special Ability}</li></ul></li></ul><hr
        /><p>@Source[PZO1110;pages=183]</p><p>Most spells require 1 standard
        action to cast. You can cast such a spell either before or after you
        take a move action.</p><p>Note: You retain your Dexterity bonus to AC
        while casting.</p><h2>Spell Components</h2><p>To cast a spell with a
        verbal (V) component, your character must speak in a firm voice. If
        you’re gagged or in the area of a silence spell, you can’t cast such a
        spell. A spellcaster who has been deafened has a 20% chance to spoil any
        spell he tries to cast if that spell has a verbal component.</p><p>To
        cast a spell with a somatic (S) component, you must gesture freely with
        at least one hand. You can’t cast a spell of this type while bound,
        grappling, or with both your hands full or occupied.</p><p>To cast a
        spell with a material (M), focus (F), or divine focus (DF) component,
        you have to have the proper materials, as described by the spell. Unless
        these components are elaborate, preparing them is a free action. For
        material components and focuses whose costs are not listed in the spell
        description, you can assume that you have them if you have your spell
        component pouch.</p><h2>Concentration</h2><p>You must concentrate to
        cast a spell. If you can’t concentrate, you can’t cast a spell. If you
        start casting a spell but something interferes with your concentration,
        you must make a concentration check or lose the spell. The check’s DC
        depends on what is threatening your concentration (see Chapter 9). If
        you fail, the spell fizzles with no effect. If you prepare spells, it is
        lost from preparation. If you cast at will, it counts against your daily
        limit of spells even though you did not cast it
        successfully.</p><h2>Concentrating to Maintain a Spell</h2><p>Some
        spells require continued concentration to keep them going. Concentrating
        to maintain a spell is a standard action that doesn’t provoke an attack
        of opportunity. Anything that could break your concentration when
        casting a spell can keep you from concentrating to maintain a spell. If
        your concentration breaks, the spell ends.</p><h2>Casting
        Time</h2><p>Most spells have a casting time of 1 standard action. A
        spell cast in this manner immediately takes effect.</p><h2>Attacks of
        Opportunity</h2><p>Generally, if you cast a spell, you provoke attacks
        of opportunity from threatening enemies. If you take damage from an
        attack of opportunity, you must make a concentration check (DC 10 +
        points of damage taken + the spell’s level) or lose the
        spell.</p><p>Spells that require only a swift action to cast don’t
        provoke attacks of opportunity.</p><h2>Casting on the
        Defensive</h2><p>Casting a spell while on the defensive does not provoke
        an attack of opportunity. It does, however, require a concentration
        check (DC 15 + double the spell’s level) to successfully cast the spell.
        Failure means that you lose the spell.</p><h2>Touch Spells in
        Combat</h2><p>Many spells have a range of touch. To use these spells,
        you cast the spell and then touch the subject. In the same round that
        you cast the spell, you may also touch (or attempt to touch) as a free
        action. You may take your move before casting the spell, after touching
        the target, or between casting the spell and touching the target. You
        can automatically touch one friend or use the spell on yourself, but to
        touch an opponent, you must succeed on an attack roll.</p><h2>Touch
        Attacks</h2><p>Touching an opponent with a touch spell is considered to
        be an armed attack and therefore does not provoke attacks of
        opportunity. The act of casting a spell, however, does provoke an attack
        of opportunity. Touch attacks come in two types: melee touch attacks and
        ranged touch attacks. You can score critical hits with either type of
        attack as long as the spell deals damage. Your opponent’s AC against a
        touch attack does not include any armor bonus, shield bonus, or natural
        armor bonus. His size modifier, Dexterity modifier, and deflection bonus
        (if any) all apply normally.</p><h2>Holding the Charge</h2><p>If you
        don’t discharge the spell in the round when you cast the spell, you can
        hold the charge indefinitely. You can continue to make touch attacks
        round after round. If you touch anything or anyone while holding a
        charge, even unintentionally, the spell discharges. If you cast another
        spell, the touch spell dissipates. You can touch one friend as a
        standard action or up to six friends as a full-round action.
        Alternatively, you may make a normal unarmed attack (or an attack with a
        natural weapon) while holding a charge. In this case, you aren’t
        considered armed and you provoke attacks of opportunity as normal for
        the attack. If your unarmed attack or natural weapon attack normally
        doesn’t provoke attacks of opportunity, neither does this attack. If the
        attack hits, you deal normal damage for your unarmed attack or natural
        weapon and the spell discharges. If the attack misses, you are still
        holding the charge.</p><h2>Ranged Touch Spells in Combat</h2><p>Some
        spells allow you to make a ranged touch attack as part of the casting of
        the spell. These attacks are made as part of the spell and do not
        require a separate action. Ranged touch attacks provoke an attack of
        opportunity, even if the spell that causes the attacks was cast
        defensively. Unless otherwise noted, ranged touch attacks cannot be held
        until a later turn.</p><h2>Dismiss a Spell</h2><p>Dismissing an active
        spell is a standard action that doesn’t provoke attacks of
        opportunity.</p>
      format: 1
    title:
      level: 1
      show: true
    type: text
sort: 0
