_id: YwZLtJ7Z59r1fx90
_key: '!journal!YwZLtJ7Z59r1fx90'
_stats:
  coreVersion: '12.331'
folder: UW4tzDX1A62qPAzr
name: 3.0.2.3. Doors
pages:
  - _id: JyoQKJX3fxhLUWRL
    _key: '!journal.pages!YwZLtJ7Z59r1fx90.JyoQKJX3fxhLUWRL'
    _stats:
      coreVersion: '12.331'
    name: 3.0.2.3. Doors
    sort: 0
    text:
      content: >-
        <p>@Compendium[pf-content.pf-rules.XOJpjqmBzwE4R4n0]{(Index)
        Environment}</p><p>@Compendium[pf-content.pf-rules.v5dHsJ6gCghRXQnL]{3.0.
        Dungeons}</p><ul><li>@Compendium[pf-content.pf-rules.oM2nAFzXnVFIS0WD]{3.0.2.1.
        Walls}</li><li>@Compendium[pf-content.pf-rules.J0yOXyPZB6M6ZUSi]{3.0.2.2.
        Floors}</li><li><strong>@Compendium[pf-content.pf-rules.YwZLtJ7Z59r1fx90]{3.0.2.3.
        Doors}</strong></li><li>@Compendium[pf-content.pf-rules.1OzU64OdBpfa0bJR]{3.0.2.4.
        Walls, Doors, and Detect
        Spells}</li><li>@Compendium[pf-content.pf-rules.tEkx7SQLAENe8KM8]{3.0.2.5.
        Stairs}</li><li>@Compendium[pf-content.pf-rules.1bkKb1E1A73Tp1IK]{3.0.2.6.
        Cave-Ins and Collapses (CR
        8)}</li><li>@Compendium[pf-content.pf-rules.hfI8oxi1ywlhrGfs]{3.0.2.7.
        Slimes, Molds, and Fungi}</li></ul><hr
        /><p>@Source[PZO1110;pages=412]</p><p>Doors in dungeons are much more
        than mere entrances and exits. Often they can be encounters all by
        themselves. Dungeon doors come in three basic types: wooden, stone, and
        iron.</p><h2>Wooden Doors</h2><p>Constructed of thick planks nailed
        together, sometimes bound with iron for strength (and to reduce swelling
        from dungeon dampness), wooden doors are the most common type. Wooden
        doors come in varying strengths: simple, good, and strong. Simple doors
        (break DC 15) are not meant to keep out motivated attackers. Good doors
        (break DC 18), while sturdy and long-lasting, are still not meant to
        take much punishment. Strong doors (break DC 25) are bound in iron and
        are a sturdy barrier to those attempting to get past them. Iron hinges
        fasten the door to its frame, and typically a circular pull-ring in the
        center is there to help open it. Sometimes, instead of a pull- ring, a
        door has an iron pull-bar on one or both sides of the door to serve as a
        handle. In inhabited dungeons, these doors are usually well-maintained
        (not stuck) and unlocked, although important areas are locked up if
        possible.</p><h2>Stone</h2><p>Carved from solid blocks of stone, these
        heavy, unwieldy doors are often built so that they pivot when opened,
        although dwarves and other skilled craftsfolk are able to fashion hinges
        strong enough to hold up a stone door. Secret doors concealed within a
        stone wall are usually stone doors. Otherwise, such doors stand as tough
        barriers protecting something important beyond. Thus, they are often
        locked or barred.</p><h2>Iron</h2><p>Rusted but sturdy, iron doors in a
        dungeon are hinged like wooden doors. These doors are the toughest form
        of nonmagical door. They are usually locked or barred.</p><h2>Breaking
        Doors</h2><p>Dungeon doors might be locked, trapped, reinforced, barred,
        magically sealed, or sometimes just stuck. All but the weakest
        characters can eventually knock down a door with a heavy tool such as a
        sledgehammer, and a number of spells and magic items give characters an
        easy way around a locked door. Attempts to literally chop down a door
        with a slashing or bludgeoning weapon use the hardness and hit points
        given in Table 13–2. When assigning a DC to an attempt to knock a door
        down, use the following as guidelines.</p><p>DC 10 or Lower: a door just
        about anyone can break open.</p><p>DC 11–15: a door that a strong person
        could break with one try and an average person might be able to break
        with one try.</p><p>DC 16–20: a door that almost anyone could break,
        given time.</p><p>DC 21–25: a door that only a strong or very strong
        person has a hope of breaking, probably not on the first try.</p><p>DC
        26 or Higher: a door that only an exceptionally strong person has a hope
        of breaking.</p><h2>Locks</h2><p>Dungeon doors are often locked, and
        thus the Disable Device skill comes in very handy. Locks are usually
        built into the door, either on the edge opposite the hinges or right in
        the middle of the door. Built-in locks either control an iron bar that
        juts out of the door and into the wall of its frame, or else a sliding
        iron bar or heavy wooden bar that rests behind the entire door. By
        contrast, padlocks are not built-in but usually run through two rings,
        one on the door and the other on the wall. More complex locks, such as
        combination locks and puzzle locks, are usually built into the door
        itself. Because such keyless locks are larger and more complex, they are
        typically only found in sturdy doors (strong wooden, stone, or iron
        doors).</p><p>The Disable Device DC to pick a lock often falls within
        the range of 20 to 30, although locks with lower or higher DCs can
        exist. A door can have more than one lock, each of which must be
        unlocked separately. Locks are often trapped, usually with poison
        needles that extend out to prick a rogue’s finger.</p><p>Breaking a lock
        is sometimes quicker than breaking the whole door. If a PC wants to
        whack at a lock with a weapon, treat the typical lock as having hardness
        15 and 30 hit points. A lock can only be broken if it can be attacked
        separately from the door, which means that a built-in lock is immune to
        this sort of treatment. In an occupied dungeon, every locked door should
        have a key somewhere.</p><p>A special door might have a lock with no
        key, instead requiring that the right combination of nearby levers must
        be manipulated or the right symbols must be pressed on a keypad in the
        correct sequence to open the door.</p><h2>Stuck Doors</h2><p>Dungeons
        are often damp, and sometimes doors get stuck, particularly wooden
        doors. Assume that about 10% of wooden doors and 5% of non-wooden doors
        are stuck. These numbers can be doubled (to 20% and 10%, respectively)
        for long-abandoned or neglected dungeons.</p><h2>Barred
        Doors</h2><p>When characters try to bash down a barred door, it’s the
        quality of the bar that matters, not the material the door is made of.
        It takes a DC 25 Strength check to break through a door with a wooden
        bar, and a DC 30 Strength check if the bar is made of iron. Characters
        can attack the door and destroy it instead, leaving the bar hanging in
        the now-open doorway.</p><h2>Magic Seals</h2><p>Spells such as arcane
        lock can discourage passage through a door. A door with an arcane lock
        spell on it is considered locked even if it doesn’t have a physical
        lock. It takes a knock spell, a dispel magic spell, or a successful
        Strength check to open such a door.</p><h2>Hinges</h2><p>Most doors have
        hinges, but sliding doors do not. They usually have tracks or grooves
        instead, allowing them to slide easily to one side.</p><h2>Standard
        Hinges</h2><p>These hinges are metal, joining one edge of the door to
        the door frame or wall. Remember that the door swings open toward the
        side with the hinges. (So, if the hinges are on the PCs’ side, the door
        opens toward them; otherwise it opens away from them.) Adventurers can
        take the hinges apart one at a time with successful Disable Device
        checks (assuming the hinges are on their side of the door, of course).
        Such a task has a DC of 20 because most hinges are rusted or stuck.
        Breaking a hinge is difficult. Most have hardness 10 and 30 hit points.
        The break DC for a hinge is the same as for breaking down the
        door.</p><h2>Nested Hinges</h2><p>These hinges are much more complex
        than ordinary hinges, and are found only in areas of excellent
        construction. These hinges are built into the wall and allow the door to
        swing open in either direction. PCs can’t get at the hinges to fool with
        them unless they break through the door frame or wall. Nested hinges are
        typically found on stone doors but sometimes on wooden or iron doors as
        well.</p><h2>Pivots</h2><p>Pivots aren’t really hinges at all, but
        simple knobs jutting from the top and bottom of the door that fit into
        holes in the door frame, allowing the door to spin. The advantages of
        pivots are that they can’t be dismantled like hinges and they’re simple
        to make. The disadvantage is that since the door pivots on its center of
        gravity (typically in the middle), nothing larger than half the door’s
        width can fit through without squeezing. Doors with pivots are usually
        stone and often quite wide to overcome this disadvantage. Another
        solution is to place the pivot toward one side and have the door be
        thicker at that end and thinner toward the other end so that it opens
        more like a normal door. Secret doors in walls often turn on pivots,
        since the lack of hinges makes it easier to hide the door’s presence.
        Pivots also allow objects such as bookcases to be used as secret
        doors.</p><h2>Secret Doors</h2><p>Disguised as a bare patch of wall (or
        floor or ceiling), a bookcase, a fireplace, or a fountain, a secret door
        leads to a secret passage or room. Someone examining the area finds a
        secret door, if one exists, on a successful Perception check (DC 20 for
        a typical secret door to DC 30 for a well-hidden secret
        door).</p><p>Many secret doors require special methods of opening, such
        as hidden buttons or pressure plates. Secret doors can open like normal
        doors, or they might pivot, slide, sink, rise, or even lower like a
        drawbridge to permit access. Builders might put a secret door low near
        the floor or high in a wall, making it difficult to find or reach.
        Wizards and sorcerers have a spell, phase door, that allows them to
        create a magic secret door that only they can use.</p><h2>Magic
        Doors</h2><p>Enchanted by the original builders, a door might speak to
        explorers, warning them away. It might be protected from harm,
        increasing its hardness or giving it more hit points as well as an
        improved saving throw bonus against disintegrate and similar spells. A
        magic door might not lead into the space behind it, but instead might be
        a portal to a faraway place or even another plane of existence. Other
        magic doors might require passwords or special keys to open
        them.</p><h2>Portcullises</h2><p>These special doors consist of iron or
        thick, ironbound wooden shafts that descend from recesses in the
        ceilings above archways. Sometimes a portcullis has crossbars that
        create a grid, sometimes not. Typically raised by means of a winch or a
        capstan, a portcullis can be dropped quickly, and the shafts end in
        spikes to discourage anyone from standing underneath (or from attempting
        to dive under it as it drops). Once it is dropped, a portcullis locks,
        unless it is so large that no normal person could lift it anyway. In any
        event, lifting a typical portcullis requires a DC 25 Strength
        check.</p><h2>Table 13-2: Doors</h2><table><thead><tr><td>
        </td><td>Break DC</td></tr><tr><td>Door Type</td><td>Typical
        Thickness</td><td>Hardness</td><td>Hit
        Points</td><td>Stuck</td><td>Locked</td></tr></thead><tbody><tr><td>Simple
        wooden</td><td>1 in.</td><td>5</td><td>10
        hp</td><td>13</td><td>15</td></tr><tr><td>Good wooden</td><td>1-1/2
        in.</td><td>5</td><td>15
        hp</td><td>16</td><td>18</td></tr><tr><td>Strong wooden</td><td>2
        in.</td><td>5</td><td>20
        hp</td><td>23</td><td>25</td></tr><tr><td>Stone</td><td>4
        in.</td><td>8</td><td>60
        hp</td><td>28</td><td>28</td></tr><tr><td>Iron</td><td>2
        in.</td><td>10</td><td>60
        hp</td><td>28</td><td>28</td></tr><tr><td>Portcullis, wooden</td><td>3
        in.</td><td>5</td><td>30
        hp</td><td>25*</td><td>25*</td></tr><tr><td>Portcullis, iron</td><td>2
        in.</td><td>10</td><td>60
        hp</td><td>25*</td><td>25*</td></tr><tr><td>Lock</td><td>—</td><td>15</td><td>30
        hp</td><td>—</td><td>—</td></tr><tr><td>Hinge</td><td>—</td><td>10</td><td>30
        hp</td><td>—</td><td>—</td></tr></tbody></table><p>*DC to lift. Use
        appropriate door figure for breaking.</p>
      format: 1
    title:
      level: 1
      show: true
    type: text
sort: 0
