_id: NzqehppEukjnQUfy
_key: '!journal!NzqehppEukjnQUfy'
_stats:
  coreVersion: '12.331'
folder: lnmu98MzhpeHCLdt
name: 9.2.3. Disguise
pages:
  - _id: unqUh9tQDIyONjCV
    _key: '!journal.pages!NzqehppEukjnQUfy.unqUh9tQDIyONjCV'
    _stats:
      coreVersion: '12.331'
    name: 9.2.3. Disguise
    sort: 0
    text:
      content: >-
        <p>@Compendium[pf-content.pf-rules.zUOuZVJH1nKufQSo]{(Index)
        Skills}</p><p>@Compendium[pf-content.pf-rules.EJtwDpvYymcGqQZk]{9.2.
        Skills in
        Conflict}</p><ul><li>@Compendium[pf-content.pf-rules.biGV1a41yIiORN2A]{9.2.1.
        Bluff}</li><li>@Compendium[pf-content.pf-rules.sNy0QWGaxnccWVTz]{9.2.2.
        Diplomacy}</li><li><strong>@Compendium[pf-content.pf-rules.NzqehppEukjnQUfy]{9.2.3.
        Disguise}</strong></li><li>@Compendium[pf-content.pf-rules.frhg7KrHN2tYhhkM]{9.2.4.
        Intimidate}</li><li>@Compendium[pf-content.pf-rules.NTIduFRh5A0p0hIu]{9.2.5.
        Perception and
        Stealth}</li><li>@Compendium[pf-content.pf-rules.B1ULzO6COuN6ILPA]{9.2.6.
        Sense
        Motive}</li><li>@Compendium[pf-content.pf-rules.92nmC7Usb2puToet]{9.2.7.
        Replacing Opposed Rolls}</li></ul><hr
        /><p>@Source[PZO1134;pages=186]</p><p><em>@UUID[Compendium.pf1.pf1e-rules.JournalEntry.x175kVqUfLGPt8tC.JournalEntryPage.xg25z3GIpS590NDW]{Disguise}</em></p><p>The
        uses of the Disguise skill are far more specific than those of Bluff and
        Diplomacy. The Disguise skill exists to allow characters to conceal
        their identity and to potentially pose as other
        characters.</p><p><strong>Not Always Opposed</strong>: The most
        important thing to note about the Disguise skill is that characters do
        not automatically get a Perception check to oppose it. Per the Core
        Rulebook, an opponent receives a Perception check only if the disguised
        character is actively drawing attention, if the perceiving character is
        actively suspicious of everyone, or if the disguised character is
        attempting to impersonate a particular person that the perceiving
        character recognizes. Under one of these circumstances, a perceiving
        character can attempt one Perception check right away and then another
        check each hour.</p><p><strong>A Single Disguise Check</strong>: Unlike
        most other skills, a character typically attempts a Disguise check only
        once when creating a physical disguise. Further Disguise checks might be
        necessary for things such as altering one’s voice or using appropriate
        mannerisms or phrasing, but the basic disguise doesn’t require further
        checks. The check result is supposed to be a secret that is revealed
        only the first time the disguise is truly tested, which can be tricky in
        the face of disguising characters who want their friends to tell them
        how good the disguise is. One way to handle this is to roll the Disguise
        check secretly only the first time it truly comes into opposition (see
        above), since the skill doesn’t indicate when the check first
        occurs.</p><p><strong>Disguise Is More Than Visual</strong>: Though the
        skill as presented in the Core Rulebook focuses on the visual aspects of
        disguise that a character prepares, later rules (such as the vocal
        alteration spell) have made it clear that there are other aspects,
        including voice, mannerisms, and phrasing. The trick is to distinguish
        between the use of the Bluff and the Disguise skills. Generally, Bluff
        checks cover telling actual lies to support a disguise, whereas Disguise
        checks cover the other aspects, such as imitating mannerisms and
        speech.</p><p><strong>Saw Through the Illusion</strong>: It is very
        tempting to use illusion or transmutation magic to augment a disguise,
        since the bonus is so high. As per the Core Rulebook, magic that
        penetrates an illusion or transmutation doesn’t automatically see
        through a mundane disguise, but it negates the magical components of the
        costume. Thus, a true master of disguise uses both types of trickery,
        and she also ensures that the person who notices her use of magic has a
        way to explain the fact that disguise magic was involved at all. For
        instance, a rogue might disguise herself as a noble with mundane means
        and then use disguise self to cloak herself in a glamer of that same
        noble, but more beautiful. Then, if someone sees through the illusion
        but not the mundane disguise, he would just think she was a vain noble
        instead of becoming suspicious due to the use of illusion magic and
        demanding a more thorough inspection.</p><p><strong>Simulacrum and
        Disguise</strong>: The caster of the simulacrum spell uses the Disguise
        skill to shape the form created. However, it is important to note that
        the Sense Motive check to detect a simulacrum is very easy at the level
        that simulacrum becomes available, so unless the simulacrum has a high
        Bluff modifier, it is still challenging to use a simulacrum as an
        impostor for long.</p>
      format: 1
    title:
      level: 1
      show: true
    type: text
sort: 0
