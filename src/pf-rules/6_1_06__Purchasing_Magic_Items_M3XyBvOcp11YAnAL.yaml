_id: M3XyBvOcp11YAnAL
_key: '!journal!M3XyBvOcp11YAnAL'
_stats:
  coreVersion: '12.331'
folder: 1SBSobU6ZnbSTMmG
name: 6.1.06. Purchasing Magic Items
pages:
  - _id: obhEPNbqwRExpl9O
    _key: '!journal.pages!M3XyBvOcp11YAnAL.obhEPNbqwRExpl9O'
    _stats:
      coreVersion: '12.331'
    name: 6.1.06. Purchasing Magic Items
    sort: 0
    text:
      content: >-
        <p>@Compendium[pf-content.pf-rules.4TX2Q1MaWt1B8298]{(Index)
        Items}</p><p>@Compendium[pf-content.pf-rules.s3ene84nkQAyhCrW]{6.1.
        Magic
        Items}</p><ul><li>@Compendium[pf-content.pf-rules.sP6qrGJREE0X596I]{6.1.01.
        Magic Items and Detect
        Magic}</li><li>@Compendium[pf-content.pf-rules.N3qNOnemt2nuACdl]{6.1.02.
        Using
        Items}</li><li>@Compendium[pf-content.pf-rules.VOtMqlqVoaZs7zu3]{6.1.03.
        Magic Items on the
        Body}</li><li>@Compendium[pf-content.pf-rules.EyKVuz8Ae2Bh5Z32]{6.1.04.
        Saving Throws Against Magic Item
        Powers}</li><li>@Compendium[pf-content.pf-rules.oXM5hPY9YSNGCEpU]{6.1.05.
        Damaging Magic
        Items}</li><li><strong>@Compendium[pf-content.pf-rules.M3XyBvOcp11YAnAL]{6.1.06.
        Purchasing Magic
        Items}</strong></li><li>@Compendium[pf-content.pf-rules.iZ6tZ7PFna0TiXkW]{6.1.07.
        Magic Item
        Descriptions}</li><li>@Compendium[pf-content.pf-rules.b91t5OrtW3axrmKd]{6.1.08.
        Magic
        Armor}</li><li>@Compendium[pf-content.pf-rules.TT91XHZCetT5yyVc]{6.1.09.
        Magic
        Weapons}</li><li>@Compendium[pf-content.pf-rules.itDodwwkCVnIf0jU]{6.1.10.
        Potions}</li><li>@Compendium[pf-content.pf-rules.LFs1l4pTdQgqrkjx]{6.1.11.
        Rings}</li><li>@Compendium[pf-content.pf-rules.LxBkrhoOTfPeNQ7Z]{6.1.12.
        Rods}</li><li>@Compendium[pf-content.pf-rules.2QvAhr3WucxaQNeN]{6.1.13.
        Scrolls}</li><li>@Compendium[pf-content.pf-rules.3mXUlZodNKkPDj0D]{6.1.14.
        Staves}</li><li>@Compendium[pf-content.pf-rules.wi435P7HqEEOQwFA]{6.1.15.
        Wands}</li><li>@Compendium[pf-content.pf-rules.HYNbOvjpMu4OZlfs]{6.1.16.
        Wondrous
        Items}</li><li>@Compendium[pf-content.pf-rules.muHuo2GwcCKyy4tP]{6.1.17.
        Intelligent
        Items}</li><li>@Compendium[pf-content.pf-rules.LaZ4cSkdCxMwDnXD]{6.1.18.
        Cursed
        Items}</li><li>@Compendium[pf-content.pf-rules.87rVetLgCkc8cm5d]{6.1.19.
        Artifacts}</li><li>@Compendium[pf-content.pf-rules.X6hkpqXjE3T3jRy2]{6.1.20.
        Magic Item Creation}</li></ul><hr
        /><p>@Source[PZO1110;pages=460]</p><p>Magic items are valuable, and most
        major cities have at least one or two purveyors of magic items, from a
        simple potion merchant to a weapon smith that specializes in magic
        swords. Of course, not every item in this book is available in every
        town.</p><p>The following guidelines are presented to help GMs determine
        what items are available in a given community. These guidelines assume a
        setting with an average level of magic. Some cities might deviate wildly
        from these baselines, subject to GM discretion. The GM should keep a
        list of what items are available from each merchant and should replenish
        the stocks on occasion to represent new acquisitions.</p><p>The number
        and types of magic items available in a community depend upon its size.
        Each community has a base value associated with it (see Table 15–1).
        There is a 75% chance that any item of that value or lower can be found
        for sale with little effort in that community. In addition, the
        community has a number of other items for sale. These items are randomly
        determined and are broken down by category (minor, medium, or major).
        After determining the number of items available in each category, refer
        to Table 15–2 to determine the type of each item (potion, scroll, ring,
        weapon, etc.) before moving on to the individual charts to determine the
        exact item. Reroll any items that fall below the community’s base
        value.</p><p>If you are running a campaign with low magic, reduce the
        base value and the number of items in each community by half. Campaigns
        with little or no magic might not have magic items for sale at all. GMs
        running these sorts of campaigns should make some adjustments to the
        challenges faced by the characters due to their lack of magic
        gear.</p><p>Campaigns with an abundance of magic items might have
        communities with twice the listed base value and random items available.
        Alternatively, all communities might count as one size category larger
        for the purposes of what items are available. In a campaign with very
        common magic, all magic items might be available for purchase in a
        metropolis.</p><p>Nonmagical items and gear are generally available in a
        community of any size unless the item is particularly expensive, such as
        full plate, or made of an unusual material, such as an adamantine
        longsword. These items should follow the base value guidelines to
        determine their availability, subject to GM discretion.</p><p><em>See
        Ultimate Equipement Compendium for roll tables</em></p><h2>Table 15-1:
        Available Magic Items</h2><table><thead><tr><td>Community
        Size</td><td>Base
        Value</td><td>Minor</td><td>Medium</td><td>Major</td></tr></thead><tbody><tr><td>Thorp</td><td>50
        gp</td><td>1d4
        items</td><td>—</td><td>—</td></tr><tr><td>Hamlet</td><td>200
        gp</td><td>1d6
        items</td><td>—</td><td>—</td></tr><tr><td>Village</td><td>500
        gp</td><td>2d4 items</td><td>1d4 items</td><td>—</td></tr><tr><td>Small
        town</td><td>1,000 gp</td><td>3d4 items</td><td>1d6
        items</td><td>—</td></tr><tr><td>Large town</td><td>2,000 gp</td><td>3d4
        items</td><td>2d4 items</td><td>1d4 items</td></tr><tr><td>Small
        city</td><td>4,000 gp</td><td>4d4 items</td><td>3d4 items</td><td>1d6
        items</td></tr><tr><td>Large city</td><td>8,000 gp</td><td>4d4
        items</td><td>3d4 items</td><td>2d4
        items</td></tr><tr><td>Metropolis</td><td>16,000
        gp</td><td>*</td><td>4d4 items</td><td>3d4
        items</td></tr></tbody></table><p>* In a metropolis, nearly all minor
        magic items are available.</p><h2>Table 15-2: Random Magic Item
        Generation</h2><table><thead><tr><td>Minor</td><td>Medium</td><td>Major</td><td>Item</td></tr></thead><tbody><tr><td>01-04</td><td>01-10</td><td>01-10</td><td>Armor
        and
        shields</td></tr><tr><td>05-09</td><td>11-20</td><td>11-20</td><td>Weapons</td></tr><tr><td>10-44</td><td>21-30</td><td>21-25</td><td>Potions</td></tr><tr><td>45-46</td><td>31-40</td><td>26-35</td><td>Rings</td></tr><tr><td>—</td><td>41-50</td><td>36-45</td><td>Rods</td></tr><tr><td>47-81</td><td>51-65</td><td>46-55</td><td>Scrolls</td></tr><tr><td>—</td><td>66-68</td><td>56-75</td><td>Staves</td></tr><tr><td>82-91</td><td>69-83</td><td>56-80</td><td>Wands</td></tr><tr><td>92-100</td><td>84-100</td><td>81-100</td><td>Wondrous
        items</td></tr></tbody></table>
      format: 1
    title:
      level: 1
      show: true
    type: text
sort: 0
