_id: WmHIEsLnruuSZcx0
_key: '!items!WmHIEsLnruuSZcx0'
_stats:
  coreVersion: '12.331'
img: systems/pf1/icons/feats/dodge.jpg
name: Charging
system:
  changes:
    - _id: w7uf5m99
      formula: '2'
      target: mattack
      type: circumstance
    - _id: 7a1naojg
      formula: '-2'
      target: ac
      type: circumstance
  contextNotes:
    - target: cmb
      text: +[[2]] to bull rush
  description:
    value: >-
      <aside><div><p><strong>FAQ</strong></p></div><div><p><strong>Can Vital
      Strike be used on a charge?</strong></p><p>No. Vital Strike can only be
      used as part of an attack action, which is a specific kind of standard
      action. Charging is a special kind of full-round action that includes the
      ability to make one melee attack, not one attack action.</p><p>[<a
      href="http://paizo.com/paizo/faq/v5748nruor1fm#v5748eaic9pyy"
      rel="nofollow">Source</a>]</p></div></aside><p>Charging is a special
      full-round action that allows you to move up to twice your speed and
      attack during the action. Charging, however, carries tight restrictions on
      how you can move.</p><p><strong>Movement During a
      Charge</strong></p><ul><li>You must move before your attack, not after.
      You must move at least 10 feet (2 squares) and may move up to double your
      speed directly toward the designated opponent. If you move a distance
      equal to your speed or less, you can also draw a weapon during a charge
      attack if your base attack bonus is at least +1.</li><li>You must have a
      clear path toward the opponent, and nothing can hinder your movement (such
      as difficult terrain or obstacles). You must move to the closest space
      from which you can attack the opponent. If this space is occupied or
      otherwise blocked, you can’t charge. If any line from your starting space
      to the ending space passes through a square that blocks movement, slows
      movement, or contains a creature (even an ally), you can’t charge.
      Helpless creatures don’t stop a charge.</li><li>If you don’t have line of
      sight to the opponent at the start of your turn, you can’t charge that
      opponent.</li><li>You can’t take a 5-foot step in the same round as a
      charge.</li><li>If you are able to take only a standard action on your
      turn, you can still charge, but you are only allowed to move up to your
      speed (instead of up to double your speed) and you cannot draw a weapon
      unless you possess the Quick Draw feat. You can’t use this option unless
      you are restricted to taking only a standard action on your
      turn.</li></ul><p><strong>Attacking on a Charge</strong></p><ul><li>After
      moving, you may make a single melee attack. You get a +2 bonus on the
      attack roll and take a –2 penalty to your AC until the start of your next
      turn.</li><li>A charging character gets a +2 bonus on combat maneuver
      attack rolls made to bull rush an opponent.</li><li>Even if you have extra
      attacks, such as from having a high enough base attack bonus or from using
      multiple weapons, you only get to make one attack during a
      charge.</li></ul><p><em>Lances and Charge Attacks:</em> A lance deals
      double damage if employed by a mounted character in a
      charge.</p><p><em>Weapons Readied against a Charge:</em> Spears, tridents,
      and other weapons with the brace feature deal double damage when readied
      (set) and used against a charging character.</p><h3>Swinging Charge from
      Ropes or Vines</h3><p>As a full-round action, you can swing using a rope,
      vine, or similar aid within reach toward an opponent and make a single
      melee attack. You must move at least 20 feet (4 squares) and you must
      start on elevation that is equal or higher than that of your opponent.
      Your movement provokes attacks of opportunity as normal.</p><p>This action
      is otherwise treated as a charge attack.</p>
  duration:
    end: turnStart
    units: round
    value: '1'
  level: null
type: buff
