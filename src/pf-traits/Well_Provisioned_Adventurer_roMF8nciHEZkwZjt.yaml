_id: roMF8nciHEZkwZjt
_key: '!items!roMF8nciHEZkwZjt'
_stats:
  coreVersion: '12.331'
folder: JADO8YDnM5A87OCO
img: icons/containers/bags/satchel-leather-brown.webp
name: Well-Provisioned Adventurer
system:
  description:
    value: >-
      <p><em>You always knew that one day you would leave your humble beginnings
      behind and become an adventurer, so you scrimped and saved, buying the
      necessary equipment one piece at a time until you had everything you
      needed.</em></p><p><strong>Benefits</strong>: Select one of the equipment
      packages below. If you select this trait during character creation, you do
      not receive any starting gold.</p><hr
      /><p><strong>Packages</strong></p><p><strong>Arcane Adept
      Package</strong></p><p><span
      id="ctl00_MainContent_DataListTypes_ctl13_LabelType"><strong>source</strong>
      </span>Adventurer's Armory 2 pg. 4</p><p>The arcane adept has collected
      useful magical gear to ensure her success on her adventures, and she
      prides herself on having just the right tool for the job. This equipment
      package is appropriate for an arcanist, sorcerer, witch, or wizard. Some
      bards and summoners might also find it attractive. This equipment package
      can also work for a magus if the 1st-level scrolls and wand are replaced
      with a masterwork melee weapon.</p><p><strong>Weapons</strong>: Light
      crossbow with 10 bolts, quarterstaff.</p><p><strong>Combat Gear</strong>:
      Acid (2), <em>scrolls of comprehend languages</em> (2), <em>scroll of
      detect secret doors</em>, <em>scroll of glitterdust</em>, <em>scrolls of
      identify</em> (2), <em>scroll of mount</em>, <em>scroll of rope
      trick</em>, thunderstone, <em>wand of mage armor</em> (16
      charges).</p><p><strong>Other Gear</strong>: Backpack, bedroll, belt
      pouch, candles (5), chalk (5 pieces), flint and steel, ink, inkpen, ioun
      torch, journal, magnifying glass, mess kit, scroll box, silk rope (50
      ft.), spell component pouch, trail rations (5 days), waterskin, 3
      gp.</p><p><strong>Total Weight</strong>: 45 lbs. (28-3/4 lbs. for a Small
      character).</p><hr /><p><strong>Blessed Warden
      Package</strong></p><p><span
      id="ctl00_MainContent_DataListTypes_ctl13_LabelType"><strong>source</strong>
      </span>Plane-Hopper's Handbook pg. 10</p><p>A blessed warden is prepared
      to protect herself against the horrors of evil-aligned planes. This
      equipment package is suitable for clerics, druids, inquisitors, oracles,
      and paladins. The specific gear is appropriate for those visiting the
      chaotic evil Abyss but can be adjusted to suit other evilaligned planes,
      such as swapping the scrolls of protection from chaos for scrolls of
      protection from law for a package designed for travel to
      Hell.</p><p><strong>Armor</strong>: Masterwork chain
      shirt.</p><p><strong>Weapons</strong>: Cold iron shortspear, light
      crossbow with 20 crossbow bolts.</p><p><strong>Combat Gear</strong>: Holy
      water (3), <em>scrolls of cure light wounds</em> (2), <em>scrolls of
      protection from chaos</em> (2), <em>scrolls of protection from evil</em>
      (2), <em>scroll of remove curse</em>.</p><p><strong>Other Gear</strong>:
      Backpack, bedroll, candles (5), chalk (5 pieces), crowbar, ioun torch,
      mess kit, shovel, silk rope (50 ft.), spell component pouch, trail rations
      (5 days), waterskin, wooden holy symbol, 13 gp.</p><p><strong>Total
      Weight</strong>: 74 lbs. (44-1/2 lbs. for a Small character).</p><hr
      /><p><strong>Corporeal Warrior Package</strong></p><p><span
      id="ctl00_MainContent_DataListTypes_ctl13_LabelType"><strong>source</strong>
      </span>Plane-Hopper's Handbook pg. 10</p><p>Normal equipment is ill suited
      to fighting intangible foes, and a warrior bound for the Ethereal Plane
      must be prepared for incorporeal threats lurking in the ever-present
      mists. This equipment package is suitable for fighters and paladins. If
      the PC can cast spells, add a spell component
      pouch.</p><p><strong>Armor</strong>: Masterwork
      breastplate.</p><p><strong>Weapons</strong>: Heavy crossbow with 10 bolts,
      heavy mace, masterwork longsword.</p><p><strong>Combat Gear</strong>:
      Alchemist’s fire, <em>potion of mage armor</em>, <em>potion of
      shield</em>, <em>oil of magic weapon</em>.</p><p><strong>Other
      Gear</strong>: Backpack, bedroll, candles (5), chalk (5 pieces), crowbar,
      ioun torch, mess kit, shovel, silk rope (50 ft.), trail rations (5 days),
      waterskin, 10 gp.</p><p><strong>Total Weight</strong>: 87 lbs. (48-1/2
      lbs. for a Small character).</p><hr /><p><strong>Daring Bravo
      Package</strong></p><p><span
      id="ctl00_MainContent_DataListTypes_ctl13_LabelType"><strong>source</strong>
      </span>Adventurer's Armory 2 pg. 4</p><p>The daring bravo is equally adept
      in social situations and combat, with the right equipment to move fluidly
      from one to the other. Such adventurers are known as much for their flair
      and panache as their martial prowess, and the daring bravo’s equipment is
      often ornately ornamented or personalized. This equipment package is good
      for a bard, fighter, rogue, swashbuckler, or vigilante. Certain cavaliers,
      investigators, skalds, or even paladins may also find it appealing. If the
      PC can cast spells, add a spell component
      pouch.</p><p><strong>Armor</strong>: Masterwork chain
      shirt.</p><p><strong>Weapons</strong>: Light crossbow with 10 bolts,
      masterwork rapier, sap, alchemical silver dagger.</p><p><strong>Combat
      Gear</strong>: Acid (2), alchemist’s kindness, <em>potions of cure light
      wounds</em> (2), sunrods (3).</p><p><strong>Other Gear</strong>: Backpack,
      bedroll, belt pouch, bottle of fine wine, chalk (5 pieces), courtier’s
      outfit with 50 gp in jewelry, flask, flint and steel, grooming kit,
      masterwork musical instrument, mess kit, mirror, perfume or cologne, sack,
      signet ring, silk rope (50 ft.), trail rations (5 days), waterskin,
      whetstone, 5 gp.</p><p><strong>Total Weight</strong>: 76-1/2 lbs. (40-1/2
      lbs. for a Small character).</p><hr /><p><strong>Holy Warrior
      Package</strong></p><p><span
      id="ctl00_MainContent_DataListTypes_ctl13_LabelType"><strong>source</strong>
      </span>Adventurer's Armory 2 pg. 4</p><p>The holy warrior is prepared to
      use her might and zeal to take the fight to the enemy, but she understands
      the importance of having the right equipment to overcome the resistances
      of her monstrous foes. This equipment package is well-suited to a cleric,
      inquisitor, paladin, warpriest, or even a fighter with a religious
      background. It’s especially suited to characters who focus on supporting
      their party members and making them more effective in a fight. Even more
      than with other equipment packages, the GM should consider substituting
      the masterwork longsword in this package with a masterwork melee weapon
      appropriate to the PC’s faith. If the PC can cast spells, add a spell
      component pouch.</p><p><strong>Armor</strong>: Heavy steel shield,
      masterwork breastplate.</p><p><strong>Weapons</strong>: Cold iron
      morningstar, heavy crossbow with 10 bolts, masterwork
      longsword.</p><p><strong>Combat Gear</strong>: Alchemist’s fire (3), holy
      water (4), <em>oil of bless weapon</em>, <em>potions of cure light
      wounds</em> (2), <em>potion of protection from evil</em>, sunrods
      (3).</p><p><strong>Other Gear</strong>: Backpack, bedroll, belt pouch,
      candles (5), chalk (5 pieces), crowbar, flint and steel, holy symbol
      (silver), mess kit, sack, silk rope (50 ft.), trail rations (5 days),
      waterskin, 7 gp.</p><p><strong>Total Weight</strong>: 102 lbs. (57-1/4
      lbs. for a Small character).</p><hr /><p><strong>Lore Seeker
      Package</strong></p><p><span
      id="ctl00_MainContent_DataListTypes_ctl13_LabelType"><strong>source</strong>
      </span>Adventurer's Armory 2 pg. 5</p><p>The lore seeker has the equipment
      necessary to delve into ancient ruins searching for lost knowledge. As
      this equipment package contains little by way of armor or weapons, it is
      most appropriate for alchemists, bards, monks, sorcerers, and wizards. If
      the PC can cast spells, add a spell component pouch and a holy symbol (if
      required).</p><p><strong>Weapons</strong>: Light crossbow with 10 bolts,
      quarterstaff, silver dagger.</p><p><strong>Combat Gear</strong>:
      Alchemist’s fire (3), antitoxin, <em>oil of erase</em>, <em>potions of
      cure light wounds</em> (2), <em>potion of protection from evil</em>,
      tanglefoot bag, thunderstone.</p><p><strong>Other Gear</strong>: Backpack,
      bedroll, belt pouch, candles (5), chalk (5 pieces), compass, crowbar,
      everburning torch, flint and steel, grappling hook, ink, inkpens (2),
      journals (2), magnifying glass, mapmaker’s kit, mess kit, sack, signal
      whistle, silk rope (50 ft.), trail rations (5 days), traveler’s any-tool,
      waterskin, 8 gp.</p><p><strong>Total Weight</strong>: 44 lbs. (27-3/4 lbs.
      for a Small character).</p><hr /><p><strong>Mystic Guide
      Package</strong></p><p><span
      id="ctl00_MainContent_DataListTypes_ctl13_LabelType"><strong>source</strong>
      </span>Adventurer's Armory 2 pg. 5</p><p>This equipment package is
      designed to provide the most aid to divine casters, such as clerics,
      druids, oracles, and inquisitors, who prefer to help their companions from
      behind the front lines of a fight.</p><p><strong>Armor</strong>: Leather
      armor, light wooden shield.</p><p><strong>Weapons</strong>: Shortspear,
      sling with 10 bullets.</p><p><strong>Combat Gear</strong>: Antitoxin,
      <em>bead of blessing</em> (as a <em>lesser strand of prayer beads</em>
      without the <em>bead of healing</em>), holy water (2), <em>scrolls of cure
      light wounds</em> (2), <em>scroll of endure
      elements</em>.</p><p><strong>Other Gear</strong>: Backpack, bedroll, belt
      pouch, candles (5), chalk (5 pieces), flint and steel, healer’s kit, holy
      symbol (silver), ioun torch, mess kit, mirror, sack, shovel, silk rope (50
      ft.), smelling salts, soap, spell component pouch, trail rations (5 days),
      waterskin, 4 sp.</p><p><strong>Total Weight</strong>: 67 lbs. (39-1/4 lbs.
      for a Small character).</p><hr /><p><strong>Planar Traveler
      Package</strong></p><p><span
      id="ctl00_MainContent_DataListTypes_ctl13_LabelType"><strong>source</strong>
      </span>Plane-Hopper's Handbook pg. 10</p><p>This package equipment
      prepares planar travelers for the challenges of a variety of planar
      destinations. This package works particularly well for lightly armored
      combatants, such as bards and rogues. If the PC purchasing it can cast
      spells, add a spell component pouch as well.</p><p><strong>Armor</strong>:
      Masterwork studded leather, light wooden
      shield.</p><p><strong>Weapons</strong>: Composite longbow with 20 arrows,
      masterwork morningstar.</p><p><strong>Combat Gear</strong>: Alchemist’s
      fire (3), <em>potion of air bubble</em>, <em>potions of cure light
      wounds</em> (2), <em>potions of endure elements</em>
      (2).</p><p><strong>Other Gear</strong>: Backpack, bedroll, candles (5),
      chalk (5 pieces), crowbar, ioun torch, mess kit, shovel, silk rope (50
      ft.), trail rations (5 days), waterskin, 11 gp.</p><p><strong>Total
      Weight</strong>: 75 lbs. (42-1/2 lbs. for a Small character).</p><hr
      /><p><strong>Questing Knight Package</strong></p><p><span
      id="ctl00_MainContent_DataListTypes_ctl13_LabelType"><strong>source</strong>
      </span>Adventurer's Armory 2 pg. 5</p><p>If not descended from nobility,
      the questing knight certainly looks like he fits the part. This equipment
      package is useful for cavaliers, fighters, and paladins. If the PC can
      cast spells, add a spell component pouch and a holy symbol (if required).
      If the PC gains a mount from a class feature, remove the horse from the
      list below and replace the lance with a masterwork
      lance.</p><p><strong>Armor</strong>: Half-plate, heavy wooden
      shield.</p><p><strong>Weapons</strong>: Javelins (4), lance,
      longsword.</p><p><strong>Combat Gear</strong>: Sunrods
      (2).</p><p><strong>Other Gear</strong>: Backpack, banner, bedroll, belt
      pouch, flask, flint and steel, mess kit, pole (10 ft.), sack, shovel,
      signet ring, silk rope (50 ft.), trail rations (5 days), waterskin,
      whetstone, 8 gp.</p><p><strong>Mount</strong>: Heavy horse (combat
      trained) with bit and bridle, military saddle, and
      saddlebags.</p><p><strong>Total Weight</strong>: 127-1/2 lbs. (73-3/4 lbs.
      for a Small character).</p><hr /><p><strong>Shadowy Stalker
      Package</strong></p><p><span
      id="ctl00_MainContent_DataListTypes_ctl13_LabelType"><strong>source</strong>
      </span>Adventurer's Armory 2 pg. 5</p><p>Skulking through a city or a
      dungeon, the shadowy stalker is equipped to strike quickly and fade away.
      This equipment package is appropriate for stealthy characters such as
      rangers, rogues, and slayers, and for some bards and investigators. If the
      PC can cast spells, add a spell component
      pouch.</p><p><strong>Armor</strong>: Masterwork leather
      armor.</p><p><strong>Weapons</strong>: Daggers (3), masterwork sickle,
      sap.</p><p><strong>Combat Gear</strong>: Alchemist’s fire (3), smokesticks
      (2), sunrods (2).</p><p><strong>Other Gear</strong>: Bedroll, belt pouch,
      caltrops, candles (5), chalk (5 pieces), disguise kit, flint and steel,
      glass cutter, masterwork backpack, masterwork thieves’ tools, mess kit,
      sack, silk rope (50 ft.), sleeves of many garments, trail rations (5
      days), waterskin, 1 gp.</p><p><strong>Total Weight</strong>: 63 lbs.
      (33-1/4 lbs. for a Small character).</p><hr /><p><strong>Wilderness
      Wanderer Package</strong></p><p><span
      id="ctl00_MainContent_DataListTypes_ctl13_LabelType"><strong>source</strong>
      </span>Adventurer's Armory 2 pg. 5</p><p>This equipment package is
      appropriate for any lightly-armored combatant in the wild, such as a
      barbarian, hunter, or ranger. If the PC can cast spells, add a spell
      component pouch and holly and mistletoe. The GM might consider altering
      which weapon is masterwork, based on the PC’s specialization. For example,
      an archery-focused ranger might prefer a masterwork composite longbow over
      a masterwork greataxe.</p><p><strong>Armor</strong>: Light wooden
      quickdraw shield, masterwork studded leather
      armor.</p><p><strong>Weapons</strong>: Cold iron flail, composite longbow
      with 20 arrows, masterwork greataxe, short sword.</p><p><strong>Combat
      Gear</strong>: Alchemist’s fire (2), antitoxin, <em>potion of keen
      senses</em>, sunrods (3).</p><p><strong>Other Gear</strong>: Backpack,
      bedroll, belt pouch, chalk (5 pieces), climber’s kit, cold-weather outfit,
      fishhook, flint and steel, mess kit, pole (10 ft.), shovel, signal
      whistle, silk rope (50 ft.), trail rations (5 days), waterskin, 2
      sp.</p><p><strong>Total Weight</strong>: 108-1/2 lbs. (63-1/4 lbs. for a
      Small character).</p>
  sources:
    - id: PZO9481
      pages: '4'
  subType: trait
  traitType: equipment
type: feat
