_id: 0pkd3f5nOjwJYT2t
_key: '!items!0pkd3f5nOjwJYT2t'
_stats:
  coreVersion: '12.331'
folder: n1kBJ9yG8vwL71rY
img: systems/pf1/icons/feats/shatter-defenses.jpg
name: Arcane Malignancies (Changeling)
system:
  description:
    value: >-
      <p><em>As changelings sense the awakening of the eerie powers they
      inherited from their hag progenitors, they are often mortified to discover
      terrifying and debilitating alterations to their physique. While the call
      seizes the changelings’ minds around the time their bodies begin maturing,
      the arcane malignancies that mar their features and extremities have
      nothing to do with puberty.</em></p><p><em>Not every changeling develops a
      malignancy, and when a changeling does develop a malignancy, it is often
      unrelated to whatever traits her hag mother has. These transformations are
      more akin to a magical allergy, manifesting in response to the well of
      arcane energy bubbling away in the changeling’s
      core.</em></p><p><strong>Effects</strong>: A changeling character can take
      an arcane malignancy as a drawback in order to select an additional trait
      at character creation. Randomly determine the arcane malignancy by rolling
      a d% and consulting the table below.</p><table class="inner"
      style="width:98.9601%"><tbody><tr><td
      style="width:8.21743%"><p><strong>d%</strong></p></td><td
      style="width:91.4907%"><p><strong>Effect</strong></p></td></tr><tr><td
      style="width:8.21743%"><p>1–2</p></td><td style="width:91.4907%"><p>The
      smallest three toes on each of your feet have fused together. You take a
      –2 penalty on Acrobatics, Climb, and Perform (dance)
      checks.</p></td></tr><tr><td style="width:8.21743%"><p>3–4</p></td><td
      style="width:91.4907%"><p>Your incisors grow so long and sharp that you
      have difficulty eating. Eating takes twice as long. Consuming a potion or
      other magical foodstuff requires a full-round action.</p></td></tr><tr><td
      style="width:8.21743%"><p>5–7</p></td><td style="width:91.4907%"><p>You
      have an extra digit on each hand, which hampers your fine motor skills.
      You take a –2 penalty on Craft, Disable Device, and Sleight of Hand
      checks.</p></td></tr><tr><td style="width:8.21743%"><p>8–10</p></td><td
      style="width:91.4907%"><p>Your limbs and torso are covered with piscine
      scales that you must constantly moisten. If more than 1 day passes without
      you applying at least a gallon of water to them, you take 1d3 points of
      damage each hour until you do so.</p></td></tr><tr><td
      style="width:8.21743%"><p>11–13</p></td><td style="width:91.4907%"><p>Your
      skin bears a sizable scarlet mark that flushes and lightens with your
      moods. You take a –2 penalty on Bluff and Disguise
      checks.</p></td></tr><tr><td style="width:8.21743%"><p>14–16</p></td><td
      style="width:91.4907%"><p>One of your eyes has grown enlarged and
      developed a pale, milky film. Daylight seems uncomfortably bright to you.
      You take a –2 penalty on saving throws against light-based spells and
      effects, and you are automatically dazzled for 1d6 rounds when exposed to
      bright light or sunlight.</p></td></tr><tr><td
      style="width:8.21743%"><p>17–19</p></td><td style="width:91.4907%"><p>The
      soles of your feet are flat, and you tire quickly. You can hustle for only
      a half hour before becoming fatigued, and every 4 hours spent traveling by
      foot in a day deals 1 point of Constitution damage.</p></td></tr><tr><td
      style="width:8.21743%"><p>20–22</p></td><td style="width:91.4907%"><p>Your
      already pallid complexion grows unusually sensitive to the rays of the
      sun. Exposure to direct sunlight for more than 1 hour at a time makes you
      sickened for 24 hours.</p></td></tr><tr><td
      style="width:8.21743%"><p>23–25</p></td><td style="width:91.4907%"><p>The
      tip of your tongue is forked and stiff. You have difficulty making
      yourself understood. You take a –2 penalty on Bluff, Diplomacy, and
      Disguise checks to fool or persuade others.</p></td></tr><tr><td
      style="width:8.21743%"><p>26–28</p></td><td style="width:91.4907%"><p>The
      painful and stiff swelling of your joints puts you at a disadvantage in
      combat. You take a –2 penalty on combat maneuver checks and Escape Artist
      checks to free yourself when grappled or pinned.</p></td></tr><tr><td
      style="width:8.21743%"><p>29–31</p></td><td style="width:91.4907%"><p>Your
      ears have stretched to an abnormal size, making you overly sensitive to
      loud noises. Whenever you are dealt sonic damage, you take 1 additional
      point of damage, and you are deafened for 1 minute whenever you take
      damage from any sonic effect.</p></td></tr><tr><td
      style="width:8.21743%"><p>32–33</p></td><td style="width:91.4907%"><p>The
      pupils of your eyes resemble a cat’s, and beasts cannot abide your
      presence. You take a –2 penalty on Handle Animal and Ride
      checks.</p></td></tr><tr><td style="width:8.21743%"><p>34–35</p></td><td
      style="width:91.4907%"><p>You grow velvety-soft fur along your back, arms,
      and legs that chafes and catches under clothing and armor, increasing the
      armor check penalty of any armor by 1 and applying a –1 armor check
      penalty to ordinary clothing. Clothing and armor can be specially fitted
      for you, eliminating the increased penalty but increasing their price by
      50 gp.</p></td></tr><tr><td style="width:8.21743%"><p>36–37</p></td><td
      style="width:91.4907%"><p>You grow an extra row of sharp, crooked teeth.
      The uneven set of your jaws gives you constant headaches. You take a –2
      penalty on concentration checks and saving throws against pain
      effects.</p></td></tr><tr><td style="width:8.21743%"><p>38–40</p></td><td
      style="width:91.4907%"><p>It is nearly impossible to find armor that fits
      your concave chest and unusually narrow, sloping shoulders. The armor
      check penalty of any armor you wear increases by 1, and you take a –1
      penalty to AC when wearing medium or heavy armor.</p></td></tr><tr><td
      style="width:8.21743%"><p>41–42</p></td><td style="width:91.4907%"><p>Your
      throat balloons out like a frog’s, and your voice comes out as an odd
      croak. You take a –2 penalty on Diplomacy checks and on Perform (act,
      oratory, and sing) checks.</p></td></tr><tr><td
      style="width:8.21743%"><p>43–45</p></td><td style="width:91.4907%"><p>An
      oozing sore has developed on your face that refuses to heal and makes you
      more susceptible to illness. You take a –2 penalty on saving throws
      against disease.</p></td></tr><tr><td
      style="width:8.21743%"><p>46–48</p></td><td style="width:91.4907%"><p>Your
      flesh grows bark, and your major joints have become gnarled and knotted
      like tree limbs, causing you to move slowly and stiffly. You take a –2
      penalty on Reflex saves.</p></td></tr><tr><td
      style="width:8.21743%"><p>49–51</p></td><td style="width:91.4907%"><p>Your
      thin, almost translucent skin is delicate and tears easily. You take 1
      point of bleed damage from slashing attacks that deal at least 1 point of
      damage to you.</p></td></tr><tr><td
      style="width:8.21743%"><p>52–54</p></td><td style="width:91.4907%"><p>A
      massive patch of multicolored warts covers much of your face. People
      cannot help but stare, and they always remember you once they’ve seen you.
      You take a –4 penalty on Disguise checks.</p></td></tr><tr><td
      style="width:8.21743%"><p>55–57</p></td><td style="width:91.4907%"><p>Your
      unruly hair grows with alarming speed. If you do not spend 1 hour trimming
      and grooming your hair every 48 hours, it snarls in nearby foliage
      whenever you are outdoors, imposing a –2 penalty to your Armor Class and
      on attack rolls and Stealth checks.</p></td></tr><tr><td
      style="width:8.21743%"><p>58–60</p></td><td style="width:91.4907%"><p>Your
      limbs are thin and frail. You take a –2 penalty to CMD against disarm,
      grapple, and trip attempts.</p></td></tr><tr><td
      style="width:8.21743%"><p>61–63</p></td><td style="width:91.4907%"><p>Your
      bones shatter as easily as glass. You take 2 additional points of damage
      each time you are hit by an attack made with a bludgeoning
      weapon.</p></td></tr><tr><td style="width:8.21743%"><p>64–65</p></td><td
      style="width:91.4907%"><p>One arm is turning to stone, though you retain
      limited mobility. You take a –4 penalty on all skill checks requiring the
      use of two hands and on attack rolls when wielding a two-handed weapon or
      fighting with two weapons.</p></td></tr><tr><td
      style="width:8.21743%"><p>66–67</p></td><td style="width:91.4907%"><p>You
      develop an allergy to one spell per spell level from the witch spell list
      (determined by your GM). Casting or being the target of a spell you’re
      allergic to causes you to break out in a painful rash. This rash deals 1d4
      points of damage per spell level,and imposes a penalty on your Charisma
      equal to the level of the spell. The rash fades after a number of hours
      equal to the level of the spell.</p></td></tr><tr><td
      style="width:8.21743%"><p>68–69</p></td><td style="width:91.4907%"><p>The
      joints in your unnaturally long thumbs have fused, and you often fumble
      objects. When you retrieve a stored item, you have a 10% chance of
      dropping it.</p></td></tr><tr><td
      style="width:8.21743%"><p>70–72</p></td><td style="width:91.4907%"><p>Your
      knees bend backward like a bird’s, which makes many athletic movements
      difficult to master. You take a –2 penalty on Climb and Swim checks, and
      your running speed is only three times your regular walking
      speed.</p></td></tr><tr><td style="width:8.21743%"><p>73–75</p></td><td
      style="width:91.4907%"><p>A tenacious patch of fungus has sprouted on your
      back, weakening your system against toxins. You take a –2 penalty on saves
      against poison effects.</p></td></tr><tr><td
      style="width:8.21743%"><p>76–77</p></td><td style="width:91.4907%"><p>Your
      misaligned eyes affect your ability to judge distances. You take a –2
      penalty on ranged attack and damage rolls.</p></td></tr><tr><td
      style="width:8.21743%"><p>78–80</p></td><td style="width:91.4907%"><p>When
      you get flustered, heat builds up in your face until it glows with inner
      fire. You take a –4 penalty on Diplomacy and Perform checks when in the
      presence of 5 or more creatures.</p></td></tr><tr><td
      style="width:8.21743%"><p>81–83</p></td><td style="width:91.4907%"><p>Your
      ears curl up like dried leaves, and you experience difficulty picking up
      sounds. You take a –2 penalty on hearing-based Perception
      checks.</p></td></tr><tr><td style="width:8.21743%"><p>84–86</p></td><td
      style="width:91.4907%"><p>Your curving, clawlike fingernails grow so long
      so quickly that they impede you in dangerous situations. You take a –2
      penalty on Escape Artist and Sleight of Hand checks.</p></td></tr><tr><td
      style="width:8.21743%"><p>87–89</p></td><td style="width:91.4907%"><p>One
      of your legs measures noticeably longer than the other, and you lose your
      balance easily. You take a –2 penalty to CMD against bull rush and trip
      attempts.</p></td></tr><tr><td style="width:8.21743%"><p>90–92</p></td><td
      style="width:91.4907%"><p>Your eyes have swollen to mere slits. You take a
      –2 penalty on sight-based Perception checks.</p></td></tr><tr><td
      style="width:8.21743%"><p>93–96</p></td><td style="width:91.4907%"><p>You
      develop a shaggy pelt about your neck and shoulders that makes you
      overheat quickly. You take a –4 penalty on checks and saves to resist
      environmental heat effects.</p></td></tr><tr><td
      style="width:8.21743%"><p>97–98</p></td><td style="width:91.4907%"><p>You
      grow a tail that you can’t easily conceal and that thrashes violently at
      inconvenient moments. You take a –2 penalty on Disguise and Stealth
      checks.</p></td></tr><tr><td style="width:8.21743%"><p>99–100</p></td><td
      style="width:91.4907%"><p>You grow vestigial gills. While they grant you
      no ability to breathe in water, they make it harder for you to catch your
      breath. When you are fatigued, it takes 12 hours of complete rest to
      recover. When you are exhausted, it takes 2 hours of complete rest to
      become fatigued.</p></td></tr></tbody></table>
  sources:
    - id: PZO9485
      pages: '10'
  subType: trait
  tags:
    - Changeling
  traitType: drawback
type: feat
