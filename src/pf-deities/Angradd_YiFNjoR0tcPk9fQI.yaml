_id: YiFNjoR0tcPk9fQI
_key: '!journal!YiFNjoR0tcPk9fQI'
_stats:
  coreVersion: '12.331'
name: Angradd
pages:
  - _id: 9EsLyRy5Ajpl1Qco
    _key: '!journal.pages!YiFNjoR0tcPk9fQI.9EsLyRy5Ajpl1Qco'
    _stats:
      coreVersion: '12.331'
    name: Angradd
    sort: 0
    text:
      content: >-
        <h1>Angradd</h1><h2>The Forge-Fire</h2>@Source[PZO9267;pages=320] <br
        /><strong>Pathfinder Wiki</strong> <a
        href="http://www.pathfinderwiki.com/wiki/Angradd">Angradd</a><h3>Details</h3><p><strong>Alignment</strong>
        LG<br /><strong>Pantheon</strong> Dwarven Deities<br /><strong>Areas of
        Concern</strong> Fire, tradition, war<br /><strong>Domains</strong>
        Fire, Good, Law, War<br /><strong>Subdomains</strong> Archon (Good),
        Archon (Law), Ash, Smoke, Tactics<br /><em>* Requires the <u>Acolyte of
        Apocrypha</u> trait.</em><br /><strong>Favored Weapon</strong>
        Greataxe<br /><strong>Symbol</strong> Smoking forge<br /><strong>Sacred
        Animal(s)</strong> Boar<br /><strong>Sacred Color(s)</strong> Gray,
        red</p><h3>Obedience</h3><p>Build a fire, preferably as part of a ritual
        that includes your closest allies and comrades. Use that fire's smoke to
        blacken the head of an axe while reciting battle litanies from
        <em>Angradd's Tempering (or Torag's <em>Hammer and Tongs</em>,
        <em>Magrim's Measure</em>, or <em>Journey of the Last Breath</em> if you
        also wish to show reverence for Angradd's respected elder brothers).
        Clean the ash from the axe with a cloth (traditionally a red cloth,
        often granted to you as a gift from a family member). Successful melee
        attacks made with that axe by a lawful good, lawful neutral, or neutral
        good creature deal an additional 1 point of fire damage. (This damage is
        not multiplied on a critical hit.)</em></p><h2>Boons - Deific
        Obedience</h2><h3>Evangelist</h3><p>@Source[PZO90144;pages=66]<br
        /><strong>1: Smoke of Righteous Flame (Sp)</strong> <em>burning hands
         </em> 3/day, <em>discovery torch</em> 2/day, or <em>ash storm</em> 1/day<br
        /><strong>2: Bulwark Against Evil (Su)</strong> You are a shining beacon
        against the forces of evil and have internalized much of Angradd's fury
        against those who are vile and cruel. You gain DR 5/-, which applies
        only against attacks from creatures with the evil subtype or attacks
        that can bypass DR/evil.<br /><strong>3: General of the Just
        (Su)</strong> You are a mobile focus of the divine strategy of Angradd,
        able to turn the tide of battle by inspiring your allies to
        instinctively work together. Allies within 60 feet (not including
        yourself) gain a +2 morale bonus on initiative checks, on melee damage
        rolls while flanking, and to AC when adjacent to you or at least one
        other ally of
        yours.</em></p><h3>Exalted</h3><p>@Source[PZO90144;pages=66]<br
        /><strong>1: Torch Against Evil (Sp)</strong> <em>weapons against evil
         </em> 3/day, <em>blistering invective</em> 2/day, or <em>flame arrow</em>
        1/day<br /><strong>2: Blazing Righteousness (Su)</strong> You can
        channel Angradd's divine flames to burn agents of malevolence. As a free
        action, you can wreathe yourself in flames, granting you resistance 20
        against fire and causing any evil creature adjacent to you at the
        beginning of your turn to take 2d6 points of damage (half of which is
        fire damage). Dismissing this ability is also a free action. This
        ability lasts a number of rounds per day equal to 1 + 1 per 4 Hit Dice
        you have (maximum 6 rounds). The rounds don't need to be consecutive.<br
        /><strong>3: Boon Companions (Sp)</strong> Three times per day, you can
        summon an ally to assist you in the fight against evil. This acts as a
        <em>summon nature's ally</em> or <em>summon monster</em> spell with a
        spell level equal to half your Hit Dice (maximum spell level 9th). The
        summoned creatures must be lawful good, lawful neutral, or neutral good,
        and they gain the fire, good, and lawful subtypes. You can communicate
        with the summoned creatures as if you had a shared
        language.</em></p><h3>Sentinel</h3><p>@Source[PZO90144;pages=66]<br
        /><strong>1: Purifying Fire (Sp)</strong> <em>burning hands</em> 3/day,
        <em>flames of the faithful</em> 2/day, or <em>fireball</em> 1/day<br
        /><strong>2: Cut Down to Size (Su)</strong> Any axe you wield shines
        with red and gold light, as the <em>light</em> spell. You gain Cleave as
        a bonus feat with any axe. If you already have Cleave, you gain the
        first feat from the following list that you don't already have when
        wielding an axe: Great Cleave, Cleaving Finish, Improved Cleaving
        Finish.<br /><strong>3: Army of One (Sp)</strong> Once per day you can
        summon a small army of allies to aid you in combat. You can summon
        either one greataxe made of pure force per 4 Hit Dice you have (each as
        a <em>spiritual weapon spell) or one dwarven warrior made of pure force
        per 8 Hit Dice you have (each as <em>spiritual ally</em>). If you summon
        force weapons, you can direct all the summoned weapons with a single
        move action. If you summon force allies, you can direct all the summoned
        allies with a single swift action. This acts as a spell with a spell
        level equal to half your Hit Dice (maximum spell level 9th). If any of
        the force effects you summon with this ability are dispelled, they are
        all dispelled.</em></p><h2>Paladin Code</h2><em>Paladins of Angradd see
        themselves as frontline soldiers in the war against evil. They seek to
        form coalitions not just to take down evils in the present, but to leave
        the world better prepared to oppose such evils in the future. Their
        tenets include the following affirmations. <ul><li>I oppose evil in all
        its forms. I do not accept a weak evil just because I must also oppose a
        mighty evil. To defeat evil, I must understand it and know its strengths
        and weaknesses. Any evil I cannot defeat now, or that by opposing I
        would allow greater evil to escape or develop, I must study and work
        against so that I may more fully defeat it when I am ready. I must share
        what I learn with others who will fight alongside me. </li><li>I am more
        than a warrior against the darkness-I am a symbol of all that is just
        and fair. I must serve as an example and as an inspiration, by being
        righteous in all my actions and showing others how benevolence and order
        can be used to create a more just, stable, and sustainable society.
        </li><li>It is not enough to stop evil actions-the source of evil must
        be found and decisively defeated. I am not satisfied to passively wait
        for wrongs to come to my sight; rather, I seek them out. I will be a
        torch in the darkness and expose evil to the sight of others who are
        also willing to oppose it. </li><li>I am open in my dealings. I do not
        lie or look away from actions I cannot support. My opinions and goals
        are open for all to see. I may use stealth and deceit as needed for
        tactical purposes, but will not promote untruths or
        fraud.</li></ul><h2>For Followers of
        Angradd</h2><h3>Spells</h3><p>Planned
        Assault</p><h3>Traits</h3><p>Angradd's Flame, Angradd's Valor,
        Battlefield Caster, Well-Prepared</p>
      format: 1
    title:
      level: 1
      show: false
    type: text
sort: 0
