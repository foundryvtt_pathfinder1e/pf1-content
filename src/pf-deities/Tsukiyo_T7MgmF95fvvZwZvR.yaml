_id: T7MgmF95fvvZwZvR
_key: '!journal!T7MgmF95fvvZwZvR'
_stats:
  coreVersion: '12.331'
name: Tsukiyo
pages:
  - _id: V6anGrQwBrAqsFIl
    _key: '!journal.pages!T7MgmF95fvvZwZvR.V6anGrQwBrAqsFIl'
    _stats:
      coreVersion: '12.331'
    name: Tsukiyo
    sort: 0
    text:
      content: >-
        <h1>Tsukiyo</h1><h2>Prince of the Moon</h2>@Source[PZO9240;pages=58] <br
        /><strong>Pathfinder Wiki</strong> <a
        href="http://www.pathfinderwiki.com/wiki/Tsukiyo">Tsukiyo</a><h3>Details</h3><p><strong>Alignment</strong>
        LG<br /><strong>Pantheon</strong> Deities of Tian Xia<br /><strong>Areas
        of Concern</strong> Jade, the moon, spirits<br
        /><strong>Domains</strong> Darkness, Good, Law, Madness, Repose<br
        /><strong>Subdomains</strong> Ancestors, Archon (Good), Archon (Law),
        Insanity, Moon, Night, Souls<br /><em>* Requires the <u>Acolyte of
        Apocrypha</u> trait.</em><br /><strong>Favored Weapon</strong>
        Longspear<br /><strong>Symbol</strong> Jade crescent moon<br
        /><strong>Sacred Animal(s)</strong> Hare</p><h3>Obedience</h3><p>Find a
        quiet place from which to study the moon. Contemplate its transition
        from full to half to crescent to nothing at all, then back again,
        comparing its transformation to your own accomplishments. If unable to
        study the moon clearly, seek a quiet place and spend 1 hour studying a
        jade trinket while mentally retelling one of Tsukiyo's parables that
        pertains to your recent experiences. Gain a +4 sacred bonus on saving
        throws against effects that cause the confused or fascinated condition
        or that deal either Wisdom damage or Wisdom drain.</p><h3>On
        Golarion</h3><p><strong>Centers of Worship</strong> Goka, Jinin, Minkai,
        Nagajor, Tianjing, Wall of Heaven, Zi Ha<br
        /><strong>Nationality</strong> Tian-Min</p><h2>Boons - Deific
        Obedience</h2><h3> Evangelist</h3><p>@Source[PZO92112;pages=59]<br
        /><strong>1: Lunar Phantom (Sp)</strong> <em>lesser confusion</em>
        3/day, <em>darkness</em> 2/day, or <em>blink</em> 1/day<br /><strong>2:
        Collective Vision (Su)</strong> When using the vision of madness granted
        power from the Madness domain, you can target creatures within 30 feet
        of you instead of having to touch a single target. You can target a
        maximum number of creatures equal to 1 for every 4 Hit Dice you have
        (maximum 5). If you don't have access to the Madness domain, you instead
        gain the ability to use the basic version of the vision of madness
        granted power, touching a single target as a melee touch attack, a
        number of times per day equal to 3 + your Wisdom modifier, as listed in
        the vision of madness description.<br /><strong>3: Moonlit Maze
        (Su)</strong> Once per day, you can point at a creature within 60 feet
        and transport it, as per <em>maze</em>, to a shadowy labyrinth
        illuminated by moonlight overhead. For every minute the target spends at
        rest or spends aimlessly wandering the maze, it is healed 1d2 points of
        Intelligence, Wisdom, or Charisma damage. However, each time the target
        fails an Intelligence check to escape the maze, it takes 1d4 points of
        Charisma
        damage.</em></p><h3>Exalted</h3><p>@Source[PZO92112;pages=59]<br
        /><strong>1: Spirit Shepherd (Sp)</strong> <em>hide from undead</em>
        3/day, <em>calm emotions</em> 2/day, or <em>helping hand</em> 1/day<br
        /><strong>2: Soothing Moon Aura (Su)</strong> You can radiate soft
        silvery moonlight from your body at will as a free action, illuminating
        a 20-foot radius with dim light as a 2nd-level spell. For a number of
        rounds per day equal to 1 + 1 for every 4 Hit Dice you have (maximum 6
        rounds), you can intensify this light as a standard action. The
        intensified light suppresses darkness effects as per <em>daylight</em>
        heightened to a 7th-level spell, though it still creates only dim light.
        Furthermore, it suppresses all harmful compulsion, emotion, and fear
        effects in the area, as well as any harmful effects from madness
        (<em>Pathfinder RPG GameMastery Guide</em> 250 or <em>Pathfinder RPG
        Horror Adventures</em> 182). These rounds don't need to be consecutive.
        You can deactivate the intensified aura as a free action.<br
        /><strong>3: Knight of the Green Crescent (Su)</strong> Once per day as
        a standard action, you can summon an advanced kirin that resembles an
        elegant jade hare. Its breath weapon deals 8d8 points of cold damage.
        The kirin follows your commands perfectly for 1 minute before returning
        to Heaven, but it doesn't obey commands that would violate its
        alignment; particularly egregious commands could cause it to attack
        you.</em></p><h3>Sentinel</h3><p>@Source[PZO92112;pages=59]<br
        /><strong>1: Cleansing Jade (Sp)</strong> <em>magic stone</em> 3/day,
        <em>stone discus</em> 2/day, or <em>searing light</em> 1/day<br
        /><strong>2: Banishing Jade (Su)</strong> You gain the ability to smite
        evil once per day as per a paladin of your character level. If you
        already have the smite evil class feature, this adds to the number of
        times per day that you can smite evil. When you use your smite evil
        class feature, you can transform either your weapon or your armor into
        enchanted jade for 1 minute or until the smite evil effect ends. Your
        transformed weapon gains the <em>ghost touch</em> special ability.
        Furthermore, if the target of smite evil is an undead creature, the
        bonus on damage rolls for the first hit with that weapon increases to 3
        points of damage per paladin level you have (including any sentinel
        levels), and your first attack with that weapon also gains the
        <em>disruption</em> weapon special ability (DC = 13 + your Charisma
        modifier). Your transformed armor gains the <em>ghost touch</em> special
        ability, and if the target of smite evil is an undead creature, the
        armor grants you a +2 sacred bonus on saving throws against the
        creature's spells and effects as well as a +2 sacred bonus to AC against
        its attacks.<br /><strong>3: Reincarnated Champion (Su)</strong> You
        increase the time that you can remain dead before being revived by
        <em>breath of life</em> by a number of rounds equal to your Charisma
        modifier (minimum +1). Once per week as a swift action, you can cast
        <em>breath of life</em> (CL 20th) on yourself, even when you would be
        otherwise unable to take actions, such as when you are
        dead.</em></p><h2>Paladin Code</h2><em>The paladins of Tsukiyo are
        patient teachers and wardens. They tend not to settle in any one
        community, either wandering from town to town or living a reclusive
        existence away from noteworthy settlements. However, these paladins are
        rarely far from communities in need, and they readily answer the call to
        shield a victim or destroy an undead menace. Their tenets include the
        following affirmations. <ul><li>I am the voice for those who go unheard,
        be they spirits who cannot speak or those whom society has judged as
        broken or insane. I shall open the doors for others to listen to the
        voiceless when I can, and when I cannot, I will speak in the words of
        the voiceless, not my own. </li><li>I am the eyes for those who cannot
        understand the truths of others who are not like them. I shall teach
        others how to accept, support, and celebrate those who think and speak
        differently than they do. </li><li>I am the guide for mortal minds and
        souls. I shall offer kindness and aid to the lost, the frightened, the
        confused, and the dead. I shall calm restless spirits, and I shall
        banish violent ones with what compassion I can provide without
        endangering others. </li><li>A different perspective can bring both
        fulfillment and pain. I do not judge those who wish to remain as they
        are, no matter what disadvantages it may bring them, nor do I judge
        those who suffer and wish to be changed. </li><li>There is no shame in
        madness, and so I do not make madness shameful by imposing it on my
        enemies as a punishment. Though darkness and attacks on the mind are
        tools I may use to vanquish the unjust, my actions will never force
        lasting insanity upon my foes.</li></ul><h2>Unique Spell
        Rules</h2>@Source[PZO92112;pages=61]<p><em></em><br
        /><h3>Cleric/Warpriest</h3></p><p><em>Faerie Fire</em> can be prepared
        as a 1st-level spell<br /><em>Calm Spirit can be prepared as a</em>
        2nd-level spell<br /><em>Darkvision</em> can be prepared as a 2nd-level
        spell<br /><em>Rainbow Pattern can be prepared as a</em> 4th-level
        spell<br /><em>Wandering Star Motes</em> can be prepared as a 4th-level
        spell<br /></em></p><h3>Inquisitor</h3><p><em>Faerie Fire can be
        prepared as a 1st-level spell<br /><em>Calm Spirit</em> can be prepared
        as a 2nd-level spell<br /><em>Darkvision can be prepared as a</em>
        2nd-level spell<br /><em>Rainbow Pattern</em> can be prepared as a
        3rd-level spell<br /><em>Wandering Star Motes can be prepared as a</em>
        3rd-level spell<br /></em></p><h3>Oracle</h3><p><em>Faerie Fire</em> can
        be prepared as a 1st-level spell<br /><em>Calm Spirit can be prepared as
        a</em> 2nd-level spell<br /><em>Darkvision</em> can be prepared as a
        2nd-level spell<br /><em>Rainbow Pattern can be prepared as a</em>
        4th-level spell<br /><em>Wandering Star Motes</em> can be prepared as a
        4th-level spell<br /></em></p><h3>Paladin</h3><p><em>Faerie Fire can be
        prepared as a 1st-level spell<br /><em>Calm Spirit</em> can be prepared
        as a 2nd-level spell<br /><em>Darkvision</em> can be prepared as a
        2nd-level spell</em></p>
      format: 1
    title:
      level: 1
      show: false
    type: text
sort: 0
