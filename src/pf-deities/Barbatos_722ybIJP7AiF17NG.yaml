_id: 722ybIJP7AiF17NG
_key: '!journal!722ybIJP7AiF17NG'
_stats:
  coreVersion: '12.331'
name: Barbatos
pages:
  - _id: 7qoNf8qe5s72Aurt
    _key: '!journal.pages!722ybIJP7AiF17NG.7qoNf8qe5s72Aurt'
    _stats:
      coreVersion: '12.331'
    name: Barbatos
    sort: 0
    text:
      content: >-
        <h1>Barbatos</h1><h2>The Bearded Lord</h2>@Source[PZO9267;pages=318] <br
        /><strong>Pathfinder Wiki</strong> <a
        href="http://www.pathfinderwiki.com/wiki/Barbatos">Barbatos</a><h3>Details</h3><p><strong>Alignment</strong>
        LE<br /><strong>Pantheon</strong> Archdevils<br /><strong>Areas of
        Concern</strong> Animals, corruption, gateways<br
        /><strong>Domains</strong> Evil, Law, Magic, Travel<br
        /><strong>Subdomains</strong> Arcane, Devil (Evil), Devil (Law), Divine,
        Portal*, Trade<br /><em>* Requires the <u>Acolyte of Apocrypha</u>
        trait.</em><br /><strong>Favored Weapon</strong> Quarterstaff<br
        /><strong>Symbol</strong> Three-eyed beard<br /><strong>Sacred
        Animal(s)</strong> Raven<br /><strong>Sacred Color(s)</strong> Gray,
        red</p><h3>Obedience</h3><p>Build a small, tree-shaped shrine to
        Barbatos out of broken branches, moss-covered stones, viscous mud, or
        any other sort of naturally occurring material. While pacing around the
        shrine, spatter it with fresh blood-either your own or that of a
        companion or an enemy-and speak 21 words of allegiance to the Bearded
        Lord. Gain a +4 profane bonus on Knowledge (arcana, planes, and
        religion) checks concerning evil-aligned planes, evil faiths, or evil
        magic.</p><h2>Boons - Fiendish
        Obedience</h2><h3>Evangelist</h3><p>@Source[PZO1139;pages=32]<br
        /><strong>1: Taint of the Promised Land (Sp)</strong> <em>ill omen</em>
        3/day, <em>blindness/deafness 2/day, or <em>howling agony</em> 1/day<br
        /><strong>2: Defile the Sacred (Su)</strong> You can project the
        terrible mysteries of Barbatos's true form into an opponent's body,
        infusing it with a mere iota of the Bearded Lord's horrors. Once per day
        as a standard action, you can target a living good-aligned or neutral
        creature with a touch attack that briefly transforms its body into a
        mass of quivering worms and undulating flesh. This deals an amount of
        damage equal to 10 points per Hit Die you have (maximum 150 points of
        damage) and nauseates the target for 1 round. A target that succeeds at
        a Fortitude saving throw (DC = 10 + half your Hit Dice + your Charisma
        modifier) takes only half damage and negates the nauseated effect. If
        the creature you target is evil-aligned, this ability has no effect and
        is wasted for the day.<br /><strong>3: Faces of the Wise (Su)</strong>
        You are keenly aware of the million possible visages that might lurk
        under Barbatos's hood and can project those images into an enemy's mind.
        Once per day as a standard action, you can target any creature within
        100 feet. If the target fails a Will saving throw (DC = 10 + half your
        Hit Dice + your Charisma modifier), it is stunned for 1 round and then
        paralyzed for a number of rounds equal to your Hit Dice. Each round on
        its turn while it is paralyzed, the target can attempt a new saving
        throw to end the paralyzing effect; this is a full-round action that
        does not provoke attacks of opportunity. A creature that recovers from
        this effect early after being affected by it for at least 2 rounds is
        staggered for 2d4 rounds. A creature that succeeds at its initial saving
        throw against this effect is merely staggered for 1 round. This is a
        mind-affecting fear
        effect.</em></p><h3>Exalted</h3><p>@Source[PZO1139;pages=33]<br
        /><strong>1: Beasts of the First (Sp)</strong> <em>magic fang </em>
        3/day, <em>fox's cunning</em> 2/day, or <em>stench of prey</em> 1/day<br
        /><strong>2: Gnashing of Brutish Teeth (Su)</strong> You are intimately
        familiar with the guttural sounds of hellbeasts and can imitate their
        cacophonies to the detriment of your foes. Once per day as a standard
        action, you can unleash this clamor, forcing all enemies within 30 feet
        of you to attempt a Will saving throw (DC = 10 + half your Hit Dice +
        your Charisma modifier). Those who fail are frightened for a number of
        rounds equal to your Hit Dice and take an amount of sonic damage equal
        to 1d6 per Hit Die you have (maximum 20d6). Creatures that succeed at
        their saving throws take half damage and are instead shaken for this
        effect's duration. This is a mind-affecting fear effect.<br /><strong>3:
        Bearded Lord's Aspect (Sp)</strong> Once per day, you can assume an
        imitation of one of the many forms Barbatos prefers when traveling in
        the mortal realms. This functions as <em>frightful aspect</em>, using
        your Hit Dice as your caster level, except you also gain darkvision with
        a range of 90 feet, scent, and a bite attack. Treat the bite attack as a
        primary attack made with your full base attack bonus; it deals an amount
        of damage equal to 3d6 plus 1-1/2 times your Strength
        modifier.</em></p><h3>Sentinel</h3><p>@Source[PZO1139;pages=33]<br
        /><strong>1: No Passage (Sp)</strong> <em>bungle</em> 3/day,
        <em>piercing shriek</em> 2/day, or <em>glyph of warding</em> 1/day<br
        /><strong>2: From Whence You Came (Sp)</strong> The Bearded Lord has
        untold knowledge of not only fiends and their places in Hell and beyond
        but also all outsiders, and you can periodically harness this
        confounding reality to your advantage. Three times per day, you can cast
        <em>dismissal as a spell-like ability. When you do, you are immediately
        affected by <em>protection from good</em> if the dismissed outsider was
        good-aligned, or protection from evil if the outsider was evil-aligned,
        for a number of minutes equal to your Hit Dice.<br /><strong>3:
        Avernus's Iron Gates (Sp)</strong> You have an unholy tie to Barbatos's
        demesne. Once per day, you can open a gate to Avernus, as per the spell
        <em>gate</em>, and use it either for planar travel or to call a pit
        fiend to your service. If you use it to call a pit fiend, this functions
        like the gate spell's calling creatures effect, but you are not required
        to pay the 10,000 gp material cost. If the pit fiend has more Hit Dice
        than you, you can't control it (as normal), but pit fiends tend to
        gladly obey the requests of powerful minions of
        Barbatos.</em></p><h2>For Followers of
        Barbatos</h2><h3>Feats</h3><p><em>Hellish
        Shackles</em></p><h3>Traits</h3><p><em>Flames of Hell</em></p>
      format: 1
    title:
      level: 1
      show: false
    type: text
sort: 0
