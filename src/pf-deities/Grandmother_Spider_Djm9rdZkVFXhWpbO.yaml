_id: Djm9rdZkVFXhWpbO
_key: '!journal!Djm9rdZkVFXhWpbO'
_stats:
  coreVersion: '12.331'
name: Grandmother Spider
pages:
  - _id: 1tEeKw5NbbnxRA3q
    _key: '!journal.pages!Djm9rdZkVFXhWpbO.1tEeKw5NbbnxRA3q'
    _stats:
      coreVersion: '12.331'
    name: Grandmother Spider
    sort: 0
    text:
      content: >-
        <h1>Grandmother Spider</h1><h2>The Weaver</h2>@Source[PZO92112;pages=11]
        <br /><strong>Pathfinder Wiki</strong> <a
        href="http://www.pathfinderwiki.com/wiki/Grandmother Spider">Grandmother
        Spider</a><h3>Details</h3><p><strong>Alignment</strong> N<br
        /><strong>Pantheon</strong> Other Deities<br /><strong>Areas of
        Concern</strong> Twilight, weaving, illusion, and family<br
        /><strong>Domains</strong> Charm, Community, Darkness, Luck, Trickery<br
        /><strong>Subdomains</strong> N/A<br /><em>* Requires the <u>Acolyte of
        Apocrypha</u> trait.</em><br /><strong>Favored Weapon</strong> Net<br
        /><strong>Symbol</strong> Diamond made of eight interwoven
        threads</p><h3>Obedience</h3><p>Spend an hour weaving something clever
        and useful that you will use to better your community, incorporating the
        stories or symbolism of Grandmother Spider. Kindhearted souls often
        interpret this to mean weaving blankets, clothing, or tapestries for
        those in need, while duplicitous worshippers take it to mean weaving a
        tall tale or confidence game that exploits someone who's taking
        advantage of the community or teaches someone a valuable lesson. Gain a
        +2 sacred or profane bonus on Bluff and Perform (oratory) checks and on
        saving throws against illusions. The type of bonus depends on your
        alignment-if you're neither good nor evil, you must choose either sacred
        or profane the first time you perform your obedience, and this choice
        can't be changed.</p><h3>On Golarion</h3><p><strong>Centers of
        Worship</strong> Droon, Nurvatchta, the Shackles, The Sodden Lands <br
        /><strong>Nationality</strong> Anadi</p><h2>Boons - Deific
        Obedience</h2><h3> Evangelist</h3><p>@Source[PZO92112;pages=11]<br
        /><strong>1: Charming Sort (Sp)</strong> <em>sleep</em> 3/day,
        <em>eagle's splendor</em> 2/day, or <em>glibness</em> 1/day<br
        /><strong>2: Storyteller (Ex)</strong> Much like Grandmother Spider, you
        can weave thrilling tales to buy yourself time or distract those around
        you, even when among those who know better than to listen to you. You
        gain the fascinate bardic performance and a number of rounds of bardic
        performance each day equal to your Charisma modifier + your Hit Dice,
        with a caster level equal to your Hit Dice. If you already have the
        bardic performance class feature, all your class levels count as bard
        levels for the purpose of determining your total rounds of bardic
        performance each day and the save DC of your fascinate performance.<br
        /><strong>3: One with the Night (Su)</strong> Grandmother Spider
        welcomes cool darkness as a relief from the heat of the sun. You too
        have learned to embrace the starlit beauty of the evening, knowing that
        the darkness will give you shelter whether you are predator or prey. You
        gain darkvision with a range of 30 feet, or extend your existing
        darkvision by 30 feet. You need only half the normal amount of sleep or
        rest each day to avoid becoming
        fatigued.</em></p><h3>Exalted</h3><p>@Source[PZO92112;pages=11]<br
        /><strong>1: The Weaver (Sp)</strong> <em>silent image</em> 3/day,
        <em>minor image</em> 2/day, or <em>borrow fortune</em> 1/day<br
        /><strong>2: Inspired Illusionist (Su)</strong> Though you lack
        Grandmother Spider's ability to weave new fates, you can emulate some
        measure of her skill at seamlessly transforming figments into reality.
        Your illusions are especially convincing and persistent. Spells you cast
        from the figment or glamer subschools last twice as long, as if extended
        with the Extend Spell feat. You cannot further extend these spells by
        applying Extend Spell metamagic a second time. You add the following
        spells to your spell list of spells known: <em>silent image (1st),
        <em>minor image</em> (2nd), <em>major image</em> (3rd),
        <em>hallucinatory terrain</em> (4th), <em>persistent image</em> (5th),
        <em>programmed image (6th), <em>project image</em> (7th),
        <em>screen</em> (8th).<br /><strong>3: Willful Weaver (Sp)</strong> What
        matters the reality if all the world believes the lie? The effects of
        your illusions are so strong they can override the physical world. Three
        times per day, you can cast a false version of a conjuration or
        evocation spell as if casting <em>shadow evocation</em> or <em>greater
        shadow
        conjuration</em>.</em></p><h3>Sentinel</h3><p>@Source[PZO92112;pages=11]<br
        /><strong>1: Trapper (Sp)</strong> <em>animate rope</em> 3/day,
        <em>web</em> 2/day, or <em>spiked pit</em> 1/day<br /><strong>2: Net
        Master (Ex)</strong> A trapper and a trickster rather than a true
        warrior, Grandmother Spider bests her foes through schemes, wits, and
        quick reflexes. You have learned to use the spider's patient guile in
        combat, trapping your foes within the spider's web. You gain Net Adept
        as a bonus feat, ignoring its prerequisites. If you already have Net
        Adept, you instead gain a +1 sacred or profane bonus on attack rolls
        with a net. When fighting with a net as a one-handed weapon and wielding
        nothing in your off hand, you also gain a +1 shield bonus to AC.<br
        /><strong>3: Binding Strike (Su)</strong> You allow no chances for your
        foes to escape once you have the advantage. Those enemies you fell are
        bound and ripe for your stewpot, should you feel less than charitable in
        victory. When you confirm a critical hit against an opponent, you can
        forgo any additional damage to immediately bind your foe in sticky webs,
        paralyzing it and pinning it in place. This functions as a <em>hold
        monster</em> spell with a caster level equal to your Hit Dice. You can
        use this ability a number of times per day equal to your Charisma
        modifier (minimum 1).</em></p><h2>Unique Spell
        Rules</h2><em>@Source[PZO92112;pages=13]<em></em><br
        /><h3>Cleric/Warpriest</h3><p><em>Silent Image</em> can be prepared as a
        1st-level spell<br /><em>Ventriloquism can be prepared as a</em>
        1st-level spell<br /><em>Spider Climb</em> can be prepared as a
        2nd-level spell<br /><em>Web</em> can be prepared as a 2nd-level
        spell</em></p>
      format: 1
    title:
      level: 1
      show: false
    type: text
sort: 0
