_id: HYIPQRgn5mR1lQyl
_key: '!journal!HYIPQRgn5mR1lQyl'
_stats:
  coreVersion: '12.331'
name: Besmara
pages:
  - _id: 9DYxnzQDkQjKK1lT
    _key: '!journal.pages!HYIPQRgn5mR1lQyl.9DYxnzQDkQjKK1lT'
    _stats:
      coreVersion: '12.331'
    name: Besmara
    sort: 0
    text:
      content: >-
        <h1>Besmara</h1><h2>The Pirate Queen</h2>@Source[PZO9290;pages=23] <br
        /><strong>Pathfinder Wiki</strong> <a
        href="http://www.pathfinderwiki.com/wiki/Besmara">Besmara</a><h3>Details</h3><p><strong>Alignment</strong>
        CN<br /><strong>Pantheon</strong> Other Deities<br /><strong>Areas of
        Concern</strong> Piracy, sea monsters, strife<br
        /><strong>Domains</strong> Chaos, Trickery, War, Water, Weather<br
        /><strong>Subdomains</strong> Deception, Duels, Innuendo, Oceans,
        Protean, Storms, Tactics, Thievery<br /><em>* Requires the <u>Acolyte of
        Apocrypha</u> trait.</em><br /><strong>Favored Weapon</strong> Rapier<br
        /><strong>Symbol</strong> Skull and crossbones<br /><strong>Sacred
        Animal(s)</strong> Parrot<br /><strong>Sacred Color(s)</strong> Black,
        white</p><h3>Obedience</h3><p>Steal a gold coin or alcoholic drink by
        force or trickery. Then, while recounting your latest or most impressive
        act of piracy and blessing Besmara's name for all to hear, offer your
        stolen item to her by throwing it into water at least 4 feet deep.
        Alternatively, you can recount your most impressive or recent act of
        piracy to someone unaware of it, although you are free to hide the fact
        that the act was yours. You then gain a +2 sacred or profane bonus to AC
        against attacks of opportunity. The type of bonus depends on your
        alignment-if you're neither good nor evil, you must choose either sacred
        or profane the first time you perform your obedience, and this choice
        can't be changed.</p><h3>On Golarion</h3><p><strong>Centers of
        Worship</strong> Garund, Ilizmagorti, The Shackles<br
        /><strong>Nationality</strong> Kellid</p><h2>Boons - Deific
        Obedience</h2><h3>
        Evangelist</h3><p>@Source[PZO9290;pages=23]<em></em><br /><strong>1:
        Tricky Adversary (Sp)</strong> <em>illusion of calm</em><sup>UC</sup>
        3/day, <em>daze monster</em> 2/day, or <em>twilight
        knife</em><sup>APG</sup> 1/day<br /><strong>2: Coerce Service
        (Su)</strong> You understand and can exhibit the blatantly charismatic
        pull of your goddess, convincing others to aid you even if they normally
        might not. Once per day, when you attempt a Diplomacy check to bribe a
        target (treat as if you are attempting to improve an NPC's attitude
        toward you by one step) or an Intimidate check to coerce a target, you
        can use this ability to gain Besmara's blessing to ensure further
        cooperation. You gain a +4 sacred or profane bonus (of the same type as
        that provided by your obedience) on the Diplomacy or Intimidate check,
        and if you succeed, the target is immediately subject to a
        <em>geas/quest</em> effect. For as long as the effect lasts, you can
        concentrate as a standard action and learn whether the target is
        actively undertaking your orders or actively defying them.<br
        /><strong>3: Bribed Sea Monster (Sp)</strong> The sea monsters at
        Besmara's behest follow your call at the goddess's command. Once per day
        as a standard action, you can summon one of the monsters Besmara has
        bribed or intimidated into aiding her followers. You must choose the
        monster from among the following list: adult sea dragon (<em>Pathfinder
        RPG Bestiary 3 96), sea serpent (<em>Pathfinder RPG Bestiary</em> 244),
        or vouivre (<em>Pathfinder RPG Bestiary</em> 4 270). The monster appears
        as if summoned via <em>summon monster</em>, and you must summon it into
        an appropriately watery environment. It follows your commands perfectly
        for 1 round per Hit Die you possess before vanishing. The monster
        doesn't follow commands that contravene Besmara's interests, such as
        blocking waterways or giving a navy better control of a sea region;
        issuing such untenable instructions, if they're egregious, could cause
        the monster to attack
        you.</em></p><h3>Exalted</h3><p>@Source[PZO9290;pages=23]<br
        /><strong>1: Captain's Bluster (Sp)</strong> <em>command</em> 3/day,
        <em>aggressive thundercloud</em><sup>ACG</sup> 2/day, or <em>wind
        wall</em> 1/day<br /><strong>2: Treacherous Mirage (Sp)</strong>
        Besmara's blessing allows you to hide the truth once per day. This
        ability takes the form of either <em>false vision or <em>mirage arcana.
        You can change the illusion by spending a move action in concentration.
        The illusion lasts until you create a new one or dismiss this effect as
        a swift action.<br /><strong>3: Rally Crew (Su)</strong> The Pirate
        Queen lends you her divine panache, which fills all of your allies with
        courage and pride in their service to you. Once per day as a standard
        action, you can inspire all allies within 60 feet to greater speed and
        might. They each gain a 10-foot bonus to their speed and the benefits of
        heroism. On any attack rolls, saving throws, or skill checks benefiting
        from this morale bonus, your allies take no penalties due to the effects
        of water or weather. The bonus lasts for 1 hour per Hit Die you possess.
        As an immediate action, you can cause any number of creatures who
        disobey your orders to lose this
        bonus.</em></p><h3>Sentinel</h3><p>@Source[PZO9290;pages=23]<br
        /><strong>1: Fearsome Boast (Sp)</strong> <em>monkey
        fish</em><sup>ACG</sup> 3/day, <em>slipstream<sup>APG</sup> 2/day, or
        <em>water breathing</em> 1/day<br /><strong>2: Doom of Sailors
        (Sp)</strong> You can immobilize ships or scatter a fleet with a wave of
        your hand. Once per day, you can cast either <em>control water or
        <em>control winds</em>.<br /><strong>3: Pirate Queen's Curse
        (Su)</strong> You know that the Pirate Queen's enemies are destined to
        suffer, and you can deliver her wrath personally. Three times per day,
        you can channel a curse through your weapon. You must declare your use
        of this ability before you make the attack roll. On a hit, the target is
        cursed unless it succeeds at a Will saving throw to negate the effect
        (DC = 10 + 1/2 your Hit Dice + your Charisma modifier). The curse causes
        the target to be sickened for a number of days equal to your Hit Dice;
        furthermore, during this period, any creatures flanking the target or
        against which it is denied its Dexterity bonus to AC deal an additional
        1d6 points of damage with every successful hit. This extra damage
        applies to the attack that delivers the curse. In addition, all
        creatures with a swim speed have a starting attitude of hostile toward
        the target. This is a 7th-level curse effect. The curse cannot be
        dispelled, but it can be removed through <em>break enchantment,
        <em>limited wish, <em>miracle</em>, <em>remove curse, or
        <em>wish</em>.</em></p><h2>Antipaladin Code</h2><em>Antipaladins of
        Besmara are cutthroat pirates, freely roaming where they will and
        seizing whatever interests them. They primarily emulate their goddess's
        violent aspects, but different antipaladins focus on varied aspects of
        Besmara's code. The code of Besmara's antipaladins includes the
        following adages. <ul><li>I pursue what I desire. One who cannot keep it
        from me does not deserve to possess it.</li><li>Treasure demonstrates my
        might. I won't waste my time on schemes with no profit to be
        had.</li><li>No trick is beneath me. I will never forgo an advantage in
        the name of fairness.</li><li>Pride does not shackle me. I retreat if I
        must to survive for another day.</li><li>The weak serve the strong. I
        will never let my crew forget or doubt my strength.</li><li>Revenge can
        be delayed to keep the ship sailing. Crew members who cross me shall die
        onshore; foes can be useful allies under changed
        circumstances.</li><li>I will suffer no restraint. Those who seek to
        pacify me will fall broken in my wake.</li></ul><h2>For Followers of
        Besmara</h2><h3>Archetypes</h3><p>Deep Shaman (Shaman)</p><h3>Magic
        Items - Armor</h3><p>Sea Banshee Coat</p><h3>Magic Items -
        Sets</h3><p>Besmara's Bounty</p><h3>Magic Items - Wondrous
        Items</h3><p>Besmara's Bicorne, Besmara's
        Tricorne</p><h3>Spells</h3><p>Advanced Scurvy, Cloud of
        Seasickness</p><h3>Traits</h3><p>Besmara's Name, Besmara's Strength,
        Cheat Death, Deck Fighter, Expert Boarder</p><h2>Unique Spell
        Rules</h2>@Source[PZO9267;pages=176]<p><h3>Cleric/Warpriest</h3></p><p><em>Geas,
        Lesser</em> can be prepared as a 4th-level spell [can only be used to
        cause an aversion to boats, ships, or open bodies of water]<br
        /><em>Curse of Disgust</em> can be prepared as a 5th-level spell [can
        only be used to cause an aversion to boats, ships, or open bodies of
        water]</p><h2>Unique Summon Rules</h2>@Source[PZO9055;pages=72]<br
        /><strong>Summon Monster II</strong>: Entropic Reefclaw - CN (uses
        entropic simple template)<br /><strong>Summon Monster V</strong>:
        Saltwater Merrow - NE<br /><strong>Summon Monster VI</strong>:
        Tylosaurus - N<br /><h2>Other Rules</h2>@Source[PZO9055;pages=72]<br />A
        cleric of Besmara may give up one domain in exchange for a bird,
        blue-ringed octopus (<em>Ultimate Magic 117), king crab (<em>Ultimate
        Magic</em> 119), monkey, or any of the familiars presented in the Core
        Rulebook. The cleric uses her cleric level as her effective wizard level
        for this purpose. A ranger who worships Besmara may select any of the
        creatures listed above as a familiar instead of choosing an animal
        companion. The ranger's effective wizard level for this ability is equal
        to his ranger level -
        3.<p>@Source[PZO9290;pages=26]</p><h3>Cleric/Warpriest</h3><p><em>Geas,
        Lesser</em> can be prepared as a 4th-level spell [can only use to cause
        aversion to boats, ships, or open bodies of water]<br /><em>Curse of
        Disgust</em> can be prepared as a 5th-level spell [can only use to cause
        aversion to boats, ships, or open bodies of water]</p>
      format: 1
    title:
      level: 1
      show: false
    type: text
sort: 0
