_id: DeDwvydxdnU4aEwH
_key: '!journal!DeDwvydxdnU4aEwH'
_stats:
  coreVersion: '12.331'
name: Brigh
pages:
  - _id: NEixK6AOwhg2KmWj
    _key: '!journal.pages!DeDwvydxdnU4aEwH.NEixK6AOwhg2KmWj'
    _stats:
      coreVersion: '12.331'
    name: Brigh
    sort: 0
    text:
      content: >-
        <h1>Brigh</h1><h2>The Whisper in the
        Bronze</h2>@Source[PZO9290;pages=29] <br /><strong>Pathfinder
        Wiki</strong> <a
        href="http://www.pathfinderwiki.com/wiki/Brigh">Brigh</a><h3>Details</h3><p><strong>Alignment</strong>
        N<br /><strong>Pantheon</strong> Other Deities<br /><strong>Other
        Pantheons</strong> Gnomish Deities<br /><strong>Areas of
        Concern</strong> Clockwork, invention, time<br
        /><strong>Domains</strong> Artifice, Earth, Fire, Knowledge<br
        /><strong>Subdomains</strong> Alchemy (Artifice)*, Construct, Industry,
        Metal, Smoke, Thought<br /><em>* Requires the <u>Acolyte of
        Apocrypha</u> trait.</em><br /><strong>Favored Weapon</strong> Light
        hammer<br /><strong>Symbol</strong> Mask with forehead rune<br
        /><strong>Sacred Animal(s)</strong> Termite<br /><strong>Sacred
        Color(s)</strong> Bronze, silver</p><h3>Obedience</h3><p>While reciting
        formulas from <em>Logic of Design</em>, you must craft a new creation,
        continue work on an elaborate device or object, or disassemble an
        existing creation to see how it works. Favored projects among the church
        include useful gear, magic items, innovative toys, and contraptions
        invented primarily to see if they would work rather than to solve a
        specific problem. Share the knowledge you discover while working on this
        project. If no people are around to hear or read your observations, or
        you are incapable of communicating with anyone, instead write down notes
        about your work to publish or otherwise share with others later. If you
        can share these notes with someone but choose not to, your obedience is
        unfulfilled; the requirement to share a set of these notes needs only be
        met once for that set of notes. You gain a +4 sacred or profane bonus on
        Disable Device checks. The type of bonus depends on your alignment-if
        you're neither good nor evil, you must choose either sacred or profane
        the first time you perform your obedience, and this choice can't be
        changed.</p><h3>On Golarion</h3><p><strong>Centers of Worship</strong>
        Absalom, Druma, Mana Wastes, Nex<br /><strong>Nationality</strong>
        Taldan</p><h2>Boons - Deific Obedience</h2><h3>
        Evangelist</h3><p>@Source[PZO9290;pages=29]<br /><strong>1: Voice of
        Bronze (Sp)</strong> <em>jury rig</em><sup>UC</sup> 3/day, <em>fox's
        cunning 2/day, <em>sands of time</em><sup>UM</sup> 1/day<br /><strong>2:
        Living Construct (Su)</strong> The Whisper in the Bronze is the patron
        of constructs, and through the teachings of her faith, you have learned
        how to sense the animating spirits within such mechanical entities. You
        can affect constructs with magic as if they were living creatures. Once
        per day, you can target a construct with a spell or spell-like ability,
        and the spell resolves as if the construct's creature type were
        humanoid. This can bypass intelligent constructs' immunity to
        mind-affecting effects, but mindless constructs remain unaffected.<br
        /><strong>3: Time Bounce (Sp)</strong> Time is simply a construct, and
        as such, it can be understood and manipulated. You gain the ability to
        perceive the flow of time and alter it around yourself, giving you the
        ability to bounce through time-space from one place to another. Once per
        day, you can use <em>dimensional bounce</em><sup>ACG</sup> as a
        spell-like
        ability.</em></p><h3>Exalted</h3><p>@Source[PZO9290;pages=29]<br
        /><strong>1: Creator (Sp)</strong> <em>crafter's
        fortune</em><sup>APG</sup> 3/day, <em>make whole</em> 2/day, <em>minor
        creation</em> 1/day<br /><strong>2: Protected by the Machine
        (Su)</strong> Your body gains a bit of the resistance of a construct,
        perhaps appearing as bronze plates shielding vital organs or bronze
        gears on your limbs to help push them past their limits, much as Brigh's
        own clockwork armor is said to change form to suit her needs. You gain a
        +2 sacred or profane bonus (of the same type as that provided by your
        obedience) on saving throws against effects that cause ability damage,
        ability drain, energy drain, exhaustion, fatigue, or nonlethal
        damage.<br /><strong>3: Inspired Crafting (Su)</strong> The quest for
        discovery and innovation never stops. By applying Brigh's deep insights
        into efficient time management and technical innovation, you can craft
        items in spare moments squeezed in while adventuring or otherwise
        serving your god's ideals. When crafting magic items while adventuring,
        you can devote 4 hours each day to creation and take advantage of the
        full amount of time spent crafting instead of netting only 2 hours'
        worth of work. In addition, you can use <em>fabricate once per day as a
        spell-like ability. Although you can't create magic items with the
        <em>fabricate</em> spell, you can use it to create items that you later
        enhance
        magically.</em></p><h3>Sentinel</h3><p>@Source[PZO9290;pages=29]<br
        /><strong>1: Bronze Warrior (Sp)</strong> <em>crafter's
        curse</em><sup>APG</sup> 3/day, <em>heat metal</em> 2/day,
        <em>haste</em> 1/day<br /><strong>2: Constructed Form (Su)</strong>
        Brigh is fond of adjusting, adding, and removing devices from her
        internal mechanical workings to constantly improve her design, and she
        blesses you with a trace of this ability. As a swift action, you can
        alter your body to incorporate construct-like features that protect you
        from physical damage. The transformation is subtle, such as adding metal
        reinforcements along your bones or small, auxiliary clockwork
        counterparts to your vital organs. The transformation grants you DR 3/-
        and lasts for a number of rounds per day equal to your Hit Dice. These
        rounds need not be consecutive. Dismissing the effect is a free
        action.<br /><strong>3: Call to Battle (Su)</strong> Brigh's mechanical
        servants answer your call to defend inventions, innocent constructs, and
        the victims of misguided inventions. Once per day as a full-round
        action, you can summon a clockwork golem (<em>Pathfinder RPG Bestiary
        </em> 2</em> 137). It has the extraplanar subtype but is otherwise a
        typical golem of its kind. The golem follows your commands perfectly for
        1 round per Hit Die you possess before vanishing.</em></p><h2>For
        Followers of Brigh</h2><h3>Archetypes</h3><p><em>Voice of Brigh
        (Bard)</em></p><h3>Magic Items - Armor</h3><p><em>Bronze Whisperer's
        Shield</em></p><h3>Monsters</h3><p><em>Latten Mechanism
        (Herald)</em></p><h3>Spells</h3><p><em>Semblance of
        Flesh</em></p><h3>Traits</h3><p><em>Brigh's Insight, Nimble Fingers,
        Keen Mind</em></p><h2>Unique Spell
        Rules</h2><em>@Source[PZO9086;pages=74]</em><br
        /><h3>Cleric/Warpriest</h3><p><em>Unbreakable Construct</em> can be
        prepared as a 5th-level spell [<sup>ISG p.176</sup>]<br /><em>Control
        Construct</em> can be prepared as a 7th-level spell [<sup>ISG
        p.176</sup>]<br /><em>Make Whole can be spontaneously cast as a
        2nd-level spell<br /><em>Mending</em> can be spontaneously cast as a
        0-level spell<br /><em>Soothe Construct can be spontaneously cast as a
        4th-level spell<br /></em></p><h3>Inquisitor</h3><p><em>Mending</em> can
        be prepared as a 1st-level spell<br /><em>Make Whole can be prepared as
        a 2nd-level spell<br /><em>Soothe Construct</em> can be prepared as a
        3rd-level spell</em></p><h2>Unique Summon
        Rules</h2>@Source[PZO9086;pages=74]<br /><strong>Summon Monster
        I</strong>: Clockwork Spy<br /><strong>Summon Monster IV</strong>:
        Clockwork Servant<br /><strong>Summon Monster VI</strong>: Clockwork
        Soldier
      format: 1
    title:
      level: 1
      show: false
    type: text
sort: 0
