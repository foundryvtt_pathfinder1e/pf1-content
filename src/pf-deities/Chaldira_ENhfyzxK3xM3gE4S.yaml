_id: ENhfyzxK3xM3gE4S
_key: '!journal!ENhfyzxK3xM3gE4S'
_stats:
  coreVersion: '12.331'
name: Chaldira
pages:
  - _id: LDBZESyPPj9qiz7y
    _key: '!journal.pages!ENhfyzxK3xM3gE4S.LDBZESyPPj9qiz7y'
    _stats:
      coreVersion: '12.331'
    name: Chaldira
    sort: 0
    text:
      content: >-
        <h1>Chaldira</h1><h2>The Calamitous Turn</h2>@Source[PZO9267;pages=324]
        <br /><strong>Pathfinder Wiki</strong> <a
        href="http://www.pathfinderwiki.com/wiki/Chaldira">Chaldira</a><h3>Details</h3><p><strong>Alignment</strong>
        NG<br /><strong>Pantheon</strong> Halfling Deities<br /><strong>Areas of
        Concern</strong> Battle, luck, mischief<br /><strong>Domains</strong>
        Good, Luck, Trickery, War<br /><strong>Subdomains</strong> Agathion,
        Fate, Friendship, Tactics<br /><em>* Requires the <u>Acolyte of
        Apocrypha</u> trait.</em><br /><strong>Favored Weapon</strong> Short
        sword<br /><strong>Symbol</strong> Shortsword with 3 notches<br
        /><strong>Sacred Animal(s)</strong> Lizard<br /><strong>Sacred
        Color(s)</strong> Green, red</p><h3>Obedience</h3><p>Steal a coin or
        button from someone that isn't likely to miss it or need it. While
        unobserved, balance the coin or button on the edge of a weapon, flip it
        into the air, and call out how it will land. If you are right, find
        someone else to give the coin or button to-perhaps even its original
        owner-and explain how the token will bring good fortune. Gain a +2
        sacred bonus on attack rolls with the type of weapon (for example, short
        swords) you used to balance the object. If you can't find someone to
        take the coin or button after an hour, or if you were wrong in calling
        how it will land, hide it somewhere where it will be found and where the
        finder might deem himself lucky for discovering it. In this case, you
        gain only a +1 sacred bonus on attack rolls with the weapon
        type.</p><h3>On Golarion</h3><p><strong>Centers of Worship</strong>
        Absalom, Cheliax, Nidal, River Kingdoms, Varisia<br
        /><strong>Nationality</strong> halfling</p><h2>Boons - Deific
        Obedience</h2><h3> Evangelist</h3><p>@Source[PZO92112;pages=5]<br
        /><strong>1: Lucky Berries (Sp)</strong> <em>goodberry</em> 3/day,
        <em>tree shape (Small berry bush only)</em> 2/day, or <em>speak with
        plants</em> 1/day<br /><strong>2: Confident Gambler (Su)</strong> While
        many creatures rely on their luck, your faith allows you to control your
        luck to a small degree. Except when performing Chaldira's obedience, you
        can determine as a free action how an ordinary coin you flip or ordinary
        die you toss will land. If you use this ability for your own personal
        gain to the detriment of someone who is innocent or less fortunate than
        you are, you are likely to incur Chaldira's disfavor and lose the use of
        this boon until you atone, as set forth in the <em>atonement spell. When
        used to recover the use of this boon, an <em>atonement</em> spell has an
        additional monetary cost equal to the amount you gained due to the
        misuse of this ability. If you instead return the amount you gained to
        the person you acquired it from (or to the person's family, community,
        friends, or other appropriate connection) and provide a genuine apology,
        then the <em>atonement</em> spell has no additional cost.<br
        /><strong>3: Auspicious Accuracy (Su)</strong> Your ability to control
        your luck expands to desperate situations of life and death. A number of
        times per day equal to your Charisma modifier (minimum once per day),
        when you would normally roll a miss chance percentage, you can instead
        choose whether or not to hit. This ability manifests as a lucky turn of
        the blade or an awkward lunge that appears to be an overcorrection but
        actually places your attack right where it needs to be. Observers are
        likely to consider you exceptionally fortunate in your attacks, rather
        than suspect that you are supernaturally guided to bypass effects that
        would cause you to miss.</em></p><h3><em>divine favor</em> 3/day,
        <em>blessing of luck and resolve</em> 2/day, or <em>prayer</em> 1/day<br
        /><strong>2: Fortunate Spells (Su)</strong> When you create an effect
        that adjusts the luck of your allies, your devotion to Chaldira turns
        their luck even more strongly in their favor. Increase luck bonuses
        provided by spells or spell-like abilities you cast by 1. This ability
        stacks with other effects that increase a luck bonus, such as the
        Fortune's Child sentinel boon, but not with other uses of this boon.<br
        /><strong>3: Providential Resistance (Su)</strong> With a quick plea to
        the Calamitous Turn, you shield yourself against a devastating energy
        attack. Three times per day as an immediate action when you would take
        acid, cold, electricity, fire, or sonic damage, you can gain resistance
        20 to that damage type for 1 minute.</em></h3><h3><em>blurred
        movement</em> 3/day, <em>blur (self only) 2/day, or <em>blink</em>
        1/day. Your appearance does not change when using these abilities,
        although they function as normal; onlookers are not aware you are
        subject to their effects until they see an attack or effect miss you due
        to one of these abilities (or until you engage in a plainly magical
        effect, such as stepping through a solid object with the <em>blink
        spell-like ability), or if they have the ability to detect the effect
        through <em>detect magic</em> or a similar ability.<br /><strong>2:
        Fortune's Child (Su)</strong> Your luck is legendary, and you derive a
        stronger benefit from effects that aid your luck than other people do.
        Increase luck bonuses you receive from any effect by 1. This ability
        stacks with other effects that increase a luck bonus, such as the
        Fortunate Spells exalted boon, but not with other uses of this boon.<br
        /><strong>3: Impetuous Ardor (Su)</strong> Chaldira protects those who
        leap into combat without hesitation, and you cloak yourself in her favor
        when you dart across the battlefield toward your foes. You do not
        provoke attacks of opportunity for movement that takes you closer to an
        enemy you can see.</em></h3><h2>Paladin Code</h2><em>Paladins of
        Chaldira Zuzaristan are firm believers in luck, and they believe that
        righteous people are inherently more fortunate than the unrighteous.
        When the unrighteous appear to have a lucky turn, it is an invitation to
        an inauspicious downfall, often at the end of the paladin's blade.
        Paladins of Chaldira are quick to leap into battle even when they appear
        outmatched, trusting in their virtue and luck to carry them to victory.
        The code of Chaldira's paladins contains the following adages. <ul><li>I
        know that mischief brings joy to those who partake in it, but mischief
        must never come at the expense of another's well-being. Theft is not
        inherently wrong, but theft must not be employed when it furthers
        suffering. </li><li>I will not hesitate to fight against those who
        oppress or attack the innocent. </li><li>I will never fail to help a
        friend in need. </li><li>I will acknowledge whenever I, or others, have
        benefitted from a lucky turn of events and praise the fortune that
        Chaldira bestows. </li><li>It is my sacred duty to learn and explore.
        </li><li>I freely serve my community and share my worldly goods with
        others, so I might demonstrate charity by example. </li><li>I will
        defend children in particular from abuse and evil, so their unwritten
        future can remain bright.</li></ul><h2>For Followers of
        Chaldira</h2><h3>Magic Items - Armor</h3><p>Calamitous
        Mail</p><h3>Traits</h3><p>Call for Help, Chaldira's Luck, Lessons of
        Chaldira, Mischievous Smile, Reckless Luck</p><h2>Unique Spell
        Rules</h2>@Source[PZO92112;pages=7]<p><h3>Cleric/Warpriest</h3></p><p><em>Mage
        Hand can be prepared as a</em> 0-level spell<br /><em>Expeditious
        Retreat</em> can be prepared as a 1st-level spell</em></p>
      format: 1
    title:
      level: 1
      show: false
    type: text
sort: 0
