_id: gTwPgLnzzvrpmDxc
_key: '!actors!gTwPgLnzzvrpmDxc'
_stats:
  coreVersion: '12.331'
img: icons/svg/mystery-man.svg
items:
  - _id: cV7yHt8i5YCV0ZTd
    _key: '!actors.items!gTwPgLnzzvrpmDxc.cV7yHt8i5YCV0ZTd'
    _stats:
      coreVersion: '12.331'
    img: systems/pf1/icons/skills/yellow_40.jpg
    name: Outsider (Native)
    system:
      bab: high
      classSkills:
        blf: true
        crf: true
        kpl: true
        per: true
        sen: true
        ste: true
      description:
        value: >-
          <p>An outsider is at least partially composed of the essence (but not
          necessarily the material) of some plane other than the Material Plane.
          Some creatures start out as some other type and become outsiders when
          they attain a higher (or lower) state of spiritual
          existence.</p><h2>Features</h2><p>An outsider has the following
          features.</p><ul><li>d10 Hit Dice.</li><li>Base attack bonus equal to
          total Hit Dice (fast progression).</li><li>Two good saving throws,
          usually&nbsp;<a
          href="https://www.d20pfsrd.com/gamemastering/combat#TOC-Reflex"
          rel="nofollow">Reflex</a>&nbsp;and Will.</li><li>Skill points equal to
          6 + Int modifier (minimum 1) per Hit Die. The following are class
          skills for outsiders: <a href="https://www.d20pfsrd.com/skills/bluff"
          rel="nofollow">Bluff</a>, <a
          href="https://www.d20pfsrd.com/skills/craft" rel="nofollow">Craft</a>,
          <a href="https://www.d20pfsrd.com/skills/knowledge"
          rel="nofollow">Knowledge</a>&nbsp;(planes), <a
          href="https://www.d20pfsrd.com/skills/perception"
          rel="nofollow">Perception</a>, <a
          href="https://www.d20pfsrd.com/skills/sense-motive"
          rel="nofollow">Sense Motive</a>, and <a
          href="https://www.d20pfsrd.com/skills/stealth"
          rel="nofollow">Stealth</a>. Due to their varied nature, outsiders also
          receive 4 additional class skills determined by the creature’s
          theme.</li></ul><h2>Traits</h2><p>An outsider possesses the following
          traits (unless otherwise noted in a creature’s
          entry).</p><ul><li>Darkvision 60 feet.</li><li>Unlike most living
          creatures, an outsider does not have a dual nature—its soul and body
          form one unit. When an outsider is slain, no soul is set loose. Spells
          that restore souls to their bodies, such as&nbsp;<a
          href="https://www.d20pfsrd.com/magic/all-spells/r/raise-dead"
          rel="nofollow">raise dead</a>,&nbsp;<a
          href="https://www.d20pfsrd.com/magic/all-spells/r/reincarnate"
          rel="nofollow">reincarnate</a>,&nbsp;and&nbsp;<a
          href="https://www.d20pfsrd.com/magic/all-spells/r/resurrection"
          rel="nofollow">resurrection</a>,&nbsp;don’t work on an outsider. It
          takes a different magical effect, such as&nbsp;<a
          href="https://www.d20pfsrd.com/magic/all-spells/l/limited-wish"
          rel="nofollow">limited wish</a>,&nbsp;<a
          href="https://www.d20pfsrd.com/magic/all-spells/w/wish"
          rel="nofollow">wish</a>,&nbsp;<a
          href="https://www.d20pfsrd.com/magic/all-spells/m/miracle"
          rel="nofollow">miracle</a>,&nbsp;or&nbsp;<a
          href="https://www.d20pfsrd.com/magic/all-spells/t/true-resurrection"
          rel="nofollow">true resurrection</a>&nbsp;to restore it to life. An
          outsider with the native subtype can be raised, reincarnated, or
          resurrected just as other living creatures can be.</li><li>Proficient
          with all simple and martial weapons and any weapons mentioned in its
          entry.</li><li>Proficient with whatever type of armor (light, medium,
          or heavy) it is described as wearing, as well as all lighter types.
          Outsiders not indicated as wearing armor are not proficient with
          armor. Outsiders are proficient with shields if they are proficient
          with any form of armor.</li><li>Outsiders breathe, but do not need to
          eat or sleep (although they can do so if they wish). Native outsiders
          breathe, eat, and sleep.</li></ul>
      hd: 10
      hp: 16
      level: 3
      savingThrows:
        ref:
          value: high
        will:
          value: high
      skillsPerLevel: 6
      tag: outsider
    type: class
  - _id: dN1jBEF9Au8StiAW
    _key: '!actors.items!gTwPgLnzzvrpmDxc.dN1jBEF9Au8StiAW'
    _stats:
      coreVersion: '12.331'
    img: systems/pf1/icons/skills/yellow_40.jpg
    name: 'Race: Outsider'
    system:
      creatureSubtypes:
        - native
      creatureTypes:
        - outsider
      description:
        value: >-
          sbc | As the statblock did not include a race, a custom one was
          generated.
    type: race
  - _id: xV7rUqIvwZu7sEzs
    _key: '!actors.items!gTwPgLnzzvrpmDxc.xV7rUqIvwZu7sEzs'
    _stats:
      coreVersion: '12.331'
    img: systems/pf1/icons/skills/shadow_12.jpg
    name: 'Sense: Darkvision 60ft.'
    system:
      abilityType: ex
      description:
        value: >-
          Darkvision is the extraordinary ability to see with no light source at
          all, out to a range specified for the creature. Darkvision is
          black-and-white only (colors cannot be discerned). It does not allow
          characters to see anything that they could not see otherwise—invisible
          objects are still invisible, and illusions are still visible as what
          they seem to be. Likewise, darkvision subjects a creature to gaze
          attacks normally. The presence of light does not spoil darkvision.
      subType: racial
    type: feat
  - _id: nGHXyAZlv5yV6jPL
    _key: '!actors.items!gTwPgLnzzvrpmDxc.nGHXyAZlv5yV6jPL'
    _stats:
      coreVersion: '12.331'
    img: systems/pf1/icons/skills/blue_29.jpg
    name: 'Sense: Scent'
    system:
      abilityType: ex
      description:
        value: >-
          This special quality allows a creature to detect approaching enemies,
          sniff out hidden foes, and track by sense of smell. Creatures with the
          scent ability can identify familiar odors just as humans do familiar
          sights.</p><p>The creature can detect opponents within 30 feet by
          sense of smell. If the opponent is upwind, the range increases to 60
          feet; if downwind, it drops to 15 feet. Strong scents, such as smoke
          or rotting garbage, can be detected at twice the ranges noted above.
          Overpowering scents, such as skunk musk or troglodyte stench, can be
          detected at triple normal range.</p><p>When a creature detects a
          scent, the exact location of the source is not revealed—only its
          presence somewhere within range. The creature can take a move action
          to note the direction of the scent. When the creature is within 5 feet
          of the source, it pinpoints the source’s location.</p><p>A creature
          with the scent ability can follow tracks by smell, making a Wisdom (or
          Survival) check to find or follow a track. The typical DC for a fresh
          trail is 10 (no matter what kind of surface holds the scent). This DC
          increases or decreases depending on how strong the quarry’s odor is,
          the number of creatures, and the age of the trail. For each hour that
          the trail is cold, the DC increases by 2. The ability otherwise
          follows the rules for the Survival skill. Creatures tracking by scent
          ignore the effects of surface conditions and poor visibility.
      subType: racial
    type: feat
  - _id: 0mUbFkOAXVaebUGN
    _key: '!actors.items!gTwPgLnzzvrpmDxc.0mUbFkOAXVaebUGN'
    _stats:
      coreVersion: '12.331'
    img: systems/pf1/icons/skills/yellow_36.jpg
    name: Dimensional Agilityuc
    system:
      description:
        value: >-
          sbc | As Dimensional AgilityUC could not be found in any compendium, a
          placeholder was generated.
    type: feat
  - _id: aSlpO0bR5A4n0f2k
    _key: '!actors.items!gTwPgLnzzvrpmDxc.aSlpO0bR5A4n0f2k'
    _stats:
      coreVersion: '12.331'
    img: systems/pf1/icons/items/inventory/monster-head.jpg
    name: Bite
    system:
      actions:
        - _id: 8rch342thrvo3iu5
          ability:
            attack: _default
            damage: str
          actionType: mwak
          activation:
            type: attack
            unchained:
              type: attack
          attackBonus: '0'
          attackName: Bite
          damage:
            parts:
              - formula: 1d4+1
                types:
                  - slashing
          duration:
            units: inst
          img: systems/pf1/icons/items/inventory/monster-head.jpg
          name: Attack
          notes:
            footer:
              - bite +6 (1d4+3)
          range:
            units: melee
          tag: bite
      subType: natural
    type: attack
  - _id: 5W2HQumj7m04GUhd
    _key: '!actors.items!gTwPgLnzzvrpmDxc.5W2HQumj7m04GUhd'
    _stats:
      coreVersion: '12.331'
    img: systems/pf1/icons/skills/yellow_36.jpg
    name: 'Environment: Any Land'
    system:
      description:
        value: >-
          sbc | Here you’ll find information on how the monster fits into the
          world, notes on its ecology and society, and other bits of useful lore
          and flavor that will help you breathe life into the creature when your
          PCs encounter it.
      subType: misc
    type: feat
  - _id: DMtQDJIzaFus3mmx
    _key: '!actors.items!gTwPgLnzzvrpmDxc.DMtQDJIzaFus3mmx'
    _stats:
      coreVersion: '12.331'
    img: systems/pf1/icons/skills/yellow_36.jpg
    name: 'Organization: Solitary Or Pack (2-4)'
    system:
      description:
        value: >-
          sbc | Here you’ll find information on how the monster fits into the
          world, notes on its ecology and society, and other bits of useful lore
          and flavor that will help you breathe life into the creature when your
          PCs encounter it.
      subType: misc
    type: feat
  - _id: 8PoKPJ2LVuJOA1H6
    _key: '!actors.items!gTwPgLnzzvrpmDxc.8PoKPJ2LVuJOA1H6'
    _stats:
      coreVersion: '12.331'
    img: systems/pf1/icons/skills/yellow_36.jpg
    name: 'Treasure: None'
    system:
      description:
        value: >-
          sbc | Here you’ll find information on how the monster fits into the
          world, notes on its ecology and society, and other bits of useful lore
          and flavor that will help you breathe life into the creature when your
          PCs encounter it.
      subType: misc
    type: feat
  - _id: tGEmjTV4WaTdfOfK
    _key: '!actors.items!gTwPgLnzzvrpmDxc.tGEmjTV4WaTdfOfK'
    _stats:
      coreVersion: '12.331'
    img: systems/pf1/icons/skills/yellow_36.jpg
    name: Thunderous Growl
    system:
      abilityType: su
      description:
        value: >-
          Three times per day, a caypup can issue a rumbling growl from its
          throat that sounds like distant thunder and scares away potential
          attackers. Creatures within 15 feet of the caypup must succeed at a DC
          12 Will save to attack the caypup (as if affected by sanctuary). The
          caypup can choose to bestow the same effect on an adjacent ally as
          well. This effect lasts for 3 rounds or until the caypup or its ally
          attacks (whichever comes first), after which time the caypup must wait
          at least 1d6 rounds before using this ability again. The save DC is
          Charisma-based.
      subType: classFeat
    type: feat
  - _id: Y0MCTsVCfH3exGH7
    _key: '!actors.items!gTwPgLnzzvrpmDxc.Y0MCTsVCfH3exGH7'
    _stats:
      coreVersion: '12.331'
    img: systems/pf1/icons/skills/yellow_36.jpg
    name: sbc | Conversion Buff
    system:
      active: true
      description:
        value: >-
          <h2>sbc | Conversion Buff</h2><p>This Buff was created by
          <strong>sbc</strong> to compensate for differences between the input
          and the values FoundryVTT calculates automatically.</p><p>Especially
          when the pathfinder system gets upgraded, entries in compendiums get
          updated or foundry changes in some way, this buff may become
          outdated.</p><p>For most mooks the conversion should more or less
          work, but for important NPCs or creatures it is adviced to double
          check the conversion manually.</p>
      hideFromToken: true
      subType: perm
      tag: sbcConversionBuff
    type: buff
name: Caypup
prototypeToken:
  texture:
    src: icons/svg/mystery-man.svg
system:
  abilities:
    cha:
      value: 12
    con:
      value: 14
    dex:
      value: 11
    int:
      value: 6
    str:
      value: 15
    wis:
      value: 9
  attributes:
    naturalAC: '+3'
    spells:
      spellbooks:
        primary:
          name: Primary
        secondary:
          name: Secondary
        spelllike:
          name: Spell-likes
        tertiary:
          name: Tertiary
  details:
    alignment: cg
    cr:
      base: 2
    notes:
      value: "\n        <div class=\"statblockContainer\" style=\"margin-top: 15px\"><div style=\"width: 100%; background-color: #ffe9c7; box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19); columns: 2 150px\">\r\n    <hr style=\"margin-left: 0; margin-top:-2px; width: 100%; height: 4px; background-color: #e0a100; border: 1px solid #000; column-span: all;\" />\r\n    <div style=\"padding: 5px;\">\r\n        <h1>Caypup CR 2 </h1>\r\n        \r\n        <!-- BASE -->\r\n        <div style=\"break-inside: avoid;\">\r\n            <strong>Source</strong>: Familiar Folio pg. 29<br/>\r\n            <strong>XP 600</strong><br/>\r\n            \r\n            \r\n            \r\n            \r\n            \r\n            CG \r\n            Small \r\n            Outsider (Native) <br/>\r\n            <strong>Init</strong> 4;\r\n            <strong>Senses</strong> darkvision 60 ft., scent; Perception +5; \r\n            \r\n        </div>\r\n        <div style=\"break-inside: avoid;\">\r\n            <h3>Defense</h3>\r\n                <strong>AC</strong> 14, <strong>Touch</strong> 11, <strong>Flat-Footed</strong> 14 (+3 natural, +1 size)<br/>\r\n            <strong>Hit Points</strong> 22 (3 HD; 3d10+6)<br/>\r\n            <strong>Fort</strong> 3, <strong>Ref</strong> 3, <strong>Will</strong> 2\r\n            \r\n            <strong>DR</strong> 5/cold iron\r\n            \r\n            \r\n            \r\n            \r\n            \r\n        </div>\r\n        <div>\r\n            <div style=\"break-inside: avoid;\">\r\n                <h3>Offense</h3>\r\n                        <strong>Speed</strong> 30 ft.<br/>\r\n                <strong>Melee</strong> bite +6 (1d4+3)<br/>\r\n                \r\n                \r\n                \r\n                \r\n            </div>\r\n        </div>\r\n        <!-- SPELLCASTING -->\r\n                \r\n        <!-- TACTICS -->\r\n                \r\n                \r\n                \r\n                \r\n        <!-- STATISTICS -->\r\n        <div style=\"break-inside: avoid;\">\r\n            <h3>Statistics</h3>\r\n                <strong>Str</strong> 15, <strong>Dex</strong> 11, <strong>Con</strong> 14, <strong>Int</strong> 6, <strong>Wis</strong> 9, <strong>Cha</strong> 12<br/>\r\n            <strong>Base Atk</strong> +3; <strong>CMB</strong> +4; <strong>CMD</strong> 14<br/>\r\n            <strong>Feats</strong> Dimensional AgilityUC, Improved Initiative<br/>\r\n            <strong>Skills</strong> Acrobatics +6, Intimidate +7, Perception +5, Sense Motive +5<br/>\r\n            <strong>Languages</strong> Celestial (can&#x27;t speak)<br/>\r\n            \r\n            \r\n            \r\n            \r\n        </div>\r\n        <!-- ECOLOGY -->\r\n            <div style='break-inside: avoid;'>\r\n                <h3>Ecology</h3>                <strong>Environment</strong>: any land<br/>\r\n                <strong>Organization</strong>: solitary or pack (2-4)<br/>\r\n                <strong>Treasure</strong>: none<br/>\r\n            </div>\r\n        <!-- SPECIAL ABILITIES -->\r\n            <div style='break-inside: avoid;'>\r\n                <h3>Special Abilities</h3>                <div style='white-space: pre-wrap;'>Thunderous Growl (su): Three times per day, a caypup can issue a rumbling growl from its throat that sounds like distant thunder and scares away potential attackers. Creatures within 15 feet of the caypup must succeed at a DC 12 Will save to attack the caypup (as if affected by sanctuary). The caypup can choose to bestow the same effect on an adjacent ally as well. This effect lasts for 3 rounds or until the caypup or its ally attacks (whichever comes first), after which time the caypup must wait at least 1d6 rounds before using this ability again. The save DC is Charisma-based.</div>\r\n            </div>\r\n        \r\n        <!-- DESCRIPTION -->\r\n            <div style='break-inside: avoid;'>\r\n                <h3>Description</h3>        <div style=\"white-space: pre-wrap\">Cayhounds-as fickle and determined as their patron god and master, Cayden Cailean-sometimes birth pups on the Material Plane or Elysium. These half-celestial offspring are known to mortals as caypups.\n\nLike their otherworldly forebears, caypups are driven to perform acts of good and to halt wrongdoing in the lands they roam. Caypups sometimes join adventurers in hopes of reenacting the legendary deeds of Cayden Cailean and his hound, Thunder. A 7th-level spellcaster with the Improved Familiar feat can gain a caypup as a familiar.\n\nAfter maturing through infancy, caypups reach an adolescent state that they occupy for their entire lives. Resembling juvenile mastiffs with rust-red fur and piercing blue eyes, caypups are 4 feet from nose to tail and weigh about 75 pounds.\n</div>\r\n                    \r\n    </div>\r\n    <hr style=\"margin-left: 0; margin-bottom: -3px; width: 100%; height: 4px; background-color: #e0a100; border: 1px solid #000; column-span: all;\" />\r\n</div></div>\n    \n        <br>\n        <hr>\n        <div class=\"rawInputContainer\" style=\"margin-top: 15px;\">\n            <h2>RAW INPUT</h2>\n            <hr>\n            <pre style=\"white-space: pre-wrap; font-size: 10px;\">Caypup CR 2\nSource Familiar Folio pg. 29\nXP 600\nCG Small outsider (native)\nInit +4; Senses darkvision 60 ft., scent; Perception +5\nDefense\nAC 14, touch 11, flat-footed 14 (+3 natural, +1 size)\nhp 22 (3d10+6)\nFort +3, Ref +3, Will +2\nDR 5/cold iron\nOffense\nSpeed 30 ft.\nMelee bite +6 (1d4+3)\nSpecial Attacks thunderous growl\nSpell-Like Abilities (CL 3rd; concentration +4)\n3/day-knock, open/close, stabilize\n1/day-dimension door (self plus 5 lbs. of objects only)\nStatistics\nStr 15, Dex 11, Con 14, Int 6, Wis 9, Cha 12\nBase Atk +3; CMB +4; CMD 14\nFeats Dimensional AgilityUC, Improved Initiative\nSkills Acrobatics +6, Intimidate +7, Perception +5, Sense Motive +5\nLanguages Celestial (can't speak)\nEcology\nEnvironment any land\nOrganization solitary or pack (2-4)\nTreasure none\nSpecial Abilities\nThunderous Growl (Su) Three times per day, a caypup can issue a rumbling growl from its throat that sounds like distant thunder and scares away potential attackers. Creatures within 15 feet of the caypup must succeed at a DC 12 Will save to attack the caypup (as if affected by sanctuary). The caypup can choose to bestow the same effect on an adjacent ally as well. This effect lasts for 3 rounds or until the caypup or its ally attacks (whichever comes first), after which time the caypup must wait at least 1d6 rounds before using this ability again. The save DC is Charisma-based.\nDescription\nCayhounds-as fickle and determined as their patron god and master, Cayden Cailean-sometimes birth pups on the Material Plane or Elysium. These half-celestial offspring are known to mortals as caypups.\n\nLike their otherworldly forebears, caypups are driven to perform acts of good and to halt wrongdoing in the lands they roam. Caypups sometimes join adventurers in hopes of reenacting the legendary deeds of Cayden Cailean and his hound, Thunder. A 7th-level spellcaster with the Improved Familiar feat can gain a caypup as a familiar.\n\nAfter maturing through infancy, caypups reach an adolescent state that they occupy for their entire lives. Resembling juvenile mastiffs with rust-red fur and piercing blue eyes, caypups are 4 feet from nose to tail and weigh about 75 pounds.</pre>\n        </div>\n    "
  skills:
    acr:
      rank: 6
    int:
      rank: 6
    per:
      rank: 3
    sen:
      rank: 3
  traits:
    dr:
      custom: 5/cold iron
    languages:
      - null
    senses:
      custom: darkvision 60 ft., scent; Perception +5
      dv:
        value: 60
    size: sm
type: npc
