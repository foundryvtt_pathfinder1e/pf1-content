_id: j5XabdYqcyhHDZ11
_key: '!actors!j5XabdYqcyhHDZ11'
_stats:
  coreVersion: '12.331'
img: icons/svg/mystery-man.svg
items:
  - _id: H8FbMUps5Z0gQdvV
    _key: '!actors.items!j5XabdYqcyhHDZ11.H8FbMUps5Z0gQdvV'
    _stats:
      coreVersion: '12.331'
    img: systems/pf1/icons/skills/mech_7.jpg
    name: Construct
    system:
      bab: high
      description:
        value: >-
          <p>A construct is an animated object or artificially created
          creature.</p><h2>Features</h2><p>A construct has the following
          features.</p><ul><li>d10 Hit Die.</li><li>Base attack bonus equal to
          total Hit Dice (fast progression).</li><li>No good saving
          throws.</li><li>Skill points equal to 2 + Int modifier (minimum 1) per
          Hit Die. However, most constructs are mindless and gain no skill
          points or feats. Constructs do not have any class skills, regardless
          of their&nbsp;<a
          href="https://www.d20pfsrd.com/basics-ability-scores/ability-scores/#Intelligence_Int">Intelligence</a>&nbsp;scores.</li><li>Construct
          Size Bonus Hit Points Fine — Diminutive — Tiny — Small 10 Medium 20
          Large 30 Huge 40 Gargantuan 60 Colossal
          80</li></ul><h2>Traits</h2><p>A construct possesses the following
          traits (unless otherwise noted in a creature’s entry).</p><ul><li>No
          Constitution score. Any DCs or other statistics that rely on a
          Constitution score treat a construct as having a score of 10 (no bonus
          or penalty).</li><li>Low-light vision.</li><li>Darkvision 60
          feet.</li><li>Immunity to all mind-affecting effects (charms,
          compulsions, morale effects, patterns, and
          phantasms).</li><li>Immunity to disease, death effects, necromancy
          effects, paralysis, poison, sleep effects, and
          stunning.</li><li>Cannot heal damage on its own, but often can be
          repaired via exposure to a certain kind of effect (see the creature’s
          description for details) or through the use of the&nbsp;<a
          href="https://www.d20pfsrd.com/bestiary/rules-for-monsters/monster-feats#TOC-Craft-Construct-Item-Creation-"
          rel="nofollow">Craft Construct</a>&nbsp;feat. Constructs can also be
          healed through spells such as&nbsp;<a
          href="https://www.d20pfsrd.com/magic/all-spells/m/make-whole"
          rel="nofollow">make whole</a>. A construct with the fast healing
          special quality still benefits from that quality.</li><li>Not subject
          to&nbsp;<a
          href="https://www.d20pfsrd.com/bestiary/rules-for-monsters/universal-monster-rules#TOC-Ability-Damage-and-Drain-Ex-or-Su-"
          rel="nofollow">ability damage</a>,&nbsp;<a
          href="https://www.d20pfsrd.com/bestiary/rules-for-monsters/universal-monster-rules#TOC-Ability-Damage-and-Drain-Ex-or-Su-"
          rel="nofollow">ability drain</a>, fatigue, exhaustion, energy drain,
          or nonlethal damage.</li><li>Immunity to any effect that requires
          a&nbsp;<a
          href="https://www.d20pfsrd.com/gamemastering/combat#TOC-Fortitude"
          rel="nofollow">Fortitude</a>&nbsp;save (unless the effect also works
          on objects, or is harmless).</li><li>Not at risk of death from massive
          damage. Immediately destroyed when reduced to 0 hit points or
          less.</li><li>A construct cannot be raised or resurrected.</li><li>A
          construct is hard to destroy, and gains bonus hit points based on
          size, as shown on the following table.</li></ul><ul><li>Proficient
          with its natural weapons only, unless generally humanoid in form, in
          which case proficient with any weapon mentioned in its
          entry.</li><li>Proficient with no armor.</li><li>Constructs do not
          breathe, eat, or sleep.</li></ul><h2>Construct Size Bonus Hit
          Points</h2><table style="width: 176px;" border="1"><tbody><tr><td
          style="width: 128px;">Fine</td><td style="width:
          48px;">10</td></tr><tr><td>Diminutive</td><td>10</td></tr><tr><td>Tiny</td><td>10</td></tr><tr><td>Small</td><td>10</td></tr><tr><td>Medium</td><td>20</td></tr><tr><td>Large</td><td>30</td></tr><tr><td>Huge</td><td>40</td></tr><tr><td>Gargantuan</td><td>60</td></tr><tr><td>Colossal</td><td>80</td></tr></tbody></table>
      hd: 10
      hp: 5
      skillsPerLevel: 2
      tag: construct
    type: class
  - _id: k3OVIJ8mxkx4Kbo0
    _key: '!actors.items!j5XabdYqcyhHDZ11.k3OVIJ8mxkx4Kbo0'
    _stats:
      coreVersion: '12.331'
    img: systems/pf1/icons/skills/mech_7.jpg
    name: 'Race: Construct'
    system:
      creatureSubtypes:
        - ''
      creatureTypes:
        - construct
      description:
        value: >-
          sbc | As the statblock did not include a race, a custom one was
          generated.
    type: race
  - _id: FAdJQX05wL2O3UNz
    _key: '!actors.items!j5XabdYqcyhHDZ11.FAdJQX05wL2O3UNz'
    _stats:
      coreVersion: '12.331'
    img: systems/pf1/icons/skills/light_01.jpg
    name: 'Sense: Blindsight'
    system:
      abilityType: ex
      description:
        value: >-
          Using nonvisual senses such as acute smell or hearing, members of this
          race notice things they cannot see.</p><p>Members of this race usually
          do not need to make Perception checks to pinpoint the location of a
          creature within 30 feet, provided they have line of effect to that
          creature.</p><p>A creature that members of this race cannot see still
          has total concealment against individuals with blindsense, and members
          of this race still have the normal miss chance when attacking
          creatures that have concealment.</p><p>Visibility still affects the
          movement of members of this race. Members of this race are still
          denied their Dexterity bonus to AC against attacks from creatures they
          cannot see.
      subType: racial
    type: feat
  - _id: KO3Z22okIko2cwDE
    _key: '!actors.items!j5XabdYqcyhHDZ11.KO3Z22okIko2cwDE'
    _stats:
      coreVersion: '12.331'
    img: systems/pf1/icons/skills/yellow_36.jpg
    name: 'Special Quality: Ioun Affinity'
    system:
      description:
        value: >-
          sbc | Placeholder for Special Qualities, which in most statblocks are
          listed under SQ in the statistics block, but described in the Special
          Abilities. Remove duplicates as needed!
      subType: misc
    type: feat
  - _id: aGcBnMXVau7qljNg
    _key: '!actors.items!j5XabdYqcyhHDZ11.aGcBnMXVau7qljNg'
    _stats:
      coreVersion: '12.331'
    img: systems/pf1/icons/skills/yellow_36.jpg
    name: 'Special Quality: Share Iouns'
    system:
      description:
        value: >-
          sbc | Placeholder for Special Qualities, which in most statblocks are
          listed under SQ in the statistics block, but described in the Special
          Abilities. Remove duplicates as needed!
      subType: misc
    type: feat
  - _id: RJuQuVzCkVNWU1jl
    _key: '!actors.items!j5XabdYqcyhHDZ11.RJuQuVzCkVNWU1jl'
    _stats:
      coreVersion: '12.331'
    img: systems/pf1/icons/items/inventory/monster-forearm.jpg
    name: Slam
    system:
      actions:
        - _id: wfvk2ezoezdbu8nm
          ability:
            attack: _default
            damage: str
          actionType: mwak
          activation:
            type: attack
            unchained:
              type: attack
          attackBonus: '0'
          attackName: Slam
          damage:
            parts:
              - formula: 1d4+0
                types:
                  - bludgeoning
          duration:
            units: inst
          img: systems/pf1/icons/items/inventory/monster-forearm.jpg
          name: Attack
          notes:
            footer:
              - slam +0 (1d4-3)
          range:
            units: melee
          tag: slam
      subType: natural
    type: attack
  - _id: R04DyNTLnWIwLVr4
    _key: '!actors.items!j5XabdYqcyhHDZ11.R04DyNTLnWIwLVr4'
    _stats:
      coreVersion: '12.331'
    img: systems/pf1/icons/skills/yellow_36.jpg
    name: 'Environment: Any'
    system:
      description:
        value: >-
          sbc | Here you’ll find information on how the monster fits into the
          world, notes on its ecology and society, and other bits of useful lore
          and flavor that will help you breathe life into the creature when your
          PCs encounter it.
      subType: misc
    type: feat
  - _id: 1l6clmZbTHU4AUdW
    _key: '!actors.items!j5XabdYqcyhHDZ11.1l6clmZbTHU4AUdW'
    _stats:
      coreVersion: '12.331'
    img: systems/pf1/icons/skills/yellow_36.jpg
    name: 'Organization: Solitary'
    system:
      description:
        value: >-
          sbc | Here you’ll find information on how the monster fits into the
          world, notes on its ecology and society, and other bits of useful lore
          and flavor that will help you breathe life into the creature when your
          PCs encounter it.
      subType: misc
    type: feat
  - _id: H8VDupOHyo7zqc7y
    _key: '!actors.items!j5XabdYqcyhHDZ11.H8VDupOHyo7zqc7y'
    _stats:
      coreVersion: '12.331'
    img: systems/pf1/icons/skills/yellow_36.jpg
    name: 'Treasure: None'
    system:
      description:
        value: >-
          sbc | Here you’ll find information on how the monster fits into the
          world, notes on its ecology and society, and other bits of useful lore
          and flavor that will help you breathe life into the creature when your
          PCs encounter it.
      subType: misc
    type: feat
  - _id: YTbu9PpbtQWgZdlk
    _key: '!actors.items!j5XabdYqcyhHDZ11.YTbu9PpbtQWgZdlk'
    _stats:
      coreVersion: '12.331'
    img: systems/pf1/icons/skills/yellow_36.jpg
    name: Ioun Affinity
    system:
      abilityType: su
      description:
        value: >-
          An ioun wyrd can integrate a number of ioun stones into its body equal
          to 1 + half its Hit Dice. Because an ioun wyrd sees all ioun stones as
          equal and gains no benefits from them, the wyrd's ioun stones can be
          swapped out by any creature the wyrd trusts.
      subType: classFeat
    type: feat
  - _id: 8ayRILjn49TUlzSy
    _key: '!actors.items!j5XabdYqcyhHDZ11.8ayRILjn49TUlzSy'
    _stats:
      coreVersion: '12.331'
    img: systems/pf1/icons/skills/yellow_36.jpg
    name: Share Iouns
    system:
      abilityType: su
      description:
        value: >-
          A character with an ioun wyrd familiar gains the benefit of the wyrd's
          ioun stones as long as the character is within 30 feet of the ioun
          wyrd.
      subType: classFeat
    type: feat
  - _id: 13JkLDpA1ftYLFXH
    _key: '!actors.items!j5XabdYqcyhHDZ11.13JkLDpA1ftYLFXH'
    _stats:
      coreVersion: '12.331'
    img: systems/pf1/icons/skills/yellow_36.jpg
    name: sbc | Conversion Buff
    system:
      active: true
      changes:
        - _id: 25ikl3qf
          formula: '5'
          target: cmb
          type: untypedPerm
        - _id: 8zl011c5
          formula: '1'
          target: ac
          type: dodge
        - _id: s5ps4udb
          formula: '5'
          target: mhp
          type: untypedPerm
        - _id: 9yq0ej6f
          formula: '5'
          target: fort
          type: untypedPerm
        - _id: 4cm08hgx
          formula: '-1'
          target: aac
          type: untypedPerm
      description:
        value: >-
          <h2>sbc | Conversion Buff</h2><p>This Buff was created by
          <strong>sbc</strong> to compensate for differences between the input
          and the values FoundryVTT calculates automatically.</p><p>Especially
          when the pathfinder system gets upgraded, entries in compendiums get
          updated or foundry changes in some way, this buff may become
          outdated.</p><p>For most mooks the conversion should more or less
          work, but for important NPCs or creatures it is adviced to double
          check the conversion manually.</p>
      hideFromToken: true
      subType: perm
      tag: sbcConversionBuff
    type: buff
name: Ioun Wyrd
prototypeToken:
  texture:
    src: icons/svg/mystery-man.svg
system:
  abilities:
    cha:
      value: 5
    con:
      value: 0
    dex:
      value: 15
    int:
      value: 3
    str:
      value: 4
    wis:
      value: 14
  attributes:
    speed:
      fly:
        base: 30
      land:
        base: 0
    spells:
      spellbooks:
        primary:
          name: Primary
        secondary:
          name: Secondary
        spelllike:
          name: Spell-likes
        tertiary:
          name: Tertiary
  details:
    cr:
      base: 0.3375
    notes:
      value: "\n        <div class=\"statblockContainer\" style=\"margin-top: 15px\"><div style=\"width: 100%; background-color: #ffe9c7; box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19); columns: 2 150px\">\r\n    <hr style=\"margin-left: 0; margin-top:-2px; width: 100%; height: 4px; background-color: #e0a100; border: 1px solid #000; column-span: all;\" />\r\n    <div style=\"padding: 5px;\">\r\n        <h1>Ioun Wyrd CR 1/3 </h1>\r\n        \r\n        <!-- BASE -->\r\n        <div style=\"break-inside: avoid;\">\r\n            <strong>Source</strong>: Ultimate Wilderness pg. 197, Familiar Folio pg. 30<br/>\r\n            <strong>XP 135</strong><br/>\r\n            \r\n            \r\n            \r\n            \r\n            \r\n            N \r\n            Tiny \r\n            Construct <br/>\r\n            <strong>Init</strong> 2;\r\n            <strong>Senses</strong> blindsight 30 ft.; Perception +2; \r\n            \r\n        </div>\r\n        <div style=\"break-inside: avoid;\">\r\n            <h3>Defense</h3>\r\n                <strong>AC</strong> 15, <strong>Touch</strong> 15, <strong>Flat-Footed</strong> 12 (+2 Dex, +1 dodge, +2 size)<br/>\r\n            <strong>Hit Points</strong> 5 (1 HD; 1d10)<br/>\r\n            <strong>Fort</strong> 0, <strong>Ref</strong> 2, <strong>Will</strong> 2\r\n            \r\n            \r\n            <strong>Immune</strong> construct traits; \r\n            \r\n            \r\n            \r\n            \r\n        </div>\r\n        <div>\r\n            <div style=\"break-inside: avoid;\">\r\n                <h3>Offense</h3>\r\n                        <strong>Speed</strong> 30 ft. (average)<br/>\r\n                <strong>Melee</strong> slam +0 (1d4-3)<br/>\r\n                \r\n                \r\n                \r\n                \r\n            </div>\r\n        </div>\r\n        <!-- SPELLCASTING -->\r\n                \r\n        <!-- TACTICS -->\r\n                \r\n                \r\n                \r\n                \r\n        <!-- STATISTICS -->\r\n        <div style=\"break-inside: avoid;\">\r\n            <h3>Statistics</h3>\r\n                <strong>Str</strong> 4, <strong>Dex</strong> 15, <strong>Con</strong> 0, <strong>Int</strong> 3, <strong>Wis</strong> 14, <strong>Cha</strong> 5<br/>\r\n            <strong>Base Atk</strong> +1; <strong>CMB</strong> +1; <strong>CMD</strong> 9<br/>\r\n            <strong>Feats</strong> Dodge<br/>\r\n            <strong>Skills</strong> Fly +10<br/>\r\n            <strong>Languages</strong> Common (can&#x27;t speak)<br/>\r\n            \r\n            \r\n            \r\n            \r\n        </div>\r\n        <!-- ECOLOGY -->\r\n            <div style='break-inside: avoid;'>\r\n                <h3>Ecology</h3>                <strong>Environment</strong>: any<br/>\r\n                <strong>Organization</strong>: solitary<br/>\r\n                <strong>Treasure</strong>: none<br/>\r\n            </div>\r\n        <!-- SPECIAL ABILITIES -->\r\n            <div style='break-inside: avoid;'>\r\n                <h3>Special Abilities</h3>                <div style='white-space: pre-wrap;'>Ioun Affinity (su): An ioun wyrd can integrate a number of ioun stones into its body equal to 1 + half its Hit Dice. Because an ioun wyrd sees all ioun stones as equal and gains no benefits from them, the wyrd&#x27;s ioun stones can be swapped out by any creature the wyrd trusts.\n\nShare Iouns (su): A character with an ioun wyrd familiar gains the benefit of the wyrd&#x27;s ioun stones as long as the character is within 30 feet of the ioun wyrd.</div>\r\n            </div>\r\n        \r\n        <!-- DESCRIPTION -->\r\n            <div style='break-inside: avoid;'>\r\n                <h3>Description</h3>        <div style=\"white-space: pre-wrap\">At first glance, these creatures appear to resemble some manner of earth elementals, but they&#x27;re actually bizarre constructs created by spellcasters from certain types of minerals and gemstones. Ioun wyrds are occasionally found in the wild, typically near the haunts of spellcasters and Arcana societies where the strange little creatures escaped their former masters.\n\nA typical ioun wyrd is roughly 2 feet in diameter and weighs between 15 and 20 pounds.\nConstruction\nAn ioun wyrd is made of small gemstones, lodestones, and bits of granite, all coated with 500 gp worth of alchemical materials. A functional ioun stone must also be present at its construction, which the ioun wyrd takes as the first ioun stone to be integrated into its body with its ioun affinity.\n\nIoun Wyrd\nCL 5th; Price 1,500 gp plus ioun stone\nConstruction\nRequirements Craft Construct, animate object, lesser geas; Skill Knowledge (arcana) DC 15; Cost 1,000 gp plus ioun stone\n</div>\r\n                    \r\n    </div>\r\n    <hr style=\"margin-left: 0; margin-bottom: -3px; width: 100%; height: 4px; background-color: #e0a100; border: 1px solid #000; column-span: all;\" />\r\n</div></div>\n    \n        <br>\n        <hr>\n        <div class=\"rawInputContainer\" style=\"margin-top: 15px;\">\n            <h2>RAW INPUT</h2>\n            <hr>\n            <pre style=\"white-space: pre-wrap; font-size: 10px;\">Ioun Wyrd CR 1/3\nSource Ultimate Wilderness pg. 197, Familiar Folio pg. 30\nXP 135\nN Tiny construct\nInit +2; Senses blindsight 30 ft.; Perception +2\nDefense\nAC 15, touch 15, flat-footed 12 (+2 Dex, +1 dodge, +2 size)\nhp 5 (1d10)\nFort +0, Ref +2, Will +2\nImmune construct traits\nOffense\nSpeed 0 ft., fly 30 ft. (average)\nMelee slam +0 (1d4-3)\nSpace 2-1/2 ft., Reach 0 ft.\nStatistics\nStr 4, Dex 15, Con -, Int 3, Wis 14, Cha 5\nBase Atk +1; CMB +1; CMD 9\nFeats Dodge\nSkills Fly +10\nLanguages Common (can't speak)\nSQ ioun affinity, share iouns\nEcology\nEnvironment any\nOrganization solitary\nTreasure none\nSpecial Abilities\nIoun Affinity (Su) An ioun wyrd can integrate a number of ioun stones into its body equal to 1 + half its Hit Dice. Because an ioun wyrd sees all ioun stones as equal and gains no benefits from them, the wyrd's ioun stones can be swapped out by any creature the wyrd trusts.\n\nShare Iouns (Su) A character with an ioun wyrd familiar gains the benefit of the wyrd's ioun stones as long as the character is within 30 feet of the ioun wyrd.\nDescription\nAt first glance, these creatures appear to resemble some manner of earth elementals, but they're actually bizarre constructs created by spellcasters from certain types of minerals and gemstones. Ioun wyrds are occasionally found in the wild, typically near the haunts of spellcasters and arcane societies where the strange little creatures escaped their former masters.\n\nA typical ioun wyrd is roughly 2 feet in diameter and weighs between 15 and 20 pounds.\nConstruction\nAn ioun wyrd is made of small gemstones, lodestones, and bits of granite, all coated with 500 gp worth of alchemical materials. A functional ioun stone must also be present at its construction, which the ioun wyrd takes as the first ioun stone to be integrated into its body with its ioun affinity.\n\nIoun Wyrd\nCL 5th; Price 1,500 gp plus ioun stone\nConstruction\nRequirements Craft Construct, animate object, lesser geas; Skill Knowledge (arcana) DC 15; Cost 1,000 gp plus ioun stone</pre>\n        </div>\n    "
  skills:
    fly:
      rank: 4
  traits:
    languages:
      - null
    senses:
      custom: blindsight 30 ft.; Perception +2
    size: tiny
type: npc
