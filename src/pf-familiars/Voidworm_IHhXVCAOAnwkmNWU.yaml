_id: IHhXVCAOAnwkmNWU
_key: '!actors!IHhXVCAOAnwkmNWU'
_stats:
  coreVersion: '12.331'
img: icons/svg/mystery-man.svg
items:
  - _id: cV7yHt8i5YCV0ZTd
    _key: '!actors.items!IHhXVCAOAnwkmNWU.cV7yHt8i5YCV0ZTd'
    _stats:
      coreVersion: '12.331'
    img: systems/pf1/icons/skills/yellow_40.jpg
    name: Outsider (Chaotic, Extraplanar, Protean, Shapechanger)
    system:
      bab: high
      classSkills:
        blf: true
        crf: true
        kpl: true
        per: true
        sen: true
        ste: true
      description:
        value: >-
          <p>An outsider is at least partially composed of the essence (but not
          necessarily the material) of some plane other than the Material Plane.
          Some creatures start out as some other type and become outsiders when
          they attain a higher (or lower) state of spiritual
          existence.</p><h2>Features</h2><p>An outsider has the following
          features.</p><ul><li>d10 Hit Dice.</li><li>Base attack bonus equal to
          total Hit Dice (fast progression).</li><li>Two good saving throws,
          usually&nbsp;<a
          href="https://www.d20pfsrd.com/gamemastering/combat#TOC-Reflex"
          rel="nofollow">Reflex</a>&nbsp;and Will.</li><li>Skill points equal to
          6 + Int modifier (minimum 1) per Hit Die. The following are class
          skills for outsiders: <a href="https://www.d20pfsrd.com/skills/bluff"
          rel="nofollow">Bluff</a>, <a
          href="https://www.d20pfsrd.com/skills/craft" rel="nofollow">Craft</a>,
          <a href="https://www.d20pfsrd.com/skills/knowledge"
          rel="nofollow">Knowledge</a>&nbsp;(planes), <a
          href="https://www.d20pfsrd.com/skills/perception"
          rel="nofollow">Perception</a>, <a
          href="https://www.d20pfsrd.com/skills/sense-motive"
          rel="nofollow">Sense Motive</a>, and <a
          href="https://www.d20pfsrd.com/skills/stealth"
          rel="nofollow">Stealth</a>. Due to their varied nature, outsiders also
          receive 4 additional class skills determined by the creature’s
          theme.</li></ul><h2>Traits</h2><p>An outsider possesses the following
          traits (unless otherwise noted in a creature’s
          entry).</p><ul><li>Darkvision 60 feet.</li><li>Unlike most living
          creatures, an outsider does not have a dual nature—its soul and body
          form one unit. When an outsider is slain, no soul is set loose. Spells
          that restore souls to their bodies, such as&nbsp;<a
          href="https://www.d20pfsrd.com/magic/all-spells/r/raise-dead"
          rel="nofollow">raise dead</a>,&nbsp;<a
          href="https://www.d20pfsrd.com/magic/all-spells/r/reincarnate"
          rel="nofollow">reincarnate</a>,&nbsp;and&nbsp;<a
          href="https://www.d20pfsrd.com/magic/all-spells/r/resurrection"
          rel="nofollow">resurrection</a>,&nbsp;don’t work on an outsider. It
          takes a different magical effect, such as&nbsp;<a
          href="https://www.d20pfsrd.com/magic/all-spells/l/limited-wish"
          rel="nofollow">limited wish</a>,&nbsp;<a
          href="https://www.d20pfsrd.com/magic/all-spells/w/wish"
          rel="nofollow">wish</a>,&nbsp;<a
          href="https://www.d20pfsrd.com/magic/all-spells/m/miracle"
          rel="nofollow">miracle</a>,&nbsp;or&nbsp;<a
          href="https://www.d20pfsrd.com/magic/all-spells/t/true-resurrection"
          rel="nofollow">true resurrection</a>&nbsp;to restore it to life. An
          outsider with the native subtype can be raised, reincarnated, or
          resurrected just as other living creatures can be.</li><li>Proficient
          with all simple and martial weapons and any weapons mentioned in its
          entry.</li><li>Proficient with whatever type of armor (light, medium,
          or heavy) it is described as wearing, as well as all lighter types.
          Outsiders not indicated as wearing armor are not proficient with
          armor. Outsiders are proficient with shields if they are proficient
          with any form of armor.</li><li>Outsiders breathe, but do not need to
          eat or sleep (although they can do so if they wish). Native outsiders
          breathe, eat, and sleep.</li></ul>
      hd: 10
      hp: 26
      level: 3
      savingThrows:
        ref:
          value: high
        will:
          value: high
      skillsPerLevel: 6
      tag: outsider
    type: class
  - _id: l1yjJVujC0Hr3XWV
    _key: '!actors.items!IHhXVCAOAnwkmNWU.l1yjJVujC0Hr3XWV'
    _stats:
      coreVersion: '12.331'
    img: systems/pf1/icons/skills/yellow_40.jpg
    name: 'Race: Outsider'
    system:
      creatureSubtypes:
        - chaotic
        - extraplanar
        - protean
        - shapechanger
      creatureTypes:
        - outsider
      description:
        value: >-
          sbc | As the statblock did not include a race, a custom one was
          generated.
    type: race
  - _id: sPFE92uZvKKvwAHS
    _key: '!actors.items!IHhXVCAOAnwkmNWU.sPFE92uZvKKvwAHS'
    _stats:
      coreVersion: '12.331'
    img: systems/pf1/icons/skills/shadow_12.jpg
    name: 'Sense: Darkvision 30ft.'
    system:
      abilityType: ex
      description:
        value: >-
          Darkvision is the extraordinary ability to see with no light source at
          all, out to a range specified for the creature. Darkvision is
          black-and-white only (colors cannot be discerned). It does not allow
          characters to see anything that they could not see otherwise—invisible
          objects are still invisible, and illusions are still visible as what
          they seem to be. Likewise, darkvision subjects a creature to gaze
          attacks normally. The presence of light does not spoil darkvision.
      subType: racial
    type: feat
  - _id: yGecNiJBWbhpBAJg
    _key: '!actors.items!IHhXVCAOAnwkmNWU.yGecNiJBWbhpBAJg'
    _stats:
      coreVersion: '12.331'
    img: systems/pf1/icons/skills/light_01.jpg
    name: 'Sense: Blindsense'
    system:
      abilityType: ex
      description:
        value: >-
          Using nonvisual senses, such as acute smell or hearing, a creature
          with blindsense notices things it cannot see.</p><p>The creature
          usually does not need to make Perception checks to pinpoint the
          location of a creature within range of its blindsense ability,
          provided that it has line of effect to that creature.</p><p>Any
          opponent the creature cannot see still has total concealment against
          the creature with blindsense, and the creature still has the normal
          miss chance when attacking foes that have
          concealment.</p><p>Visibility still affects the movement of a creature
          with blindsense. A creature with blindsense is still denied its
          Dexterity bonus to Armor Class against attacks from creatures it
          cannot see.
      subType: racial
    type: feat
  - _id: M9K7wUBAh7W1tMnE
    _key: '!actors.items!IHhXVCAOAnwkmNWU.M9K7wUBAh7W1tMnE'
    _stats:
      coreVersion: '12.331'
    img: systems/pf1/icons/skills/yellow_36.jpg
    name: >-
      Special Quality: Change Shape (2 Forms,both Of Which Must Be Tiny
      Animals,beast Shape Ii)
    system:
      description:
        value: >-
          sbc | Placeholder for Special Qualities, which in most statblocks are
          listed under SQ in the statistics block, but described in the Special
          Abilities. Remove duplicates as needed!
      subType: misc
    type: feat
  - _id: LpHqFRNMafwAyVrc
    _key: '!actors.items!IHhXVCAOAnwkmNWU.LpHqFRNMafwAyVrc'
    _stats:
      coreVersion: '12.331'
    img: systems/pf1/icons/skills/yellow_36.jpg
    name: Fast Healing 2
    system:
      description:
        value: sbc | Placeholder
      subType: misc
    type: feat
  - _id: NbOLd4FQjf14RlYN
    _key: '!actors.items!IHhXVCAOAnwkmNWU.NbOLd4FQjf14RlYN'
    _stats:
      coreVersion: '12.331'
    img: systems/pf1/icons/skills/blue_20.jpg
    name: Amorphous Anatomy
    system:
      abilityType: su
      actions:
        - _id: rgprdhmn6emo0f2j
          activation:
            type: passive
            unchained:
              type: passive
          img: systems/pf1/icons/skills/blue_20.jpg
          name: Use
          tag: amorphousAnatomy
      associations:
        classes:
          - Sorcerer
      description:
        value: >-
          <p>At 20th level, your vital organs shift position, and as a rule they
          are constantly changing their shape and size to best protect you. You
          gain&nbsp;<a
          href="https://www.d20pfsrd.com/bestiary/rules-for-monsters/universal-monster-rules#TOC-Immunity-Ex-or-Su-">immunity</a>&nbsp;to
          critical hits and&nbsp;<a
          href="https://www.d20pfsrd.com/classes/core-classes/rogue#TOC-Sneak-Attack">sneak
          attacks</a>. In addition, you gain&nbsp;<a
          href="https://www.d20pfsrd.com/gamemastering/special-abilities#TOC-Blindsight-and-Blindsense">blindsight</a>&nbsp;with
          a range of 60 feet and&nbsp;<a
          href="https://www.d20pfsrd.com/gamemastering/special-abilities#TOC-Damage-Reduction">damage
          reduction</a>&nbsp;5/—. You automatically recover from physical
          blindness or deafness after 1 round by growing new sensory organs to
          replace the compromised ones; you cannot otherwise regenerate lost
          body parts.</p>
      subType: classFeat
    type: feat
  - _id: QLIHoosh12D3Jtlr
    _key: '!actors.items!IHhXVCAOAnwkmNWU.QLIHoosh12D3Jtlr'
    _stats:
      coreVersion: '12.331'
    img: systems/pf1/icons/skills/yellow_36.jpg
    name: Freedom Of Movement
    system:
      description:
        value: sbc | Placeholder
      subType: classFeat
    type: feat
  - _id: lITftcOyLIvVZcxC
    _key: '!actors.items!IHhXVCAOAnwkmNWU.lITftcOyLIvVZcxC'
    _stats:
      coreVersion: '12.331'
    img: systems/pf1/icons/items/inventory/monster-head.jpg
    name: Bite
    system:
      actions:
        - _id: rkbq14j0wglkereh
          ability:
            attack: _default
            damage: str
          actionType: mwak
          activation:
            type: attack
            unchained:
              type: attack
          attackBonus: '5'
          attackName: Bite
          damage:
            parts:
              - formula: 1d3+0
                types:
                  - slashing
          duration:
            units: inst
          img: systems/pf1/icons/items/inventory/monster-head.jpg
          name: Attack
          notes:
            footer:
              - bite +8 (1d3-2)
          range:
            units: melee
          tag: bite
      subType: natural
    type: attack
  - _id: ypZtC2D0kf1Y4ihV
    _key: '!actors.items!IHhXVCAOAnwkmNWU.ypZtC2D0kf1Y4ihV'
    _stats:
      coreVersion: '12.331'
    img: systems/pf1/icons/items/inventory/monster-tail.jpg
    name: Tail Slap
    system:
      actions:
        - _id: 3kg6uywxi7w1rxu6
          ability:
            attack: _default
            damage: str
          actionType: mwak
          activation:
            type: attack
            unchained:
              type: attack
          attackBonus: '5'
          attackName: Tail Slap
          damage:
            parts:
              - formula: 1d3+5
          duration:
            units: inst
          img: systems/pf1/icons/items/inventory/monster-tail.jpg
          name: Attack
          notes:
            footer:
              - tail slap +3 (1d3-2)
          range:
            units: melee
          tag: tailSlap
      subType: natural
    type: attack
  - _id: BnfNB931v64WoBnV
    _key: '!actors.items!IHhXVCAOAnwkmNWU.BnfNB931v64WoBnV'
    _stats:
      coreVersion: '12.331'
    img: systems/pf1/icons/skills/yellow_36.jpg
    name: 'Environment: Any (limbo)'
    system:
      description:
        value: >-
          sbc | Here you’ll find information on how the monster fits into the
          world, notes on its ecology and society, and other bits of useful lore
          and flavor that will help you breathe life into the creature when your
          PCs encounter it.
      subType: misc
    type: feat
  - _id: Z4Sf8VKo3LwR1lWe
    _key: '!actors.items!IHhXVCAOAnwkmNWU.Z4Sf8VKo3LwR1lWe'
    _stats:
      coreVersion: '12.331'
    img: systems/pf1/icons/skills/yellow_36.jpg
    name: 'Organization: Solitary, Pair, Or School (3-18)'
    system:
      description:
        value: >-
          sbc | Here you’ll find information on how the monster fits into the
          world, notes on its ecology and society, and other bits of useful lore
          and flavor that will help you breathe life into the creature when your
          PCs encounter it.
      subType: misc
    type: feat
  - _id: BrZrgC3ftffcqsBk
    _key: '!actors.items!IHhXVCAOAnwkmNWU.BrZrgC3ftffcqsBk'
    _stats:
      coreVersion: '12.331'
    img: systems/pf1/icons/skills/yellow_36.jpg
    name: 'Treasure: None'
    system:
      description:
        value: >-
          sbc | Here you’ll find information on how the monster fits into the
          world, notes on its ecology and society, and other bits of useful lore
          and flavor that will help you breathe life into the creature when your
          PCs encounter it.
      subType: misc
    type: feat
  - _id: VlETX2j8yO8TsNKl
    _key: '!actors.items!IHhXVCAOAnwkmNWU.VlETX2j8yO8TsNKl'
    _stats:
      coreVersion: '12.331'
    img: systems/pf1/icons/skills/yellow_36.jpg
    name: Confusion
    system:
      abilityType: su
      description:
        value: >-
          A creature struck by a voidworm's tail slap must make a DC 12 Will
          save or become confused for 1 round. This is a mind-affecting effect.
          The save DC is Charisma-based.
      subType: classFeat
    type: feat
  - _id: 02kz8Ny130eDKtkX
    _key: '!actors.items!IHhXVCAOAnwkmNWU.02kz8Ny130eDKtkX'
    _stats:
      coreVersion: '12.331'
    img: systems/pf1/icons/skills/yellow_36.jpg
    name: sbc | Conversion Buff
    system:
      active: true
      changes:
        - _id: rcmry0gq
          formula: '5'
          target: cmb
          type: untypedPerm
        - _id: 48g3phjr
          formula: '-10'
          target: mhp
          type: untypedPerm
        - _id: naxwebhn
          formula: '-8'
          target: skill.fly
          type: untypedPerm
      description:
        value: >-
          <h2>sbc | Conversion Buff</h2><p>This Buff was created by
          <strong>sbc</strong> to compensate for differences between the input
          and the values FoundryVTT calculates automatically.</p><p>Especially
          when the pathfinder system gets upgraded, entries in compendiums get
          updated or foundry changes in some way, this buff may become
          outdated.</p><p>For most mooks the conversion should more or less
          work, but for important NPCs or creatures it is adviced to double
          check the conversion manually.</p>
      hideFromToken: true
      subType: perm
      tag: sbcConversionBuff
    type: buff
name: Voidworm
prototypeToken:
  texture:
    src: icons/svg/mystery-man.svg
system:
  abilities:
    cha:
      value: 13
    dex:
      value: 17
    int:
      value: 8
    str:
      value: 7
    wis:
      value: 8
  attributes:
    cmdNotes: can't be tripped
    speed:
      fly:
        base: 50
        maneuverability: perfect
      land:
        base: 20
    spells:
      spellbooks:
        primary:
          name: Primary
        secondary:
          name: Secondary
        spelllike:
          name: Spell-likes
        tertiary:
          name: Tertiary
  details:
    alignment: cn
    cr:
      base: 2
    notes:
      value: "\n        <div class=\"statblockContainer\" style=\"margin-top: 15px\"><div style=\"width: 100%; background-color: #ffe9c7; box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19); columns: 2 150px\">\r\n    <hr style=\"margin-left: 0; margin-top:-2px; width: 100%; height: 4px; background-color: #e0a100; border: 1px solid #000; column-span: all;\" />\r\n    <div style=\"padding: 5px;\">\r\n        <h1>Voidworm CR 2 </h1>\r\n        \r\n        <!-- BASE -->\r\n        <div style=\"break-inside: avoid;\">\r\n            <strong>Source</strong>: Bestiary 2 pg. 217<br/>\r\n            <strong>XP 600</strong><br/>\r\n            \r\n            \r\n            \r\n            \r\n            \r\n            CN \r\n            Tiny \r\n            Outsider (Chaotic, Extraplanar, Protean, Shapechanger) <br/>\r\n            <strong>Init</strong> 3;\r\n            <strong>Senses</strong> blindsense 30 ft., darkvision 30 ft., detect law; Perception +8; \r\n            \r\n        </div>\r\n        <div style=\"break-inside: avoid;\">\r\n            <h3>Defense</h3>\r\n                <strong>AC</strong> 15, <strong>Touch</strong> 15, <strong>Flat-Footed</strong> 12 (+3 Dex, +2 size)<br/>\r\n            <strong>Hit Points</strong> 16 (3 HD; 3d10);  fast healing 2<br/>\r\n            <strong>Fort</strong> 1, <strong>Ref</strong> 6, <strong>Will</strong> 2\r\n            <br/><strong>Defensive Abilities</strong> amorphous anatomy, freedom of movement; \r\n            \r\n            <strong>Immune</strong> acid; \r\n            <strong>Resist</strong> electricity 10, sonic 10; \r\n            \r\n            \r\n            \r\n        </div>\r\n        <div>\r\n            <div style=\"break-inside: avoid;\">\r\n                <h3>Offense</h3>\r\n                        <strong>Speed</strong> 50 ft. (perfect)<br/>\r\n                <strong>Melee</strong> bite +8 (1d3-2), tail slap +3 (1d3-2 plus confusion)<br/>\r\n                \r\n                \r\n                \r\n                \r\n            </div>\r\n        </div>\r\n        <!-- SPELLCASTING -->\r\n                \r\n        <!-- TACTICS -->\r\n                \r\n                \r\n                \r\n                \r\n        <!-- STATISTICS -->\r\n        <div style=\"break-inside: avoid;\">\r\n            <h3>Statistics</h3>\r\n                <strong>Str</strong> 7, <strong>Dex</strong> 17, <strong>Con</strong> 10, <strong>Int</strong> 8, <strong>Wis</strong> 8, <strong>Cha</strong> 13<br/>\r\n            <strong>Base Atk</strong> +3; <strong>CMB</strong> +4; <strong>CMD</strong> 12<br/>\r\n            <strong>Feats</strong> Skill Focus (Perception), Weapon Finesse<br/>\r\n            <strong>Skills</strong> Acrobatics +9 (+5 jump), Bluff +7, Escape Artist +7, Fly +19, Knowledge (arcana) +5, Perception +8, Stealth +15<br/>\r\n            <strong>Languages</strong> Common, Protean<br/>\r\n            \r\n            \r\n            \r\n            \r\n        </div>\r\n        <!-- ECOLOGY -->\r\n            <div style='break-inside: avoid;'>\r\n                <h3>Ecology</h3>                <strong>Environment</strong>: any (Limbo)<br/>\r\n                <strong>Organization</strong>: solitary, pair, or school (3-18)<br/>\r\n                <strong>Treasure</strong>: none<br/>\r\n            </div>\r\n        <!-- SPECIAL ABILITIES -->\r\n            <div style='break-inside: avoid;'>\r\n                <h3>Special Abilities</h3>                <div style='white-space: pre-wrap;'>Confusion (su): A creature struck by a voidworm&#x27;s tail slap must make a DC 12 Will save or become confused for 1 round. This is a mind-affecting effect. The save DC is Charisma-based.</div>\r\n            </div>\r\n        \r\n        <!-- DESCRIPTION -->\r\n            <div style='break-inside: avoid;'>\r\n                <h3>Description</h3>        <div style=\"white-space: pre-wrap\">Debate rages as to whether or not the strange and capricious creatures called voidworms are actually proteans at all. To the wizards and sorcerers who summon them as familiars, the answer seems obvious-these tiny dwellers of Limbo have all the requisite racial traits of proteans, down to their serpentine shapes. Yet the established protean castes find such claims outright insulting, claiming instead that it is such acts of conjuration that call voidworms forth from the raw stuff of Limbo, giving them shape and life according to the spellcasters&#x27; expectations, and that these lesser beings are but pale reflections of their formidable kin. Voidworms themselves have little to say on the matter-creatures of the moment, and sparing little thought for the constantly mutable concept of &#x27;reality,&#x27; voidworms only barely grasp cause and effect, and the past has no more substance or significance for them than a dream. In order to gain a voidworm as a familiar, a spellcaster must be chaotic neutral, be caster level 7th, and have the Improved Familiar feat.\n\nRegardless of their actual origins, voidworms maintain a thriving ecology in the chaos of Limbo, forming together into darting, flashing schools that are often hunted for sport by naunets and other predators of chaos. Mortal wizards, however, most commonly encounter voidworms as summoned familiars. These tiny, serpentine creatures are particularly valued by illusionists, evokers, and other magical practitioners who deal with distorting or molding reality, though the familiars&#x27; bizarre logic and miniscule attention spans sometimes make them more trouble than they&#x27;re worth. Still, their confusing attack and remarkable hardiness have saved more than one wizard on the battlefield, and their strange thought processes can sometimes offer unique insights in the laboratory. When traveling in more mundane lands, wizards often order voidworm familiars to use their change shape ability to disguise themselves as ordinary pets or animal familiars, though these disguises tend to slip when the voidworm grows curious or playful.\n\nA voidworm is only 2 feet long and weighs a mere 2 pounds. No two voidworms are exactly alike in their coloration or markings. Their two feathery wings generally take on brighter colors than the rest of their bodies, and in the case of voidworms conjured as familiars, these &#x27;wings&#x27; are the same color as their masters&#x27; eyes.\n</div>\r\n                    \r\n    </div>\r\n    <hr style=\"margin-left: 0; margin-bottom: -3px; width: 100%; height: 4px; background-color: #e0a100; border: 1px solid #000; column-span: all;\" />\r\n</div></div>\n    \n        <br>\n        <hr>\n        <div class=\"rawInputContainer\" style=\"margin-top: 15px;\">\n            <h2>RAW INPUT</h2>\n            <hr>\n            <pre style=\"white-space: pre-wrap; font-size: 10px;\">Voidworm CR 2\nSource Bestiary 2 pg. 217\nXP 600\nCN Tiny outsider (chaotic, extraplanar, protean, shapechanger)\nInit +3; Senses blindsense 30 ft., darkvision 30 ft., detect law; Perception +8\nDefense\nAC 15, touch 15, flat-footed 12 (+3 Dex, +2 size)\nhp 16 (3d10); fast healing 2\nFort +1, Ref +6, Will +2\nDefensive Abilities amorphous anatomy, freedom of movement; Immune acid; Resist electricity 10, sonic 10\nOffense\nSpeed 20 ft., fly 50 ft. (perfect)\nMelee bite +8 (1d3-2), tail slap +3 (1d3-2 plus confusion)\nSpace 2-1/2 ft., Reach 0 ft.\nSpell-Like Abilities (CL 6th; concentration +7)\nConstant-detect law\nAt will-dancing lights, ghost sound (DC 11), prestidigitation\n3/day-blur (self only), obscuring mist\n1/week-commune (CL 12th, 6 questions)\nStatistics\nStr 7, Dex 17, Con 10, Int 8, Wis 8, Cha 13\nBase Atk +3; CMB +4; CMD 12 (can't be tripped)\nFeats Skill Focus (Perception), Weapon Finesse\nSkills Acrobatics +9 (+5 jump), Bluff +7, Escape Artist +7, Fly +19, Knowledge (arcana) +5, Perception +8, Stealth +15\nLanguages Common, Protean\nSQ change shape (2 forms, both of which must be Tiny animals; beast shape II)\nEcology\nEnvironment any (Limbo)\nOrganization solitary, pair, or school (3-18)\nTreasure none\nSpecial Abilities\nConfusion (Su) A creature struck by a voidworm's tail slap must make a DC 12 Will save or become confused for 1 round. This is a mind-affecting effect. The save DC is Charisma-based.\nDescription\nDebate rages as to whether or not the strange and capricious creatures called voidworms are actually proteans at all. To the wizards and sorcerers who summon them as familiars, the answer seems obvious-these tiny dwellers of Limbo have all the requisite racial traits of proteans, down to their serpentine shapes. Yet the established protean castes find such claims outright insulting, claiming instead that it is such acts of conjuration that call voidworms forth from the raw stuff of Limbo, giving them shape and life according to the spellcasters' expectations, and that these lesser beings are but pale reflections of their formidable kin. Voidworms themselves have little to say on the matter-creatures of the moment, and sparing little thought for the constantly mutable concept of 'reality,' voidworms only barely grasp cause and effect, and the past has no more substance or significance for them than a dream. In order to gain a voidworm as a familiar, a spellcaster must be chaotic neutral, be caster level 7th, and have the Improved Familiar feat.\n\nRegardless of their actual origins, voidworms maintain a thriving ecology in the chaos of Limbo, forming together into darting, flashing schools that are often hunted for sport by naunets and other predators of chaos. Mortal wizards, however, most commonly encounter voidworms as summoned familiars. These tiny, serpentine creatures are particularly valued by illusionists, evokers, and other magical practitioners who deal with distorting or molding reality, though the familiars' bizarre logic and miniscule attention spans sometimes make them more trouble than they're worth. Still, their confusing attack and remarkable hardiness have saved more than one wizard on the battlefield, and their strange thought processes can sometimes offer unique insights in the laboratory. When traveling in more mundane lands, wizards often order voidworm familiars to use their change shape ability to disguise themselves as ordinary pets or animal familiars, though these disguises tend to slip when the voidworm grows curious or playful.\n\nA voidworm is only 2 feet long and weighs a mere 2 pounds. No two voidworms are exactly alike in their coloration or markings. Their two feathery wings generally take on brighter colors than the rest of their bodies, and in the case of voidworms conjured as familiars, these 'wings' are the same color as their masters' eyes.</pre>\n        </div>\n    "
  skills:
    acr:
      rank: 6
    blf:
      rank: 3
    esc:
      rank: 4
    fly:
      rank: 12
    kar:
      rank: 6
    per:
      rank: 6
    ste:
      rank: 1
  traits:
    di:
      - acid
    eres:
      custom: >-
        Electricity 10;Sonic 10;Electricity 10;Sonic 10;Electricity 10;Sonic
        10;Electricity 10;Sonic 10;Electricity 10;Sonic 10;
    languages:
      - common
      - null
    senses:
      custom: blindsense 30 ft., darkvision 30 ft., detect law; Perception +8
      dv:
        value: 30
    size: tiny
type: npc
