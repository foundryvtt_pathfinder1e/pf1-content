_id: bPJNmeL9hs2LTbco
_key: '!items!bPJNmeL9hs2LTbco'
_stats:
  coreVersion: '12.331'
folder: jQR4YTxfCPFBsjW4
img: systems/pf1/icons/skills/affliction_24.jpg
name: Invocation
system:
  abilityType: su
  actions:
    - _id: 9qqhgxy4n68t413d
      activation:
        type: standard
        unchained:
          cost: 2
          type: action
      duration:
        units: minute
        value: '1'
      name: Use
      range:
        units: ft
        value: '30'
      target:
        value: allies
  associations:
    classes:
      - Omdura
  description:
    value: >-
      <p>An omdura can call forth he power of her deity to improve her allies’
      abilities as a <em>standard action</em>.</p>

      <p>When the omdura calls upon this power, she must select one type of
      invocation from those listed below to grant the benefits to her allies
      within 30 feet. If the omdura is evil, she grants <em>profane</em> bonuses
      instead of sacred, as appropriate. Neutral omduras must select
      <em>profane</em> or sacred bonuses. Once made, this choice cannot be
      changed. A called invocation cannot be disrupted, but it ends immediately
      if the omdura is killed, <em>paralyzed</em>, <em>stunned</em>, or knocked
      <em>unconscious</em>.</p>

      <p>The omdura can use this ability for a number of minutes per day equal
      to her level. This duration does not need to be consecutive, but it must
      be spent in 1-minute increments. The omdura can change her chosen
      invocation to another type as a <em>swift action</em>, but doing so
      expends a 1-minute increment of her invocations.</p>

      <p>At 7th level, an omdura can call an invocation as a <em>move
      action</em> instead of a <em>standard action</em>. At 13th level, an
      omdura can call an invocation as a <em>swift action</em>.</p>

      <ul>

      <li><strong>Destruction</strong>: The omdura’s allies are filled with
      divine wrath, gaining a +1 <em>sacred bonus</em> on weapon damage rolls.
      This bonus increases by 1 for every 3 class levels the omdura has.</li>

      <li><strong>Healing</strong>: The omdura radiates a healing light,
      granting <em>fast healing</em> 1. This causes the omdura’s allies to
      recover 1 hit point each round (up to their maximum <em>hit points</em>).
      The amount of <em>fast healing</em> increases by 1 for every 3 class
      levels the omdura has.</li>

      <li><strong>Justice</strong>: This invocation spurs the omdura’s allies to
      seek justice, granting a +1 <em>sacred bonus</em> on <em>attack
      rolls</em>. This bonus increases by 1 for every 5 class levels the omdura
      has. At 10th level, this bonus is doubled on <em>attack rolls</em> to
      confirm critical hits.</li>

      <li><strong>Piercing</strong>: This invocation gives the omdura’s allies
      great focus and makes their spells more potent. This benefit grants a +1
      <em>sacred bonus</em> on <em>concentration</em> checks and <em>caster
      level</em> checks to overcome a target’s <em>spell resistance</em>. This
      bonus increases by 1 for every 3 class levels the omdura has.</li>

      <li><strong>Protection</strong>: The omdura radiates a protective aura,
      granting a +1 <em>sacred bonus</em> to <em>Armor Class</em>. This bonus
      increases by 1 for every 5 class levels the omdura has. At 10th level,
      this bonus is doubled against <em>attack rolls</em> to confirm critical
      hits against the omdura’s allies.</li>

      <li><strong>Purify</strong>: The omdura’s allies are protected from the
      vile taint of her foes, gaining a +1 <em>sacred bonus</em> on all saving
      throws. This bonus increases by 1 for every 5 class levels the omdura has.
      At 10th level, the bonus is doubled against curses, diseases, and
      poisons.</li>

      <li><strong>Resiliency</strong>: This invocation makes the omdura’s allies
      resistant to harm, granting <em>DR</em> 1/magic. This <em>DR</em>
      increases by 1 for every 5 class levels the omdura has. At 10th level,
      this <em>DR</em> changes from magic to an <em>alignment</em> (chaotic,
      evil, good, or lawful) that is opposite the omdura’s. If she is neutral,
      the type of <em>DR</em> doesn’t change.</li>

      <li><strong>Resistance</strong>: The omdura’s allies are shielded by a
      flickering aura, gaining 2 points of <em>energy resistance</em> against
      one energy type (acid, cold, electricity, fire, or sonic)chosen when the
      omdura calls this invocation. The protection increases by 2 for every 3
      class levels the omdura has.</li>

      <li><strong>Smiting</strong>: This invocation bathes the weapons of the
      omdura’s allies in a divine light. These weapons count as magic for the
      purpose of bypassing <em>damage reduction</em>. At 6th level, the allies’
      weapons also count as one alignment type (chaotic, evil, good, or lawful)
      for the purpose of bypassing <em>damage reduction</em>. The type selected
      must match one of the omdura’s alignments. If the omdura is neutral, she
      does not grant this bonus. At 10th level, the allies’ weapons also count
      as <em>adamantine</em> for the purpose of overcoming <em>damage
      reduction</em> (but not for reducing <em>hardness</em>).</li>

      </ul>
  sources:
    - title: Pathfinder - Niobe
  subType: classFeat
  tag: classFeat_invocation
  uses:
    maxFormula: '@class.level'
    per: day
type: feat
